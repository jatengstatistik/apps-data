<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

class CkanApi extends Component {

  
    public function action($host='',$action='',$data='',$method='',$auth='')
    {
        // $client = new Client(['baseUrl' => 'http://103.9.227.205/api/3']);
        // $host = 'http://data.jatengprov.go.id'
        $baseUrl = $host.'/api/3/action';
        $client = new Client(['baseUrl' => $baseUrl]);
        $response = $client->createRequest()
          // ->setHeaders(['Authorization' => '61e47dd8-352a-4fe3-99c6-058b1b7b73b0'])
          ->setHeaders(['Authorization' => $auth])
          ->setMethod($method)
          ->setFormat(Client::FORMAT_JSON)
          ->setUrl($action)
          ->setData($data)
          ->send();
        return $responseData = (object) $response->getData();
        // echo '<pre>';
        // print_r((object) $responseData->result);
        // echo '</pre>';
    }

    public function recentDataset()
    {
      // current_package_list_with_resources?limit=5&offset=0
      // recently_changed_packages_activity_list
      $package_list = $this->apiCkan('action/current_package_list_with_resources',['limit'=>5,'offset'=>0])->result;
      foreach ($package_list as $value) {
        $dataset[] = [
          'title' => $value['title'], 
          'name' => $value['name'], 
          'notes' => $value['notes'], 
          'link_dataset' => '/dataset/'.$value['name'],
          // 'title' => $value['data']['package']['title'], 
          // 'name' => $value['data']['package']['name'], 
          // 'notes' => $value['data']['package']['notes'], 
          // 'link_dataset' => '/dataset/'.$value['data']['package']['name'],
        ];
      }
      return $dataset;
      // echo '<pre>';
      // print_r($dataset);
      // echo '</pre>';
    }

  public function organizationList($value='')
  {
    $list_org = $this->apiCkan('action/organization_list');
    foreach ($list_org->result as $key => $organization) {
      $show_org = (object) $this->apiCkan('action/organization_show',['id'=>$organization,'include_users'=>false,'include_extras'=>false,'include_tags'=>false,'include_followers'=>false])->result;
      // echo "<pre>";
      // print_r($show_org);
      // echo "</pre>";   
      $organization_list[] = [
        'title' => $show_org->title,
        'description' => $show_org->description,
        'slug' => $organization,
        'package_count' => $show_org->package_count,
      ];
    }
    return $organization_list;
    // echo "<pre>";
    // print_r($this->array_sort($organization_list,'package_count',SORT_DESC));
    // echo "</pre>";
  }

  public function groupList()
  {
    $group = $this->apiCkan('action/group_list',['all_fields'=>true]);
    foreach ($group->result as $value) {
      $group_list[] = [
        'display_name' => ArrayHelper::getValue($value,'display_name'),
        'name' => ArrayHelper::getValue($value,'name'),
        'description' => ArrayHelper::getValue($value,'description'),
        'package_count' => ArrayHelper::getValue($value,'package_count'),
        'image_display_url' => ArrayHelper::getValue($value,'image_display_url'),
      ];
    }

    return $group_list;
    // echo "<pre>";
    // print_r((object) $group->result);
    // echo "</pre>"; 
  }

    private function apiCkan($url='',$data='',$method='')
    {
      // $client = new Client(['baseUrl' => 'http://103.9.227.205/api/3']);
      $client = new Client(['baseUrl' => 'http://data.jatengprov.go.id/api/3']);
    $response = $client->createRequest()
      // ->setHeaders(['Authorization' => '61e47dd8-352a-4fe3-99c6-058b1b7b73b0'])
      ->setMethod($method='GET')
      // ->setFormat(Client::FORMAT_JSON)
      ->setUrl($url)
      ->setData($data)
      ->send();
    return (object) $responseData = $response->getData();
    // echo '<pre>';
    // print_r((object) $responseData->result);
    // echo '</pre>';
    }

}