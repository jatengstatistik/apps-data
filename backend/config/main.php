<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'name'=>'Open Data',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', 'scheduler'],
    'as access' => [
         'class' => '\hscstudio\mimin\components\AccessControl',
         'allowActions' => [
            // add wildcard allowed action here!
            'visitor/now',
            'visitor/weekly',
            // 'data/*',
            // 'site/*',
            // 'debug/*',
            // 'mimin/*', // only in dev mode
            // 'gii/*',
        ],
    ],
    'modules' => [
        'db-manager' => [
            'class' => 'bs\dbManager\Module',
            // path to directory for the dumps
            'path' => '@root/db',
            // list of registerd db-components
            'dbList' => ['db'],
            // 'as access' => [
                // 'class' => 'yii\filters\AccessControl',
                // 'rules' => [
                //     [
                //         'allow' => true,
                //         'roles' => ['admin'],
                //     ],
                // ],
            // ],
        ],
        'scheduler' => ['class' => 'webtoolsnz\scheduler\Module'],
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
        ],
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['127.0.0.1', '::1', '192.168.1.*', 'XXX.XXX.XXX.XXX','*'] // adjust this to your needs
        ],
        'mimin' => [
            'class' => '\hscstudio\mimin\Module',
        ],
        'filemanager' => [
            'class' => 'pendalf89\filemanager\Module',
            // Upload routes
            'routes' => [
                // Base absolute path to web directory
                'baseUrl' => '',
                // Base web directory url
                'basePath' => '@backend/web',
                // Path for uploaded files in web directory
                'uploadPath' => 'uploads',
            ],
            // Thumbnails info
            'thumbs' => [
                'small' => [
                    'name' => 'Small',
                    'size' => [100, 100],
                ],
                'medium' => [
                    'name' => 'Medium',
                    'size' => [300, 200],
                ],
                'large' => [
                    'name' => 'Large',
                    'size' => [500, 400],
                ],
            ],
        ],
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager', // only support DbManager
        ],
        // 'geoip' => ['class' => 'lysenkobv\GeoIP\GeoIP'],
        'geoip' => ['class' => 'dpodium\yii2\geoip\components\CGeoIP'],
        'view' => [
             'theme' => [
                 'pathMap' => [
                    '@app/views' => '@app/views/adminlte'
                 ],
             ],
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
            // 'baseUrl' => '/devdash',
        ],
        'user' => [
            // 'identityClass' => 'common\models\User',
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];
