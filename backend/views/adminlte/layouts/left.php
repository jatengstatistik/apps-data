<aside class="main-sidebar">

    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div> -->

        <!-- search form -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

<?php 
use hscstudio\mimin\components\Mimin;
$menuItems = [
                    ['label' => 'System Administrator', 'options' => ['class' => 'header']],
                    [
                        'label' => 'User Management', 'icon' => 'address-card', 'url' => ['#'], 'items' => [
                            ['label'=>'User', 'icon'=>'users', 'url'=>['/user/index'],],
                            ['label'=>'Role', 'icon'=>'user-circle', 'url'=>['/mimin/role'],],
                            ['label'=>'Route', 'icon'=>'gg-circle', 'url'=>['/mimin/route'],],
                        ],
                    ],
                    [
                        'label' => 'CKAN Management', 'icon' => 'tv', 'url' => ['#'], 'items' => [
                            // ['label'=>'Statistik', 'icon'=>'line-chart', 'url'=>['/'],],
                            // ['label'=>'User CKAN', 'icon'=>'users', 'url'=>['/user/index'],],
                            ['label'=>'Server Access', 'icon'=>'gg-circle', 'url'=>['/accounts-ods/index'],],
                            ['label'=>'Organization', 'icon'=>'building', 'url'=>['/ckan-organization/index'],],
                            ['label'=>'Group', 'icon'=>'users', 'url'=>['/ckan-group/index'],],
                        ],
                    ],
                    [
                        'label' => 'Master', 'icon' => 'list-ul', 'url' => ['#'], 'items' => [
                            ['label'=>'Kategori', 'icon'=>'list-alt', 'url'=>['/news-cat/index'],],
                            ['label'=>'Kategori', 'icon'=>'columns', 'url'=>['/categories/index'],],
                            ['label'=>'File Manager', 'icon'=>'files-o', 'url'=>['/filemanager'],],
                            // ['label'=>'Sekilas Info', 'icon'=>'object-group', 'url'=>['/popup-banner'],],
                        ],
                    ],
                    // ['label' => 'Web Admin', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Site Setting', 'icon' => 'link', 'url' => ['#'], 'items' => [
                            ['label'=>'Carousel', 'icon'=>'image', 'url'=>['/carousel/index'],],
                            ['label'=>'Banner', 'icon'=>'flag', 'url'=>['/banner/index'],],
                            ['label'=>'Backup DB', 'icon'=>'database', 'url'=>['/db-manager/default/index'],],
                        ],
                    ],
                    ['label' => 'CMS Menu', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Statistics', 'icon' => 'dashboard', 'url' => ['#'], 'items' => [
                            ['label'=>'Visitor', 'icon'=>'users', 'url'=>['/visitor/index'],],
                            ['label'=>'Dataset', 'icon'=>'files-o', 'url'=>['/ckan-dataset/index'],],
                            ['label'=>'Dataset SKPD', 'icon'=>'bank', 'url'=>['/ckan-dataset/skpd'],],
                            ['label'=>'Dataset Kab/Kota', 'icon'=>'building', 'url'=>['/ckan-dataset/kabko'],],
                        ],
                    ],
                    [
                        'label' => 'Publikasi', 'icon' => 'bullhorn', 'url' => ['#'], 'items' => [
                            ['label'=>'Artikel', 'icon'=>'file-text-o', 'url'=>['/news-article/index'],],
                        ],
                    ],
                    
                    // ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    // ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    // [
                    //     'label' => 'Some tools',
                    //     'icon' => 'share',
                    //     'url' => '#',
                    //     'items' => [
                    //         ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                    //         ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                    //         [
                    //             'label' => 'Level One',
                    //             'icon' => 'circle-o',
                    //             'url' => '#',
                    //             'items' => [
                    //                 ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                    //                 [
                    //                     'label' => 'Level Two',
                    //                     'icon' => 'circle-o',
                    //                     'url' => '#',
                    //                     'items' => [
                    //                         ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                    //                         ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                    //                     ],
                    //                 ],
                    //             ],
                    //         ],
                    //     ],
                    // ],
                ]; 

        $menuItems = Mimin::filterMenu($menuItems);
        // in other case maybe You want ensure same of route so You can add parameter strict true
        // $menuItems = Mimin::filterMenu($menuItems,true); 

       echo dmstr\widgets\Menu::widget([
            'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
            'items' => $menuItems,
        ]);
        ?>



    </section>

</aside>
