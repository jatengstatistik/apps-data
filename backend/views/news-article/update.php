<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NewsArticle */

$this->title = Yii::t('app', 'Update News Article: {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="news-article-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_cat' => $list_cat,
        'list_regencies' => $list_regencies,
    ]) ?>

</div>
