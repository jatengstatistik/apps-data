<?php

// use kartik\file\FileInput;
use app\models\Categories;
use kartik\tree\TreeViewInput;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use pendalf89\filemanager\widgets\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NewsArticle */
/* @var $form yii\widgets\ActiveForm */

 
 // $longitude = $location->latitude; //Available in only City DB
 // $latitude = $location->longitude; //Available in only City DB

?>
<div class="box box-success">
<div class="box-header with-border">
  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

  <div class="box-tools pull-right">
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
</div>
<div class="box-body">
  <div class="news-article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'cat_id')->widget(Select2::className(), [
        'data' => $list_cat,
        'options' => ['placeholder' => 'Pilih'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>
    <p>
    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default-image-featured">
    Show Image
    </button>
    </p>
    <?= $form->field($model, 'image_featured')->fileInput() ?>
        
    <label>Deskripsi</label>
    <!-- <?= $form->field($model, 'content')->textarea(['rows' => 30]) ?> -->
    <?php echo froala\froalaeditor\FroalaEditorWidget::widget([
    'model' => $model,
    'attribute' => 'content',
    'options' => [
        // html attributes
        'id'=>'content'
    ],
    'clientOptions' => [
        'height' => 300,
        'toolbarInline' => false,
        'theme' => 'royal', //optional: dark, red, gray, royal
        'language' => 'en_us', // optional: ar, bs, cs, da, de, en_ca, en_gb, en_us ...
        'toolbarButtons' => ['fullScreen','insertImage','insertFile','insertLink', 'bold', 'italic', 'underline', 'paragraphStyle', 'paragraphFormat','alignRight','alignCenter','alignLeft','alignJustify','formatOL','formatUL','outdent', 'indent','table'],
        'imageUploadParam' => 'file',
        'imageUploadURL' => Url::to(['news-article/upload/']),
        'imageUploadParams' => [
            Yii::$app->request->csrfParam => Yii::$app->request->getCsrfToken(),
        ],
        'imageManagerDeleteParams' => [
            Yii::$app->request->csrfParam => Yii::$app->request->getCsrfToken(),
        ],
        'fileUploadParam' => 'file',
        'fileUploadURL' => \yii\helpers\Url::to(['post/upload']),
        'fileUploadParams' => [
            Yii::$app->request->csrfParam => Yii::$app->request->getCsrfToken(),
        ],
        'fileManagerDeleteParams' => [
            Yii::$app->request->csrfParam => Yii::$app->request->getCsrfToken(),
        ],
    ],
    'clientPlugins'=> ['fullscreen', 'paragraph_format', 'image','link','align','table','lists','file']
]); ?>

    <!-- <?= $form->field($model, 'cat_id')->textInput() ?> -->

    <?php 
    // echo $form->field($model, 'image_featured')->widget(FileInput::classname(), [
    //     'options' => ['accept' => 'image/*'],
    //     'pluginOptions' => [
    //             'uploadUrl' => Url::to(['/news-article/upload-featured/']),
    //             'previewFileType' => 'image',
                // 'uploadExtraData' => [
                //     'album_id' => 20,
                //     'cat_id' => 'Nature'
                // ],
                // 'maxFileCount' => 10
            // ]
    // ]);?>

        <?php 
            echo '<br>';
            echo '<label class="control-label">Tanggal Tayang</label>';
            echo DatePicker::widget([
            'name' => 'timestamp_display_from',
            'value' => (empty($model->timestamp_display_from)) ? '0' : $model->timestamp_display_from,
            'type' => DatePicker::TYPE_RANGE,
            'name2' => 'timestamp_display_until',
            'value2' => (empty($model->timestamp_display_until)) ? '0' : $model->timestamp_display_until,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true,
            ]
            ]);
        ?>

    <!-- <?= $form->field($model, 'image_list')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'file_list')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'create_user_id')->textInput() ?> -->

    <!-- <?= $form->field($model, 'update_user_id')->textInput() ?> -->

    <!-- <?= $form->field($model, 'timestamp_create')->textInput() ?> -->

    <!-- <?= $form->field($model, 'timestamp_update')->textInput() ?> -->

    <!-- <?= $form->field($model, 'timestamp_display_from')->textInput() ?> -->

    <!-- <?= $form->field($model, 'timestamp_display_until')->textInput() ?> -->

    <!-- <?= $form->field($model, 'is_deleted')->textInput() ?> -->

    <!-- <?= $form->field($model, 'is_display_limit')->textInput() ?> -->

    <!-- <?= $form->field($model, 'teaser_text')->textarea(['rows' => 6]) ?> -->
    <br>
    <?= $form->field($model, 'regencies_id')->widget(Select2::className(), [
        'data' => $list_regencies,
        'options' => ['placeholder' => 'Pilih'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'geo_location')->widget(\kalyabin\maplocation\SelectMapLocationWidget::className(), [
        'attributeLatitude' => 'latitude',
        'attributeLongitude' => 'longitude',   
        'googleMapApiKey' => 'AIzaSyBnQsr7o5y07cK1uaqp0R7PkJ5pFOHL7yE',
        'draggable' => true,
        // Semarang 
        // -6.990649, 110.422982 
    ]); ?>
    <?php //echo Html::hiddenInput('geo_latitude', $model->geo_latitude); ?>
    <!-- <?= $form->field($model, 'latitude')->hiddenInput()->label(false); ?> -->
    <!-- <?= $form->field($model, 'longitude')->hiddenInput()->label(false); ?> -->

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>

<!-- Modal Show Image -->
<div class="modal fade" id="modal-default-image-featured">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span></button>
    <h4 class="modal-title"><?= $model->title ?></h4>
  </div>
  <div class="modal-body" align="center">
    <img src="<?php echo '/'.$model->image_featured; ?>" class="img-responsive">
  </div>
  <!-- <div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary">Save changes</button>
  </div> -->
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>

<style type="text/css" media="screen">
    #w0 > div.fr-box.royal-theme.fr-basic.fr-top > div.fr-wrapper > div:nth-child(1) > a {
    visibility: hidden;
}
</style>
