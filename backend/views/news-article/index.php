<?php

use hscstudio\mimin\components\Mimin;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News Articles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-article-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create News Article'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body" style="">
          <?= GridView::widget([
                'tableOptions' => [
                    'class' => 'table table-striped',
                ],
                'options' => [
                    'class' => 'table-responsive',
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'id',
                    // 'title:ntext',
                    [
                        'label' => 'Title',
                        'attribute' => 'title',
                        'format' => 'raw',
                        'value' => function ($data){
                            return Html::a($data->title,['view', 'id' => $data->id]);
                        },
                    ],
                    // 'content:ntext',
                    // 'cat_id',
                    [
                        'label' => 'Categories',
                        'attribute' => 'cat_id',
                        'value' => 'categories.title',
                    ],
                    // 'image_featured',
                    //'image_list:ntext',
                    //'file_list:ntext',
                    //'create_user_id',
                    //'update_user_id',
                    // 'timestamp_update:date',
                    //'timestamp_display_from:datetime',
                    //'timestamp_display_until:datetime',
                    // 'is_deleted',
                    [
                        'label' => 'Headline',
                        'attribute' => 'headline',
                        'format' => 'raw',
                        'value' => function ($model){
                            return Html::a($model->getHeadline($model->headline),['headline', 'id' => $model->id]);
                        },
                    ],
                    [
                        'label' => 'Publikasi',
                        'attribute' => 'is_deleted',
                        'format' => 'raw',
                        'value' => function ($model){
                            return Html::a($model->getStatus($model->is_deleted),['status', 'id' => $model->id]);
                        },
                    ],
                    'timestamp_create:datetime',
                    //'is_display_limit',
                    //'teaser_text:ntext',
                    // 'geo_location:ntext',
                    // 'geo_latitude',
                    // 'geo_longitude',

                    // ['class' => 'yii\grid\ActionColumn'],
                    [
                      'class' => 'yii\grid\ActionColumn',
                      'template' => Mimin::filterActionColumn([
                          'view','update'/*,'delete','status'*/
                      ],$this->context->route),
                    ],
                ],
            ]); ?>
        </div>
      </div>
    
    <?php Pjax::end(); ?>

</div>
