<?php

use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\NewsArticle */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="news-article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-warning']) : null ?>
    </p>
<div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'title:ntext',
            // 'content:ntext',
            [
                'label' => 'Content',
                'attribute' => 'content',
                'format' => 'raw',
                'value' => function ($data){
                    return $data->content;
                },
            ],
            // 'cat_id',
            [
                'label' => 'Category',
                'attribute' => 'cat_id',
                'value' => $model->categories->title,
            ],
            // 'image_featured',
            // 'image_list:ntext',
            // 'file_list:ntext',
            [
                'label' => 'Show Image',
                // 'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($data){
                    return '<p>
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default-image-featured">
                            Show Image
                            </button>
                            </p>';
                },
            ],
            [
                'label' => 'User Created',
                'attribute' => 'create_user_id',
                'value' => $model->userCreate->username,
            ],
            [
                'label' => 'User Updated',
                'attribute' => 'updated_user_id',
                'value' => $model->userUpdate->username,
            ],
            // 'create_user_id',
            // 'update_user_id',
            'timestamp_create:datetime',
            'timestamp_update:datetime',
            'timestamp_display_from:datetime',
            'timestamp_display_until:datetime',
            // 'is_deleted',
            [
                'label' => 'Headline',
                'attribute' => 'headline',
                'format' => 'raw',
                'value' => function ($model){
                    return Html::a($model->getHeadline($model->headline),['headline', 'id' => $model->id]);
                },
            ],
            [
                'label' => 'Publikasi',
                'attribute' => 'is_deleted',
                'format' => 'raw',
                'value' => function ($model){
                    return Html::a($model->getStatus($model->is_deleted),['status', 'id' => $model->id]);
                },
            ],
            // 'is_display_limit',
            // 'teaser_text:ntext',
            // 'geo_location',
        ],
    ]) 

    ?>
    </div>
</div>
    

    <!-- Modal Show Image -->
<div class="modal fade" id="modal-default-image-featured">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span></button>
    <h4 class="modal-title"><?= $model->title ?></h4>
  </div>
  <div class="modal-body" align="center">
    <img src="<?php echo (strstr($model->image_featured, 'http'))? $model->image_featured : Yii::$app->request->baseUrl.'/'.$model->image_featured; ?>" class="img-responsive">
  </div>
</div>
</div>
</div>

</div>


