<?php

use app\models\Categories;
use hscstudio\mimin\components\Mimin;
use kartik\tree\TreeView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-article-index">

    <h1><?= Html::encode($this->title) ?></h1>

<div class="box box-default">
	<div class="box-header with-border">
	  <h3 class="box-title">Categories</h3>

	  <div class="box-tools pull-right">
	    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	    </button>
	  </div>
	  
	</div>
	
	<div class="box-body">
	  <?php 		 
		echo TreeView::widget([
		    // single query fetch to render the tree
		    // use the Product model you have in the previous step
		    'query' => Categories::find()->addOrderBy('root, lft'), 
		    'headingOptions' => ['label' => 'Categories'],
		    'fontAwesome' => false,     // optional
		    'isAdmin' => true,         // optional (toggle to enable admin mode)
		    'displayValue' => 1,        // initial display value
		    'softDelete' => false,       // defaults to true
		    'cacheSettings' => [        
		        'enableCache' => true   // defaults to true
		    ]
		]);

	   ?>
	</div>
	
</div>

</div>
