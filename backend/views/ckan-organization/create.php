<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CkanOrganization */

$this->title = Yii::t('app', 'Create Ckan Organization');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ckan Organizations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ckan-organization-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_account' => $list_account,
    ]) ?>

</div>
