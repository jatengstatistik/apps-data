<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NewsCat */

$this->title = Yii::t('app', 'Create News Cat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News Cats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-cat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_cat' => $list_cat,
    ]) ?>

</div>
