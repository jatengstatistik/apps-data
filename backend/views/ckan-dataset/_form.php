<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CkanDatasetCount */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ckan-dataset-count-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'organization_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dataset_harvested_count')->textInput() ?>

    <?= $form->field($model, 'dataset_count')->textInput() ?>

    <?= $form->field($model, 'date_harvested')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'formated_date')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
