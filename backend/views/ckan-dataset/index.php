<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Statistik Dataset');
$this->params['breadcrumbs'][] = $this->title;

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="ckan-dataset-daily">
    <?= $this->render('_chart-datasets-daily', [
        'daily_dataset' => $daily_dataset,
    ]) ?>
</div>

<div class="ckan-dataset-monthly">
    <?= $this->render('_chart-datasets-monthly', [
        'monthly_dataset' => $monthly_dataset,
    ]) ?>
</div>
