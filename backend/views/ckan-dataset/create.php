<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CkanDatasetCount */

$this->title = Yii::t('app', 'Create Ckan Dataset Count');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ckan Dataset Counts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ckan-dataset-count-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
