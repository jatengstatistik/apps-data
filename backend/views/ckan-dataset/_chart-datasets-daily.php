<?php

use kartik\widgets\Select2;
use dosamigos\chartjs\ChartJs;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

 $bulan = [1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'];

for ($i = 2019; $i <= date('Y') ; $i++) {
    $year[$i] = $i;
}

?>
<div class="box box-default">
    <?php 
        // Formating Data Before show in chart
        foreach ($daily_dataset as $value) {
            $data['date'][] = 'Date '.$value['date'];
            $data['title'] = "Dataset Harian";
            $data['data'][] = $value['dataset'];
            $data['title_skpd'] = 'Dataset Harian SKPD';
            $data['data_skpd'][] = $value['dataset_skpd'];
            $data['title_kabko'] = 'Dataset Harian Kab / Kota';
            $data['data_kabko'][] = $value['dataset_kabko'];
        }
     ?>
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo "Dataset Harian Bulan ".$bulan[date('n',strtotime($daily_dataset[0]['date']))]." ".date('Y',strtotime($daily_dataset[0]['date'])) ?></h3>
      <div class="box-tools pull-right">
        <?php $form = ActiveForm::begin([
            'id' => 'search-date',
            'method' => 'get',
            'action' => ['ckan-dataset/index'],
        ]); ?>
        <div class="btn-group">
            <?= Select2::widget([
            'name' => 'month',
            'data' => $bulan,
            'options' => [
                'placeholder' => 'Bulan ',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?> 
        </div>
         
        <div class="btn-group">
            <?= Select2::widget([
            'name' => 'year',
            'data' => $year,
            'options' => [
                'placeholder' => 'Tahun ',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>

         <?= Html::submitButton("Cari", ['class' => "btn"]); ?>
        
        <?php ActiveForm::end(); ?>
      </div>
    </div>
    <div class="box-body" style="">                   
    <?php 
        echo ChartJs::widget([
                'type' => 'bar',
                'data' => [
                    'labels' => $data['date'],
                    'datasets' => [
                        [
                            'label' => $data['title'],
                            'backgroundColor' => "rgba(".$r= rand(0,255).",".$g= rand(0,255).",".$b= rand(0,255).",".$opacity=0.8.")",
                            'borderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                            'pointBackgroundColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                            'pointBorderColor' => "#fff",
                            'pointHoverBackgroundColor' => "#fff",
                            'pointHoverBorderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                            'data' => $data['data'],
                        ],
                        [
                            'label' => $data['title_kabko'],
                            'backgroundColor' => "rgba(".$r= rand(0,255).",".$g= rand(0,255).",".$b= rand(0,255).",".$opacity=0.8.")",
                            'borderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                            'pointBackgroundColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                            'pointBorderColor' => "#fff",
                            'pointHoverBackgroundColor' => "#fff",
                            'pointHoverBorderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                            'data' => $data['data_kabko'],
                        ],
                        [
                            'label' => $data['title_skpd'],
                            'backgroundColor' => "rgba(".$r= rand(0,255).",".$g= rand(0,255).",".$b= rand(0,255).",".$opacity=0.8.")",
                            'borderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                            'pointBackgroundColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                            'pointBorderColor' => "#fff",
                            'pointHoverBackgroundColor' => "#fff",
                            'pointHoverBorderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                            'data' => $data['data_skpd'],
                        ],

                    ],
                ],
            ]);
        ?>
    </div>
</div>
