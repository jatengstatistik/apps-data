<?php

use app\models\CkanDatasetCount;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CkanDatasetCountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Statistik Dataset SKPD (Update : '.date('d-M-Y',strtotime($date)).')');
$this->params['breadcrumbs'][] = $this->title;
?>
<br>
<p>
<!-- <?php //echo ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-warning']) : null ?> -->
</p>
<div class="ckan-dataset-count-index">
    <?php Pjax::begin(); ?>
    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
    <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showFooter' => true,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                // 'id',
                // 'organization_id',
                // 'organizaton.name',
                [
                    'label' => 'Organzation',
                    'attribute' => 'organization_id',
                    'format' => 'raw',
                    'value' => function ($data){
                        return Html::a($data->organization->title,$retVal = (empty($data->organization->ckanuser['site'])) ? 'http://data.jatengprov.go.id/organization/'.$data->organization->name : $data->organization->ckanuser['site'].'/dataset',['target'=>'_blank']);
                    },
                    'footer' => 'Total Data',
                ],
                // 'dataset_harvested_count',
                // [
                //     'label'     =>  'Data Kab/Kota',
                //     'attribute' => 'dataset_harvested_count',
                //     'footer' => CkanDatasetCount::getTotal($dataProvider->models, 'dataset_harvested_count'),
                // ],
                // 'dataset_count',
                // [
                //     'label'     => 'Data Instansi',
                //     'attribute' => 'dataset_count',
                //     'footer' => CkanDatasetCount::getTotal($dataProvider->models, 'dataset_count'),
                // ],
                // 'dataset_total',
                [
                    'attribute' => 'dataset_total',
                    'footer' => CkanDatasetCount::getTotal($dataProvider->models, 'dataset_total'),
                ],
                // 'date_harvested',
                // 'updated_at',
                // [
                //     'label' => 'Last Update',
                //     'attribute' => 'updated_at',
                //     'format' => 'raw',
                //     'value' => function ($data){
                //         return date('d-m-Y H:i:s',$data->updated_at);
                //     },
                // ],
                // [
                //  'attribute' =>'dataset_total',
                //  'footer' => CkanDatasetCount::getTotal($dataProvider->models, 'dataset_total'),
                // ],
                //'formated_date',

                // ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        </div>
    </div>

    <?php Pjax::end(); ?>

</div>