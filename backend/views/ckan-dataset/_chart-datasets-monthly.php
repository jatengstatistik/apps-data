<?php
use kartik\widgets\Select2;
use dosamigos\chartjs\ChartJs;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

for ($i = 2019; $i <= date('Y') ; $i++) {
    $year[$i] = $i;
}

?>
<div class="box box-default">
    <?php 
        // Formating Data Before show in chart
        foreach ($monthly_dataset as $value) {
            $dataMonthly['date'][] = 'Date '.$value['date_month_range'];
            $dataMonthly['title'] = "Dataset Bulanan";
            $dataMonthly['data'][] = $value['dataset'];
            $dataMonthly['title_skpd'] = 'Dataset Bulanan SKPD';
            $dataMonthly['data_skpd'][] = $value['dataset_skpd'];
            $dataMonthly['title_kabko'] = 'Dataset Bulanan Kab / Kota';
            $dataMonthly['data_kabko'][] = $value['dataset_kabko'];
        }
     ?>
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo "Dataset Bulanan Tahun ".date('Y',strtotime($monthly_dataset[0]['date_month_range'])) ?></h3>
      <div class="box-tools pull-right">
        <?php $form = ActiveForm::begin([
            'id' => 'search-date',
            'method' => 'get',
            'action' => ['ckan-dataset/index'],
        ]); ?>         
        <div class="btn-group">
            <?= Select2::widget([
            'name' => 'year',
            'data' => $year,
            'options' => [
                'placeholder' => 'Tahun ',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>

         <?= Html::submitButton("Cari", ['class' => "btn"]); ?>
        
        <?php ActiveForm::end(); ?>
        </button>
      </div>
    </div>
    <div class="box-body" style="">                   
    <?php echo ChartJs::widget([
            'type' => 'bar',
            'data' => [
                'labels' => $dataMonthly['date'],
                'datasets' => [
                    [
                        'label' => $dataMonthly['title'],
                        'backgroundColor' => "rgba(".$r= rand(0,255).",".$g= rand(0,255).",".$b= rand(0,255).",".$opacity=0.8.")",
                        'borderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                        'pointBackgroundColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                        'pointBorderColor' => "#fff",
                        'pointHoverBackgroundColor' => "#fff",
                        'pointHoverBorderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                        'data' => $dataMonthly['data'],
                    ],
                    [
                        'label' => $dataMonthly['title_kabko'],
                        'backgroundColor' => "rgba(".$r= rand(0,255).",".$g= rand(0,255).",".$b= rand(0,255).",".$opacity=0.8.")",
                        'borderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                        'pointBackgroundColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                        'pointBorderColor' => "#fff",
                        'pointHoverBackgroundColor' => "#fff",
                        'pointHoverBorderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                        'data' => $dataMonthly['data_kabko'],
                    ],
                    [
                        'label' => $dataMonthly['title_skpd'],
                        'backgroundColor' => "rgba(".$r= rand(0,255).",".$g= rand(0,255).",".$b= rand(0,255).",".$opacity=0.8.")",
                        'borderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                        'pointBackgroundColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                        'pointBorderColor' => "#fff",
                        'pointHoverBackgroundColor' => "#fff",
                        'pointHoverBorderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                        'data' => $dataMonthly['data_skpd'],
                    ],

                ],
            ],
        ]);
        ?>
    </div>
</div>
