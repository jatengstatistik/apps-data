<?php

use app\models\CkanDatasetCount;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CkanDatasetCountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dataset Statistics (Update : '.date('d-M-Y',strtotime($date)).')');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ckan-dataset-count-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!-- <?= Html::a(Yii::t('app', 'Create Ckan Dataset'), ['create'], ['class' => 'btn btn-success']) ?> -->
        <?= Html::a(Yii::t('app', 'Instansi Dataset Update'), ['console/count-dataset'], ['class' => 'btn btn-warning','target'=>'_blank']) ?>
        <?= Html::a(Yii::t('app', 'Kab/Kota Dataset Update'), ['console/count-dataset-kab'], ['class' => 'btn btn-warning','target'=>'_blank']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php 
        // echo ExportMenu::widget([
        //     'dataProvider' => $dataProvider,
        //     'columns' => $gridColumns,
        //     'dropdownOptions' => [
        //         'label' => 'Export All',
        //         'class' => 'btn btn-secondary'
        //     ]
        // ]) . "<hr>\n".
     ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'organization_id',
            // 'organizaton.name',
            [
                'label' => 'Organzation',
                'attribute' => 'organization_id',
                'format' => 'raw',
                'value' => function ($data){
                    return Html::a($data->organization->title,$retVal = (empty($data->organization->ckanuser['site'])) ? 'http://data.jatengprov.go.id/organization/'.$data->organization->name : $data->organization->ckanuser['site'].'/dataset');
                },
                'footer' => 'Total Data',
            ],
            // 'dataset_harvested_count',
            [
                'label'     =>  'Data Kab/Kota',
                'attribute' => 'dataset_harvested_count',
                'footer' => CkanDatasetCount::getTotal($dataProvider->models, 'dataset_harvested_count'),
            ],
            // 'dataset_count',
            [
                'label'     => 'Data Instansi',
                'attribute' => 'dataset_count',
                'footer' => CkanDatasetCount::getTotal($dataProvider->models, 'dataset_count'),
            ],
            // 'dataset_total',
            [
                'attribute' => 'dataset_total',
                'footer' => CkanDatasetCount::getTotal($dataProvider->models, 'dataset_total'),
            ],
            // 'date_harvested',
            // 'updated_at',
            // [
            //     'label' => 'Last Update',
            //     'attribute' => 'updated_at',
            //     'format' => 'raw',
            //     'value' => function ($data){
            //         return date('d-m-Y H:i:s',$data->updated_at);
            //     },
            // ],
            // [
            //  'attribute' =>'dataset_total',
            //  'footer' => CkanDatasetCount::getTotal($dataProvider->models, 'dataset_total'),
            // ],
            //'formated_date',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
