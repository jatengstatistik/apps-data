<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AccountsOdsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accounts-ods-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'regencies_id') ?>

    <?= $form->field($model, 'site') ?>

    <?= $form->field($model, 'host') ?>

    <?= $form->field($model, 'port') ?>

    <?php // echo $form->field($model, 'ssh_username') ?>

    <?php // echo $form->field($model, 'ssh_password') ?>

    <?php // echo $form->field($model, 'sysadmin_user') ?>

    <?php // echo $form->field($model, 'sysadmin_password') ?>

    <?php // echo $form->field($model, 'notes') ?>

    <?php // echo $form->field($model, 'file_batch') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
