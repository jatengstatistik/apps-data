<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\AccountsOds */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Accounts Ods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php Pjax::begin(); ?>
<div class="accounts-ods-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Create Users CKAN'), ['create-users', 'id' => $model->id], [
            'class' => 'btn btn-warning',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to Create This Users?'),
            ],
        ]) ?>
        <?= Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            // 'regencies_id',
             [
                'label' => 'Regencies',
                'attribute' => 'regencies_id',
                'value' => $model->regencies->name,
            ],
            'site',
            'host',
            'port',
            'ssh_username',
            'ssh_password',
            // 'sysadmin_user',
            // 'sysadmin_password',
            // 'notes:ntext',
            'file_batch',
            // 'created_at:ntext',
            // 'updated_at:ntext',
        ],
    ]) ?>

</div>

 <div class="data-storage-index">

    
    <?php 
        // echo '<pre>';
        // print_r($dataProvider);
        // echo '</pre>';

        // array_shift($attribute);
     ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $attribute,
    ]); ?>

    

</div>
<?php Pjax::end(); ?>
