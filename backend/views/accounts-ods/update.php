<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AccountsOds */

$this->title = Yii::t('app', 'Update Accounts Ods: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Accounts Ods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="accounts-ods-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_regencies' => $list_regencies,
    ]) ?>

</div>
