<?php

use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AccountsOds */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accounts-ods-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'regencies_id')->textInput(['maxlength' => true]) ?> -->
    <?= $form->field($model, 'regencies_id')->widget(Select2::className(), [
        'data' => $list_regencies,
        'options' => ['placeholder' => 'Pilih'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'site')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'host')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'api_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'port')->textInput() ?>

    <?= $form->field($model, 'ssh_username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ssh_password')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sysadmin_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sysadmin_password')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

    <!-- <?= $form->field($model, 'file_batch')->textInput(['maxlength' => true]) ?> -->
    <?= $form->field($model, 'file_batch')->fileInput() ?>

    <!-- <?= $form->field($model, 'created_at')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'updated_at')->textarea(['rows' => 6]) ?> -->

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
