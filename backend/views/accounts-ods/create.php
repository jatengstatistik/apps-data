<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AccountsOds */

$this->title = Yii::t('app', 'Create Accounts Ods');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Accounts Ods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accounts-ods-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_regencies' => $list_regencies,
    ]) ?>

</div>
