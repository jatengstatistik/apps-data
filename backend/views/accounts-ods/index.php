<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use hscstudio\mimin\components\Mimin;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AccountsOdsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Accounts Ods');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Pjax::begin(); ?>
<div class="accounts-ods-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Accounts Ods'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'regencies_id',
            // [
            //     'label' => 'Regencies',
            //     'attribute' => 'regencies_id',
            //     'value' => 'regencies.name',
            // ],
            [
                'label' => 'Regencies',
                'attribute' => 'regencies_id',
                'format' => 'raw',
                'value' => function ($data){
                    return Html::a($data->regencies->name,['view', 'id' => $data->id]);
                },
            ],
            // 'site',
            [
                'label' => 'Alamat Situs',
                'attribute' => 'site',
                'format' => 'url',
                'value' => 'site',
            ],
            'host',
            'port',
            'ssh_username',
            //'ssh_password',
            //'sysadmin_user',
            //'sysadmin_password',
            'notes:ntext',
            //'file_batch',
            //'created_at:ntext',
            //'updated_at:ntext',

            // ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => Mimin::filterActionColumn([
                'view','users-view','update','delete'
                ],$this->context->route),
                // 'template' => '{users-view} {view} {update} {delete}',
                'buttons' => [
                    'users-view' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-user"></span>',
                            // 'format' => 'raw',
                            ['users-view', 'id' => $model->id],
                            [
                                'title' => 'Users Create',
                                // 'data-pjax' => '0',
                                // 'target' => '_blank',
                            ]
                        );
                    },
                ],
            ]
        ],
    ]); ?>

    

</div>
<?php Pjax::end(); ?>
