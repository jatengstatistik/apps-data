<?php

use dosamigos\chartjs\ChartJs;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\VisitorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Visitors');
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="visitor-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php // Html::a(Yii::t('app', 'Create Visitor'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
     <?php 
        
        // echo '<pre>';
        // print_r($visitorToday);
        // echo '<br>';
        // print_r($visitorWeekly);
        // echo '<br>';
        // print_r($visitorMonthly);
        // echo '<br>';
        // print_r($visitorYearlyInMonth);
        // echo '<br>';
        // print_r($dataVisitMonthly['data']);
        // echo '<br>';
        // print_r($visitorQuarterly);
        // echo '</pre>';
        

        
     ?>
    <div class="row">
        <div class="col-lg-offset-5  col-lg-2">
            <div class="small-box bg-green">
                <div class="inner">
                  <h3><?= number_format($visitorToday['counter']) ?></h3>
                  <p>Today Visitor</p>
                </div>
                <div class="icon">
                  <i class="fa fa-user"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-default">
        <?php 

            // Formating Data Before show in chart
            foreach ($visitorWeekly as $value) {
                $dataVisitWeekly['title'] = 'Data Visitor Weekly in '.date('F Y');
                $dataVisitWeekly['week'][] = 'Week '.$value['week'];
                $dataVisitWeekly['data'][] = $value['counter'];
                
            }
         ?>
        <div class="box-header with-border">
          <h3 class="box-title"><?= $dataVisitWeekly['title']?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body" style="">                   
        <?php
        echo ChartJs::widget([
                'type' => 'bar',
                'data' => [
                    'labels' => $dataVisitWeekly['week'],
                    'datasets' => [
                        [
                            'label' => $dataVisitWeekly['title'],
                            'backgroundColor' => "rgba(179,181,198,0.2)",
                            'borderColor' => "rgba(179,181,198,1)",
                            'pointBackgroundColor' => "rgba(179,181,198,1)",
                            'pointBorderColor' => "#fff",
                            'pointHoverBackgroundColor' => "#fff",
                            'pointHoverBorderColor' => "rgba(179,181,198,1)",
                            'data' => $dataVisitWeekly['data']
                        ]
                    ]
                ]
            ]);
            ?>
        </div>
    </div>
    <div class="box box-default">
        <?php 
            // Formating Data Before show in chart
            foreach ($visitorMonthly as $value) {
                $dataVisitMonthly['title'] = 'Data Visitor Monthly in '.date('Y');
                $dataVisitMonthly['month'][] = date('F',strtotime($value['month_range']));
                $dataVisitMonthly['data'][] = $value['counter'];
                
            }
         ?>
        <div class="box-header with-border">
          <h3 class="box-title"><?= $dataVisitMonthly['title']?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body" style="">                   
                <?php
                echo ChartJs::widget([
                        'type' => 'line',
                        'data' => [
                            'labels' => $dataVisitMonthly['month'],
                            'datasets' => [
                                [
                                    'label' => $dataVisitMonthly['title'],
                                    'backgroundColor' => "rgba(179,181,198,0.2)",
                                    'borderColor' => "rgba(179,181,198,1)",
                                    'pointBackgroundColor' => "rgba(179,181,198,1)",
                                    'pointBorderColor' => "#fff",
                                    'pointHoverBackgroundColor' => "#fff",
                                    'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                    'data' => $dataVisitMonthly['data']
                                ]
                            ]
                        ]
                    ]);
                    ?>
            </div>
        </div>
    <div class="box box-default">
        <?php 
            // Formating Data Before show in chart
            foreach ($visitorQuarterly as $value) {
                $visitorQuarterly['title'] = 'Data Visitor Quarterly In '.date('Y');
                $visitorQuarterly['month'][] = $value['month_range'];
                $visitorQuarterly['data'][] = $value['counter'];
                
            }
         ?>
        <div class="box-header with-border">
          <h3 class="box-title"><?= $visitorQuarterly['title']?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body" style="">                   
        <?php
        echo ChartJs::widget([
                'type' => 'bar',
                'data' => [
                    'labels' => $visitorQuarterly['month'],
                    'datasets' => [
                        [
                            'label' => $visitorQuarterly['title'],
                            'backgroundColor' => "rgba(179,181,198,0.2)",
                            'borderColor' => "rgba(179,181,198,1)",
                            'pointBackgroundColor' => "rgba(179,181,198,1)",
                            'pointBorderColor' => "#fff",
                            'pointHoverBackgroundColor' => "#fff",
                            'pointHoverBorderColor' => "rgba(179,181,198,1)",
                            'data' => $visitorQuarterly['data'],
                        ]
                    ]
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="box box-default">
        <?php 

        function randomColor(){
            $result = array('rgb' => '', 'hex' => '');
            foreach(array('r', 'b', 'g') as $col){
                $rand = mt_rand(0, 255);
                $result['rgb'][$col] = $rand;
                $dechex = dechex($rand);
                if(strlen($dechex) < 2){
                    $dechex = '0' . $dechex;
                }
                $result['hex'] .= $dechex;
            }
            return $result;
        }
            // Formating Data Before show in chart
            foreach ($visitorYearlyInMonth as $year => $visitorYear) {
                // echo '<pre>';
                // echo $year;
                // print_r($visitorYear);
                // echo '</pre>';

                foreach ($visitorYear as $visitorDataValue) {
                    $visitorDValue['month'][$year][] = date('F',strtotime($visitorDataValue['month_range']));
                    $visitorDValue['data'][$year][] = $visitorDataValue['counter'];
                }

                $r = rand(0,255); $g = rand(0,255); $b = rand(0,255); $opacity = 0.4;

                $visitorYearlyData[] = [
                    'backgroundColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                    'borderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                    'pointBackgroundColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                    'pointBorderColor' => "#fff",
                    'pointHoverBackgroundColor' => "#fff",
                    'pointHoverBorderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
                    'label' => $year,
                    'data' => $visitorDValue['data'][$year],
                ];
            }

            // echo '<pre>';
            // print_r($visitorYearlyData);
            // echo '<br>';
            // print_r($visitorDValue['data']);
            // echo '</pre>';
         ?>
        <div class="box-header with-border">
          <h3 class="box-title">Visitor Every Years</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body" style="">                   
        <?php
        echo ChartJs::widget([
                'type' => 'line',
                'data' => [
                    'labels' => $visitorDValue['month']['2018'],
                    'datasets' => $visitorYearlyData,
                ]
            ]);
            ?>
        </div>
    </div>
    

    <?php Pjax::end(); ?>

</div>
