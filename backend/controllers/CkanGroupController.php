<?php

namespace backend\controllers;

use Yii;
use app\models\CkanGroup;
use app\models\CkanGroupSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CkanGroupController implements the CRUD actions for CkanGroup model.
 */
class CkanGroupController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CkanGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CkanGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CkanGroup model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CkanGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CkanGroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CkanGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CkanGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CkanGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CkanGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CkanGroup::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionAddGroup()
    {
        // public function action($host='',$action='',$data='',$method='',$auth='')
        // $site = 'http://data.jatengprov.go.id';
        // $action = 'group_list_authz';
        // $method = 'GET';
        // $api_key = '94106015-00ef-41d1-b750-2dbae360d054';
        // $group = Yii::$app->CkanApi->action($site,$action,'',$method,$api_key);
        // foreach ($group->result as $value) {
        //     $model = CkanGroup::find()->where(['id_group' => $value['id']])->one();
        //     $model->id_group = $value['id'];
        //     $model->title = $value['title'];
        //     $model->name = $value['name'];
        //     $model->description = $value['description'];
        //     $model->display_name = $value['display_name'];
        //     $model->image_display_url = $value['image_display_url'];
        //     $model->image_url = $value['image_url'];
        //     $model->package_count = $value['package_count'];
        //     $model->updated_at = time();
        //     $model->save();
        // }
        // var_dump($get_data);
        // echo '<pre>';
        // print_r($group->result);
        // echo '</pre>';
    }
}
