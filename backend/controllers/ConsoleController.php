<?php

namespace backend\controllers;

use Yii;
use app\models\CkanDatasetCount;
use app\models\CkanOrganization;
use app\models\NewsArticle;
use app\models\SiteVariable;
use yii\httpclient\Client;

class ConsoleController extends \yii\web\Controller
{
    public function actionCreateSlug()
    {
        $news = NewsArticle::find()->all();
        foreach ($news as $value) {
            if (empty($value->slug)) {
                $change_slug = NewsArticle::findOne($value->id);
                $change_slug->slug = str_replace(' ', '-', strtolower($value->title));
                $change_slug->save(false);
            }
        }
    }

    public function is_json($string, $return_data = false)
    {
        $data = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
    }

    public function stringToSlug($str)
    {
        $slug = preg_replace('@[\s!:;_\?=\\\+\*/%&#]+@', '-', $str);
        //this will replace all non alphanumeric char with '-'
        $slug = mb_strtolower($slug);
        //convert string to lowercase
        return $slug = trim($slug, '-');
        //trim whitespaces
    }

    public function actionSalatigaku()
    {
        $client = new Client(['baseUrl' => 'http://salatigaku.salatiga.go.id']);
        // Salatigaku Kuliner
        $kuliner = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('/webservice/service/kuliner')
            ->setData(['token' => md5('salatigaku')])
            ->send();
        if ($kuliner->isOk) {
            $get_kuliner = $kuliner->getData();
            foreach ($get_kuliner['datalist'] as $value) {
                $dta_kuliner[] = [
                    'regencies_id' => '3373',
                    'title' => $value['nama'],
                    'slug' => $this->stringToSlug($value['nama']),
                    'content' => $value['deskripsi'],
                    'cat_id' => '3',
                    'image_featured' => ($this->is_json($value['foto'])) ? $client->baseUrl . '/images/kuliner/' . json_decode($value['foto'])[0] : $client->baseUrl . '/images/kuliner/' . $value['foto'],
                    'timestamp_display_from' => '0',
                    'timestamp_display_until' => '0',
                    'geo_latitude' => $value['latitude'],
                    'geo_longitude' => $value['longitude'],
                ];
            }
        }

        $wisata = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('/webservice/service/wisata')
            ->setData(['token' => md5('salatigaku')])
            ->send();
        if ($wisata->isOk) {
            $get_wisata = $wisata->getData();
            foreach ($get_wisata['datalist'] as $value) {
                $dta_wisata[] = [
                    'regencies_id' => '3373',
                    'title' => $value['nama'],
                    'slug' => $this->stringToSlug($value['nama']),
                    'content' => $value['deskripsi'],
                    'cat_id' => '2',
                    'image_featured' => ($this->is_json($value['foto'])) ? $client->baseUrl . '/images/wisata/' . json_decode($value['foto'])[0] : $client->baseUrl . '/images/wisata/' . $value['foto'],
                    'timestamp_display_from' => '0',
                    'timestamp_display_until' => '0',
                    'geo_latitude' => $value['latitude'],
                    'geo_longitude' => $value['longitude'],
                ];
            }
        }

        $event = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('/webservice/service/event')
            ->setData(['token' => md5('salatigaku')])
            ->send();
        if ($event->isOk) {
            $get_event = $event->getData();
            foreach ($get_event['datalist'] as $value) {
                $dta_event[] = [
                    'regencies_id' => '3373',
                    'title' => $value['nama'],
                    'slug' => $this->stringToSlug($value['nama']),
                    'content' => $value['deskripsi'],
                    'cat_id' => '13',
                    'image_featured' => ($this->is_json($value['foto'])) ? $client->baseUrl . '/images/event/' . json_decode($value['foto'])[0] : $client->baseUrl . '/images/event/' . $value['foto'],
                    'timestamp_display_from' => strtotime($value['wkt_mulai']),
                    'timestamp_display_until' => strtotime($value['wkt_selesai']),
                    'geo_latitude' => $value['latitude'],
                    'geo_longitude' => $value['longitude'],
                ];
            }
        }

        $data_news = array_merge($dta_kuliner, $dta_wisata, $dta_event);

        foreach ($data_news as $value) {
            $news_slug_exist = NewsArticle::find()->where(['title' => $value['title']])->one();
            if (empty($news_slug_exist)) {
                $model = new NewsArticle();
                $model->regencies_id = $value['regencies_id'];
                $model->title = $value['title'];
                $model->slug = $value['slug'];
                $model->content = $value['content'];
                $model->cat_id = $value['cat_id'];
                $model->image_featured = $value['image_featured'];
                $model->timestamp_display_from = $value['timestamp_display_from'];
                $model->timestamp_display_until = $value['timestamp_display_until'];
                $model->geo_latitude = $value['geo_latitude'];
                $model->geo_longitude = $value['geo_longitude'];
                $model->save();
                // echo "done";
            } else {
                $news_slug_exist->regencies_id = $value['regencies_id'];
                $news_slug_exist->content = $value['content'];
                $news_slug_exist->cat_id = $value['cat_id'];
                $news_slug_exist->image_featured = $value['image_featured'];
                $news_slug_exist->timestamp_display_from = $value['timestamp_display_from'];
                $news_slug_exist->timestamp_display_until = $value['timestamp_display_until'];
                $news_slug_exist->geo_latitude = $value['geo_latitude'];
                $news_slug_exist->geo_longitude = $value['geo_longitude'];
                $news_slug_exist->save();
            }
        }

        // echo "<pre>";
        // print_r($news);
        // echo "<br>";
        // print_r($dta_kuliner);
        // echo "</pre>";
    }

    public function actionGeojson()
    {
        $client = new Client(['baseUrl' => 'http://cjip.jatengprov.go.id/api/']);
        // Investasi
        $invest = $client->createRequest()
            ->setMethod('GET')
            ->setUrl('peluang-potensi-investasi-jawa-tengah')
            ->send();
        if ($invest->isOk) {
            $get_invest = $invest->getData();
            foreach ($get_invest['features'] as $key => $value) {
                // $value[] = array_replace($value['geometry'],$value['geometry']['coordinates' => [$value['geometry']['coordinates']['lat'],$value['geometry']['coordinates']['lng']]]);
                // foreach ($value['geometry'] as $geometry) {
                // $value[] = array_merge($geometry,['coordinates' => [$value['geometry']['coordinates']['lat'],$value['geometry']['coordinates']['lng']]]);
                //     print_r($geometry);
                // }
            }
        }
    }

    // function bulanIndo($nobulan='')
    // {
    //     $bulan = array ('Januari',
    //             'Februari',
    //             'Maret',
    //             'April',
    //             'Mei',
    //             'Juni',
    //             'Juli',
    //             'Agustus',
    //             'September',
    //             'Oktober',
    //             'November',
    //             'Desember'
    //         );
    //     return $bulan[$nobulan-1];
    // }

    public function actionBps()
    {
        // https://webapps.bps.go.id/jateng/api/v1/dashboard/in
        $client = new Client(['baseUrl' => 'https://webapps.bps.go.id/jateng/api/v1/']);
        $dashboard = $client->createRequest()
            ->setMethod('GET')
            ->setUrl('dashboard/in')
            ->send();
        if ($dashboard->isOk) {
            $valdashboard = $dashboard->getData();
            foreach ($valdashboard['data'] as $value) {
                $dibanding = (empty($value['sebelumnya'])) ? '' : 'dibanding periode lalu ' . $value['sebelumnya'];
                $indikator[$value['id']] = [
                    'name'  => $value['indikator'],
                    'now' => $value['bulan'] . ' ' . $value['tahun'],
                    'value' => $value['nilai'],
                    'mark'  => $value['tanda'],
                    'change'  => $value['poin'],
                    'before'  => $value['sebelumnya'],
                    // 'image'  => '<a href="/uploads/ots_img/'.$value['id'].'.png">File</a>',
                    'image'  => '/uploads/ots_img/' . $value['id'] . '.png',
                    'unit'  => $value['satuan'],
                    'desc'  => $value['indikator'] . ' ' . $value['bulan'] . ' ' . $value['tahun'] . ' ' . $value['tanda'] . ' ' . $value['poin'] . ' ' . $value['delta'] . ' ' . $dibanding,
                ];
            }
        }

        echo '<pre>';
        // print_r($vnaker);
        // echo '<br>';
        print_r($indikator);
        // echo '</pre>';
    }

    function whetherCodeBmkg($code = '')
    {
        switch ($code) {
                // case ($code == '0'||$code == '100'):
            case (0):
                return 'Cerah';
                break;
                // case ($code == '1'||$code == '101'):
            case (1):
                return 'Cerah Berawan';
                break;
            case (2):
                return 'Cerah Berawan';
                break;
            case (3):
                return 'Berawan';
                break;
            case (4):
                return 'Berawan Tebal';
                break;
            case (5):
                return 'Udara Kabur';
                break;
            case (10):
                return 'Asap';
                break;
            case (45):
                return 'Kabut';
                break;
            case (60):
                return 'Hujan Ringan';
                break;
            case (61):
                return 'Hujan Sedang';
                break;
            case (63):
                return 'Hujan Lebat';
                break;
            case (80):
                return 'Hujan Lokal';
                break;
            case (95):
                return 'Hujan Petir';
                break;
            case (97):
                return 'Hujan Petir';
                break;
            default:
                return null;
                break;
        }
        // http://data.bmkg.go.id/prakiraan-cuaca/#kodecuaca
        // 0 / 100 : Cerah / Clear Skies
        // 1 / 101 : Cerah Berawan / Partly Cloudy
        // 2 / 102 : Cerah Berawan / Partly Cloudy
        // 3 / 103 : Berawan / Mostly Cloudy
        // 4 / 104 : Berawan Tebal / Overcast
        // 5 : Udara Kabur / Haze
        // 10 : Asap / Smoke
        // 45 : Kabut / Fog
        // 60 : Hujan Ringan / Light Rain
        // 61 : Hujan Sedang / Rain
        // 63 : Hujan Lebat / Heavy Rain
        // 80 : Hujan Lokal / Isolated Shower
        // 95 : Hujan Petir / Severe Thunderstorm
        // 97 : Hujan Petir / Severe Thunderstorm
    }

    function currentWhether($whethers = '')
    {
        date_default_timezone_set("Asia/Jakarta");
        $time = date('d-m-Y H:i:s', strtotime(date('d-m-Y H:i:s')));
        switch ($time) {
            case ((strtotime($time) >= strtotime(date('d-m-Y 07:01:00'))) && (strtotime($time) <= strtotime(date('d-m-Y 13:00:00')))):
                $whether = $whethers['0'];
                return $this->whetherCodeBmkg($whether['value']);
                break;
            case ((strtotime($time) >= strtotime(date('d-m-Y 13:01:00'))) && (strtotime($time) <= strtotime(date('d-m-Y 19:00:00')))):
                $whether = $whethers['1'];
                return $this->whetherCodeBmkg($whether['value']);
                break;
            case ((strtotime($time) >= strtotime(date('d-m-Y 19:01:00'))) && (strtotime($time) <= strtotime(date('d-m-Y 01:00:00') . '+1 day'))):
                $whether = $whethers['2'];
                return $this->whetherCodeBmkg($whether['value']);
                break;
            case ((strtotime($time) >= strtotime(date('d-m-Y 01:01:00'))) && (strtotime($time) <= strtotime(date('d-m-Y 07:00:00')))):
                $whether = $whethers['3'];
                return $this->whetherCodeBmkg($whether['value']);
                break;
            default:
                return null;
                break;
        }
    }

    // http://dataweb.bmkg.go.id/MEWS/DigitalForecast/DigitalForecast-JawaTengah.xml
    public function actionWhether()
    {
        $nowTime = date('d-M-Y H:i:s', strtotime(date('d-M-Y H:i:s') . "+7hours"));
        $client = new Client(['baseUrl' => 'http://dataweb.bmkg.go.id/']);
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl('MEWS/DigitalForecast/DigitalForecast-JawaTengah.xml')
            ->send();
        if ($response->isOk) {
            $response_data = $response->getData();
            // Save to DB Prevent Error Access API BMKG
            $saveData = SiteVariable::find(1)->one();
            $saveData->site_whether = json_encode($response_data);
            $saveData->save(false);
        } else {
            // get data From DB last active API
            $savedData = SiteVariable::find(1)->one()->site_whether;
            $response_data = json_decode($savedData, 'true');
        }

        $data_cuaca = $response_data['forecast']['area'];
        foreach ($data_cuaca as $value) {
            if (!empty($value['parameter']['6']['timerange'])) {
                $whethers[] = $value['name']['1'] . ' - ' . $this->currentWhether($value['parameter']['6']['timerange']);
            }
        }

        return "Prakiraan Cuaca Jawa Tengah " . $nowTime . '  ' . implode(', ', $whethers) . " Sumber: BMKG (Badan Meteorologi, Klimatologi, dan Geofisika)";
    }

    // http://datadisplay.bmkg.go.id/XML/WarningCuaca.xml
    public function actionWarningWhether()
    {
        $client = new Client(['baseUrl' => 'http://datadisplay.bmkg.go.id/XML']);
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl('WarningCuaca.xml')
            ->send();
        // if ($response->isOk) {
        //     $response_data = $response->getData();
        //     // Save to DB Prevent Error Access API BMKG
        //     $saveData = SiteVariable::find(1)->one();
        //     $saveData->site_warning_whether = json_encode($response_data);
        //     $saveData->save(false);
        // } else {
        //     // get data From DB last active API
        //     $savedData = SiteVariable::find(1)->one()->site_warning_whether;
        //     $response_data = json_decode($savedData, 'true');
        // }
        // $data_cuaca = $response_data['info']['data'];
        // foreach ($data_cuaca as $value) {
        //     if (strpos($value['headline'], "Jawa Tengah") !== false) {
        //         return $value['headline'] . ' - ' . $value['description'];
        //     } else {
        //         return null;
        //     }
        // }

        echo "<pre>";
        print_r($response);
        echo "</pre>";
    }

    function isDateRange($date_from_user, $start_date, $end_date)
    {
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($date_from_user);

        // Check that user date is between start & end
        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }

    public function actionDeactiveOutDateArticle()
    {
        $today = date('d-m-Y');
        $article = NewsArticle::find()->where(['not in', 'timestamp_display_from', ['0']])->all();
        foreach ($article as $value) {
            if (!$this->isDateRange($today, $value->timestamp_display_from, $value->timestamp_display_until)) {
                if ($value->is_deleted != 1) {
                    $deactive_article = NewsArticle::find($value->id);
                    $deactive_article->is_deleted = '1';
                    $deactive_article->save(false);
                    echo $value->id . '.U-';
                }
            } else {
                echo $value->id . '.N-';
            }
        }
        // echo '<pre>';
        // print_r($article);
        // echo '<br>';
        // print_r($today);
        // echo '</pre>';
    }

    public function actionCountDataset()
    {
        // public function action($host='',$action='',$data='',$method='',$auth='')
        $var = SiteVariable::find(1)->one();
        $site = $var->site_url;
        $action = 'organization_list?all_fields=true';
        $method = 'GET';
        // init Client Request
        $api_key = $var->ckan_api_key;
        $baseUrl = $site . '/api/3/action';
        $client = new Client(['baseUrl' => $baseUrl]);
        $response = $client->createRequest()
            ->setHeaders(['Authorization' => $api_key])
            ->setMethod($method)
            ->setUrl($action)
            ->setData([])
            ->send();
        if ($response->isOk) {
            $response_data = (object) $response->getData();
            foreach ($response_data->result as $value) {
                $model = CkanDatasetCount::find()->where(['organization_id' => $value['id']])->andWhere(['formated_date' => date('d-m-Y', time())])->one();

                if (is_null($model)) {
                    // Jika Null Insert Data Baru
                    $model_insert = new CkanDatasetCount;
                    $model_insert->organization_id = $value['id'];
                    $model_insert->dataset_count = $value['package_count'];
                    $model_insert->formated_date = date('d-m-Y', time());
                    $model_insert->updated_at = time();
                    $model_insert->save();
                } else {
                    // Cek Data Dobel
                    if (CkanDatasetCount::find()->where(['organization_id' => $value['id']])->andWhere(['formated_date' => date('d-m-Y', time())])->count() >= 1) {
                        // Update Data
                        $model->dataset_total = ($model->dataset_harvested_count > $model->dataset_count) ? $model->dataset_harvested_count : $model->dataset_count;
                        $model->dataset_count = $value['package_count'];
                        $model->updated_at = time();
                        $model->save(false);
                    } else {
                        // Ada Data Dobel Delete
                        $model->delete();
                    }
                }
            }
        } else {
            echo 'Failed get Response';
        }
    }

    public function actionCountDatasetKab()
    {
        $org = CkanOrganization::find()->where(['agency_type' => 'KABKOTA'])->all();
        foreach ($org as $value) {
            if (!empty($value->ckanuser)) {
                // Check Site Avalible / 200 ?
                $headers = @get_headers($value->ckanuser['site']);
                if ($headers && strpos($headers[0], '200')) {
                    $site_kab = $value->ckanuser['site'];
                    $action_kab = "organization_list?all_fields=true";
                    $method_kab = "GET";
                    $api_key_kab = "";
                    $data_kab = [];
                    $baseUrl = $site_kab . '/api/3/action';
                    $client = new Client(['baseUrl' => $baseUrl]);
                    $response = $client->createRequest()
                        ->setHeaders(['Authorization' => $api_key_kab])
                        ->setMethod($method_kab)
                        ->setUrl($action_kab)
                        ->setData($data_kab)
                        ->send();
                    if ($response->isOk) {
                        $response_data = (object) $response->getData();
                        $package_count = 0;
                        foreach ($response_data->result as $kab_ko) {
                            $package_count += $kab_ko['package_count'];
                        }
                        // Update Data Yang ada Jika data tersedia
                        $model = CkanDatasetCount::find()->where(['organization_id' => $value['id_organization']])->andWhere(['formated_date' => date('d-m-Y')])->one();
                        if (!empty($model)) {
                            $model->dataset_harvested_count = $package_count;
                            $model->updated_at = time();
                            $model->save(false);
                        }
                    }

                    // echo '<pre>';
                    // print_r($kab_kota);
                    // echo '<br>';
                    // print_r($response->isOk);
                    // echo '<br>';
                    // print_r($value->id_organization);
                    // echo '<br>';
                    // print_r($value->ckanuser->site);
                    // echo '<br>';
                    // print_r($package_count);
                    // echo '<br>';
                    // echo '<br>';
                    // echo '</pre>';
                }
            }
        }
    }

    public function actionCountDatasetRsud()
    {
        $org = CkanOrganization::find()->where(['like', 'title', 'RSUD'])->all();
        foreach ($org as $value) {
            if (!empty($value->ckanuser)) {
                $site_kab = $value->ckanuser['site'];
                $action_kab = 'organization_list?all_fields=true';
                $method_kab = 'GET';
                $api_key_kab = '';
                $kab_kota = Yii::$app->CkanApi->action($site_kab, $action_kab, '', $method_kab, $api_key_kab);
                $package_count = 0;
                foreach ($kab_kota->result as $kab_ko) {
                    $package_count += $kab_ko['package_count'];
                }
                $model = CkanDatasetCount::find()->where(['organization_id' => $value['id_organization']])->andWhere(['formated_date' => date('d-m-Y')])->one();
                // Update Data Yang ada
                $model->dataset_harvested_count = $package_count;
                $model->updated_at = time();
                $model->save(false);
                echo '<pre>';
                print_r($value->title);
                echo '<br>';
                print_r($value->id_organization);
                echo '<br>';
                print_r($value->ckanuser->site);
                echo '<br>';
                print_r($package_count);
                echo '<br>';
                echo '<br>';
                echo '</pre>';
            }
        }
    }

    public function actionCountDatasetRsjd()
    {
        $org = CkanOrganization::find()->where(['like', 'title', 'RSJD'])->all();
        foreach ($org as $value) {
            if (!empty($value->ckanuser)) {
                $site_kab = $value->ckanuser['site'];
                $action_kab = 'organization_list?all_fields=true';
                $method_kab = 'GET';
                $api_key_kab = '';
                $kab_kota = Yii::$app->CkanApi->action($site_kab, $action_kab, '', $method_kab, $api_key_kab);
                $package_count = 0;
                foreach ($kab_kota->result as $kab_ko) {
                    $package_count += $kab_ko['package_count'];
                }
                $model = CkanDatasetCount::find()->where(['organization_id' => $value['id_organization']])->andWhere(['formated_date' => date('d-m-Y')])->one();
                // Update Data Yang ada
                $model->dataset_harvested_count = $package_count;
                $model->updated_at = time();
                $model->save(false);
                echo '<pre>';
                print_r($value->title);
                echo '<br>';
                print_r($value->id_organization);
                echo '<br>';
                print_r($value->ckanuser->site);
                echo '<br>';
                print_r($package_count);
                echo '<br>';
                echo '<br>';
                echo '</pre>';
            }
        }
    }

    public function actionOrganization()
    {
        $site = 'http://data.jatengprov.go.id';
        $action = 'organization_list?all_fields=true';
        $method = 'GET';
        $api_key = '94106015-00ef-41d1-b750-2dbae360d054';
        $organization = Yii::$app->CkanApi->action($site, $action, [], $method, $api_key);
        foreach ($organization->result as $value) {
            $model = CkanOrganization::find()->where(['id_organization' => $value['id']])->one();
            echo '<pre>';
            print_r($value);
            echo '<br>';
            print_r(is_null($model));
            echo '</pre>';
            if (is_null($model)) {
                $model_insert = new CkanOrganization;
                // $model_insert->id_organization = $value['id'];
                // $model_insert->name = $value['name'];
                // $model_insert->title = $value['title'];
                // $model_insert->description = $value['description'];
                // $model_insert->image_display_url = $value['image_display_url'];
                // $model_insert->agency_type = $model->agency_type;
                // $model_insert->created_at = time();
                // $model_insert->updated_at = time();
                // $model_insert->save(false);
            } else {
                // $model->id_organization = $value['id'];
                // $model->name = $value['name'];
                // $model->title = $value['title'];
                // $model->description = $value['description'];
                // $model->image_display_url = $value['image_display_url'];
                // $model->agency_type = $model->agency_type;
                // $model->created_at = time();
                // $model->updated_at = time();
                // $model->save(false);
            }
        }
    }

    public function actionOrganizationDatasetDate()
    {
        $org = CkanOrganization::find()->where(['agency_type' => 'SKPD'])->all();

        foreach ($org as $value) {
            $var = SiteVariable::find(1)->one();
            $recent_dataset = Yii::$app->CkanApi->action(
                $var->site_url,
                'organization_show?include_users=False&include_datasets=True&id=' . $value->id_organization,
                [],
                'GET',
                []
            );
            // $var->ckan_api_key
            // organization_activity_list
            echo '<pre>';
            print_r($recent_dataset->result['packages']['0']);
            echo '</pre>';
        }
    }

    public function actionDatasetCountKab()
    {
        $org = CkanOrganization::find()->where(['agency_type' => 'KABKOTA'])->all();
        // echo '<pre>';
        // print_r($org);
        // echo '</pre>';
        foreach ($org as $value) {
            if (!empty($value->ckanuser)) {
                $site_kab = $value->ckanuser['site'];
                $action_kab = 'organization_list?all_fields=true';
                $method_kab = 'GET';
                $api_key_kab = '';
                $kab_kota = Yii::$app->CkanApi->action($site_kab, $action_kab, [], $method_kab, $api_key_kab);
                $package_count = 0;
                foreach ($kab_kota->result as $kab_ko) {
                    $package_count += $kab_ko['package_count'];
                }
                $model = CkanDatasetCount::find()->where(['organization_id' => $value['id_organization']])->andWhere(['formated_date' => date('d-m-Y', time())])->one();
                // Update Data Yang Ada
                // $model->dataset_harvested_count = $package_count;
                // $model->organization_id = $value['id'];
                // $model->dataset_count = $value['package_count'];
                // $model->updated_at = time();
                // $model->save(false);
                echo '<pre>';
                print_r($model);
                echo '</pre>';
            }
        }
    }

    // BACKUPPP DRAFT TASK 

    public function actionCreateApi()
    {

        // action($host='',$action='',$data='',$method='',$auth='')

        // $organization = Yii::$app->CkanApi->action('http://data.jatengprov.go.id','organization_list_for_user','','GET','94106015-00ef-41d1-b750-2dbae360d054');
        // Save to Model
        // foreach ($organization->result as $value) {
        //     $model = new CkanOrganization();
        //     $model->organization_id = $value['id'];
        //     $model->name = $value['name'];
        //     $model->title = $value['title'];
        //     $model->description = $value['description'];
        //     $model->image_display_url = $value['image_display_url'];
        //     $model->save(false);
        // }
        // echo '<pre>';
        // print_r($organization->result);
        // echo '</pre>';
    }


    // public function actionDatasetCount()
    // {
    //     // public function action($host='',$action='',$data='',$method='',$auth='')
    //     $site = 'http://data.jatengprov.go.id';
    //     $action = 'organization_list?all_fields=true';
    //     $method = 'GET';
    //     $api_key = '94106015-00ef-41d1-b750-2dbae360d054';
    //     $organization = Yii::$app->CkanApi->action($site,$action,'',$method,$api_key);
    //     foreach ($organization->result as $value) {
    //         // echo $value['id'];
    //         $model = CkanDatasetCount::find()->where(['organization_id' => $value['id']])->andWhere(['formated_date' => date('d-m-Y',time())])->one();
    //         // var_dump($model);

    //         if (is_null($model)) {
    //             // Jika Null Insert Data Baru
    //             $model_insert = new CkanDatasetCount;
    //             $model_insert->organization_id = $value['id'];
    //             $model_insert->dataset_count = $value['package_count'];
    //             $model_insert->formated_date = date('d-m-Y',time());
    //             $model_insert->updated_at = time();
    //             $model_insert->save();
    //         } else {
    //             if (!empty($model->organization->ckanuser)) {
    //                 $site_kab = $model->organization->ckanuser['site'];
    //                 $action_kab = 'organization_list?all_fields=true';
    //                 $method_kab = 'GET';
    //                 $api_key_kab = '';
    //                 $kab_kota = Yii::$app->CkanApi->action($site_kab,$action_kab,'',$method_kab,$api_key_kab);
    //                 $package_count = 0;
    //                 foreach ($kab_kota->result as $value) {
    //                     $package_count += $value['package_count'];
    //                 }
    //                 $model->dataset_harvested_count = $package_count;
    //                 // echo '<pre>';
    //                 // print_r($package_count);
    //                 // echo '</pre>';
    //             }
    //             // Update Data Yang Ada
    //             $model->organization_id = $value['id'];
    //             $model->dataset_count = $value['package_count'];
    //             $model->updated_at = time();
    //             $model->save();
    //         }
    //     }
    //     // var_dump($get_data);
    //     // echo '<pre>';
    //     // print_r($organization);
    //     // echo '</pre>';
    // }

    public function actionDatasetCountKabConsole()
    {
        $org = CkanOrganization::find()->where(['agency_type' => 'KABKOTA'])->all();
        // echo '<pre>';
        // print_r($org);
        // echo '</pre>';
        foreach ($org as $value) {
            if (!empty($value->ckanuser)) {
                $site_kab = $value->ckanuser['site'];
                $action_kab = 'organization_list?all_fields=true';
                $method_kab = 'GET';
                $api_key_kab = '';
                $kab_kota = Yii::$app->CkanApi->action($site_kab, $action_kab, '', $method_kab, $api_key_kab);
                $package_count = 0;
                foreach ($kab_kota->result as $kab_ko) {
                    $package_count += $kab_ko['package_count'];
                }
                $model = CkanDatasetCount::find()->where(['organization_id' => $value['id_organization']])->andWhere(['formated_date' => date('d-m-Y', time())])->one();
                // Update Data Yang Ada
                $model->dataset_harvested_count = $package_count;
                // $model->organization_id = $value['id'];
                // $model->dataset_count = $value['package_count'];
                $model->updated_at = time();
                $model->save(false);
                // echo '<pre>';
                // print_r($model->);
                // echo '</pre>';
            }
        }
        // // public function action($host='',$action='',$data='',$method='',$auth='')
        // $site = 'http://data.jatengprov.go.id';
        // $action = 'organization_list?all_fields=true';
        // $method = 'GET';
        // $api_key = '94106015-00ef-41d1-b750-2dbae360d054';
        // $organization = Yii::$app->CkanApi->action($site,$action,'',$method,$api_key);
        // foreach ($organization->result as $value) {
        //     // echo $value['id'];
        //     $model = CkanDatasetCount::find()->where(['organization_id' => $value['id']])->andWhere(['formated_date' => date('d-m-Y',time())])->one();
        //     // var_dump($model);

        //     if (is_null($model)) {
        //         // Jika Null Insert Data Baru
        //         $model_insert = new CkanDatasetCount;
        //         $model_insert->organization_id = $value['id'];
        //         $model_insert->dataset_count = $value['package_count'];
        //         $model_insert->formated_date = date('d-m-Y',time());
        //         $model_insert->updated_at = time();
        //         $model_insert->save();
        //     } else {
        //         // Cek Data Dobel
        //         if (CkanDatasetCount::find()->where(['organization_id' => $value['id']])->andWhere(['formated_date' => date('d-m-Y',time())])->count() >= 1) {
        //             // Cek Kab Ko ada Apa tidak
        //             // if (!empty($model->organization->ckanuser)) {
        //             //     $site_kab = $model->organization->ckanuser['site'];
        //             //     $action_kab = 'organization_list?all_fields=true';
        //             //     $method_kab = 'GET';
        //             //     $api_key_kab = '';
        //             //     $kab_kota = Yii::$app->CkanApi->action($site_kab,$action_kab,'',$method_kab,$api_key_kab);
        //             //     $package_count = 0;
        //             //     foreach ($kab_kota->result as $value) {
        //             //         $package_count += $value['package_count'];
        //             //     }
        //             //     // Update Data Yang Ada
        //             //     // $model->dataset_harvested_count = $package_count;
        //             //     $model->organization_id = $value['id'];
        //             //     $model->dataset_count = $value['package_count'];
        //             //     $model->updated_at = time();
        //             //     $model->save();
        //             //     // echo '<pre>';
        //             //     // print_r($package_count);
        //             //     // echo '</pre>';
        //             // }else{
        //             //     // Update Data Yang Ada
        //             //     $model->organization_id = $value['id'];
        //             //     $model->dataset_count = $value['package_count'];
        //             //     $model->updated_at = time();
        //             //     $model->save();
        //             // }

        //             // $model->organization_id = $value['id'];
        //             $model->dataset_count = $value['package_count'];
        //             $model->updated_at = time();
        //             $model->save();

        //         }else{
        //             // $delete = CkanDatasetCount::find($model->id);
        //             $model->delete();
        //         }
        //     }
        // }
        // var_dump($get_data);
        // echo '<pre>';
        // print_r($organization);
        // echo '</pre>';
    }

    public function actionGroupConsole()
    {
        $site = 'http://data.jatengprov.go.id';
        $action = 'group_list_authz';
        $method = 'GET';
        $api_key = '94106015-00ef-41d1-b750-2dbae360d054';
        $group = Yii::$app->CkanApi->action($site, $action, [], $method, $api_key);
        foreach ($group->result as $value) {
            $model = CkanGroup::find()->where(['id_group' => $value['id']])->one();
            $model->id_group = $value['id'];
            $model->title = $value['title'];
            $model->name = $value['name'];
            $model->description = $value['description'];
            $model->display_name = $value['display_name'];
            $model->image_display_url = $value['image_display_url'];
            $model->image_url = $value['image_url'];
            $model->package_count = $value['package_count'];
            $model->updated_at = time();
            $model->save();
        }
    }

    public function actionOrganizationConsole()
    {
        $site = 'http://data.jatengprov.go.id';
        $action = 'organization_list?all_fields=true';
        $method = 'GET';
        $api_key = '94106015-00ef-41d1-b750-2dbae360d054';
        $organization = Yii::$app->CkanApi->action($site, $action, [], $method, $api_key);
        foreach ($organization->result as $value) {
            $model = CkanOrganization::find()->where(['id_organization' => $value['id']])->one();
            echo '<pre>';
            print_r($value);
            echo '<br>';
            print_r(is_null($model));
            echo '</pre>';
            if (is_null($model)) {
                $model_insert = new CkanOrganization;
                $model_insert->id_organization = $value['id'];
                $model_insert->name = $value['name'];
                $model_insert->title = $value['title'];
                $model_insert->description = $value['description'];
                $model_insert->image_display_url = $value['image_display_url'];
                // $model_insert->agency_type = $model->agency_type;
                $model_insert->created_at = time();
                $model_insert->updated_at = time();
                $model_insert->save(false);
            } else {
                $model->id_organization = $value['id'];
                $model->name = $value['name'];
                $model->title = $value['title'];
                $model->description = $value['description'];
                $model->image_display_url = $value['image_display_url'];
                // $model->agency_type = $model->agency_type;
                // $model->created_at = time();
                $model->updated_at = time();
                $model->save(false);
            }
        }
    }

    public function actionDatasetConsole()
    {
        $var = SiteVariable::find(1)->one();
        $recent_dataset = Yii::$app->CkanApi->action(
            $var->site_url,
            'current_package_list_with_resources?limit=5',
            [],
            'GET',
            $var->ckan_api_key
        );
        $var->site_recent_dataset = json_encode($recent_dataset->result);
        $var->save(false);
        echo '<pre>';
        print_r(json_encode($recent_dataset->result));
        echo '</pre>';
    }
}
