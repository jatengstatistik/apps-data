<?php

namespace backend\controllers;

use Yii;
use app\models\NewsArticle;
use app\models\NewsArticleSearch;
use app\models\NewsCat;
use app\models\Regencies;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * NewsArticleController implements the CRUD actions for NewsArticle model.
 */
class NewsArticleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NewsArticle models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NewsArticle model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NewsArticle model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NewsArticle();

        $list_cat = ArrayHelper::map(NewsCat::find()->asArray()->all(), 'id', 'title');
        $list_regencies = ArrayHelper::map(Regencies::find()->where(['province_id'=>'33'])->asArray()->all(), 'id', 'name');
        $post_data = Yii::$app->request->post();
        if ($model->load($post_data)) {
            
            $model->create_user_id = Yii::$app->user->id;
            $model->update_user_id = Yii::$app->user->id;
            $model->timestamp_display_from = $post_data['timestamp_display_from'];
            $model->timestamp_display_until = $post_data['timestamp_display_until'];
            $model->geo_location = $post_data['NewsArticle']['geo_location'];
            $model->geo_latitude = $post_data['NewsArticle']['latitude'];
            $model->geo_longitude = $post_data['NewsArticle']['longitude'];
            $model->content = str_replace('<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>', "", $model->content);
            $uploadFile = UploadedFile::getInstance($model, 'image_featured');
            if (!empty($uploadFile) && $uploadFile->size !== 0) {
                $uploadFile->saveAs(Yii::getAlias('@root').'/uploads/featured/'.$uploadFile->baseName.'.'.$uploadFile->extension);
                $model->image_featured = 'uploads/featured/'.$uploadFile->name;
            }
            // }else{
            //     $model->site_logo = $current_logo;
            // }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'list_cat' => $list_cat,
            'list_regencies' => $list_regencies,
        ]);
    }

    /**
     * Updates an existing NewsArticle model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function DUpdate($id)
    {
        $model = $this->findModel($id);
        $list_cat = ArrayHelper::map(NewsCat::find()->asArray()->all(), 'id', 'title');
        $list_regencies = ArrayHelper::map(Regencies::find()->where(['province_id'=>'33'])->asArray()->all(), 'id', 'name');
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        // var_dump(Yii::$app->request->post());
        // $model->validate();
        // var_dump($model->errors);
        return $this->render('update', [
            'model' => $model,
            'list_cat' => $list_cat,
            'list_regencies' => $list_regencies,
        ]);

    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $list_cat = ArrayHelper::map(NewsCat::find()->asArray()->all(), 'id', 'title');
        $list_regencies = ArrayHelper::map(Regencies::find()->where(['province_id'=>'33'])->asArray()->all(), 'id', 'name');
        $current_logo = $model->image_featured;
        $post_data = Yii::$app->request->post();
        if ($model->load($post_data)) {
            // var_dump($post_data);
            $model->geo_location = $post_data['NewsArticle']['geo_location'];
            if (!empty($post_data['NewsArticle']['latitude'])) {
                $model->geo_latitude = $post_data['NewsArticle']['latitude'];
                $model->geo_longitude = $post_data['NewsArticle']['longitude'];
            }
            $model->content = str_replace('<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>', "", $model->content);
            $uploadFile = UploadedFile::getInstance($model, 'image_featured');
            if (!empty($uploadFile) && $uploadFile->size !== 0) {
                $uploadFile->saveAs('uploads/featured/'.$uploadFile->baseName.'.'.$uploadFile->extension);
                $model->image_featured = 'uploads/featured/'.$uploadFile->name;
            }else{
                $model->image_featured = $current_logo;
            }
            $model->update_user_id = Yii::$app->user->id;
            $model->timestamp_display_from = $post_data['timestamp_display_from'];
            $model->timestamp_display_until = $post_data['timestamp_display_until'];
            // var_dump($model->errors);
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        return $this->render('update', [
            'model' => $model,
            'list_cat' => $list_cat,
            'list_regencies' => $list_regencies,
        ]);
    }

    /**
     * Deletes an existing NewsCat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing NewsArticle model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionStatus($id)
    {
        $model = $this->findModel($id);
        
        if ($model->is_deleted == '0') {
            $model->is_deleted = 1;
        }else{
            $model->is_deleted = 0;
        }
        // var_dump($model->is_deleted);
        $model->save(false);

        return $this->redirect(['index']);
    }

    public function actionHeadline($id)
    {
        $model = $this->findModel($id);
        
        if ($model->headline == '0') {
            $model->headline = 1;
        }else{
            $model->headline = 0;
        }
        $model->save(false);
        return $this->redirect(['index']);
    }


    /**
     * Finds the NewsArticle model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsArticle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsArticle::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionUpload() {
        $model = new NewsArticle();
        $base_path = Yii::getAlias('@app');
        $web_path = Yii::getAlias('@web');
        
        // echo "<pre>";
        // print_r($base_path)
        // echo "</pre>";

        // if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstanceByName('file');
            $imageName = $model->file->baseName.'.'.$model->file->extension;

            if ($model->validate()) {
                $model->file->saveAs($base_path . '/web/uploads/content/' . $model->file->baseName . '.' . $model->file->extension);
            }
        // }

        // Get file link
        $res = [
            'link' => $web_path . '/uploads/content/'.$imageName ,
        ];

        // Response data
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $res;
    }

    public function actionUploadFeatured() {
        $model = new NewsArticle();
        $base_path = Yii::getAlias('@app');
        $web_path = Yii::getAlias('@web');
        
        // echo "<pre>";
        // print_r($base_path)
        // echo "</pre>";

        // if (Yii::$app->request->isPost) {
            $model->image_featured = UploadedFile::getInstanceByName('NewsArticle[image_featured]');
            $imageName = $model->image_featured->baseName . '.' . $model->image_featured->extension;

            if ($model->validate()) {
                $model->image_featured->saveAs($base_path . '/web/uploads/featured/' . $model->image_featured->baseName . '.' . $model->image_featured->extension);
            }
        // }


        // Get file link
        $res = [
            'link' => $web_path . '/uploads/featured/'.$imageName ,
        ];
        // $model->image_featured = $res['link'];
        // $model->save();
        // Response data
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $res;
    }

}
