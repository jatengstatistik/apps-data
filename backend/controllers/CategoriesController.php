<?php

namespace backend\controllers;

use Yii;
use yii\filters\VerbFilter;

class CategoriesController extends \yii\web\Controller
{
	/**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
	
    public function actionIndex()
    {
        return $this->render('index');
    }

}
