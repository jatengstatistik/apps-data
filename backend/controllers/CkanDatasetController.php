<?php

namespace backend\controllers;

use Yii;
use app\models\CkanDatasetCount;
use app\models\CkanDatasetCountSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CkanDatasetController implements the CRUD actions for CkanDatasetCount model.
 */
class CkanDatasetController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function datasetDaily($date='')
    {
        $stats_data = [];
        if(empty($date)) {
            $date = date('m-Y');
        }
        
        $days = CkanDatasetCount::find()->select('formated_date')->where(['LIKE','formated_date',$date])->distinct()->all();
        foreach ($days as $value) {
            $stats_data[] = [
                'date' => $value->formated_date,
                // 'dataset' => CkanDatasetCount::find()->select('sum(dataset_count) as counters')->where(['formated_date'=>$value->formated_date])->asArray()->all()[0]['counters'],
                'dataset' => CkanDatasetCount::find()->where(['formated_date'=>$value->formated_date])->sum('dataset_total'),
                'dataset_skpd' => CkanDatasetCount::find()->skpd($value->formated_date)->sum('dataset_total'),
                'dataset_kabko' => CkanDatasetCount::find()->kabko($value->formated_date)->sum('dataset_total'),
            ];
        }
        return $stats_data;
        
        // echo "<pre>";
        // // print_r($date);
        // echo '<br>';
        // print_r($stats_data);
        // echo "</pre>";

    }

    public function datasetMonthly($year = '')
    {
        if(empty($year)) {
            $year = date('Y');
          }
        for ($i=1; $i <= 12; $i++) { 
            $startDate = '1-'.$i.'-'.$year;
            $endDate = date('d-m-Y',strtotime('last day of this month', strtotime($startDate)));

            $datasetMonthly[] = [
                'date_month_range' => $endDate,
                'dataset' => CkanDatasetCount::find()->where(['formated_date'=>$endDate])->sum('dataset_total'),
                'dataset_skpd' => CkanDatasetCount::find()->skpd($endDate)->sum('dataset_total'),
                'dataset_kabko' => CkanDatasetCount::find()->kabko($endDate)->sum('dataset_total'),
            ];
        }

        return $datasetMonthly;
        // echo "<pre>";
        // print_r($oneMonth);
        // print_r($datasetMonthly);
        // echo "</pre>";

    }    

    /**
     * Lists all CkanDatasetCount models.
     * @return mixed
     */
    public function actionIndex($month = '',$year = '')
    {

        if (empty($year) || empty($month)) {
            $year = date('Y');
            $date = date('m-Y');
        }else{
            $year = Yii::$app->request->get('year');
            $date = $month.'-'.$year;
        }

        $daily_dataset = $this->datasetDaily($date);
        $monthly_dataset = $this->datasetMonthly($year);

        // echo '<pre>';
        // print_r($this->datasetDaily($date));
        // echo '</pre>';
        if (empty($daily_dataset) || empty($monthly_dataset)) {
            Yii::$app->session->setFlash('warning','Data Tidak Tersedia');
            return $this->redirect(['index']);    
        }

        return $this->render('index', [
            'daily_dataset' => $daily_dataset,
            'monthly_dataset' => $monthly_dataset,
        ]);
    }

    public function actionSkpd()
    {
        if (empty(Yii::$app->request->get('date'))) {
            $date = date("d-m-Y");       
        }else{
            $date = Yii::$app->request->get('date');
        }
        $searchModel = new CkanDatasetCountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->skpd(['formated_date'=>$date]);
        return $this->render('skpd', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'date' => $date,
        ]);
    }

    public function actionKabko()
    {
        if (empty(Yii::$app->request->get('date'))) {
            $date = date("d-m-Y");       
        }else{
            $date = Yii::$app->request->get('date');
        }
        $searchModel = new CkanDatasetCountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->kabko(['formated_date'=>$date]);
        return $this->render('kabkota', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'date' => $date,
        ]);
    }

    /**
     * Displays a single CkanDatasetCount model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView()
    {
        if (empty(Yii::$app->request->get('date'))) {
            $date = date("d-m-Y");       
        }else{
            $date = Yii::$app->request->get('date');
        }
        $searchModel = new CkanDatasetCountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['formated_date'=>$date]);
        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'date' => $date,
        ]);
    }

    /**
     * Creates a new CkanDatasetCount model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CkanDatasetCount();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CkanDatasetCount model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CkanDatasetCount model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CkanDatasetCount model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CkanDatasetCount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CkanDatasetCount::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
