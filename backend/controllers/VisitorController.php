<?php

namespace backend\controllers;

use Yii;
use app\models\Visitor;
use app\models\VisitorSearch;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * VisitorController implements the CRUD actions for Visitor model.
 */
class VisitorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://www.data.jatengprov.go.id', 'https://www.data.jatengprov.go.id','http://localhost:28182','http://localhost:28181','http://data.jatengprov.go.id:8182','http://data.jatengprov.go.id'],
                    // Allow only POST and PUT methods
                    // 'Access-Control-Request-Method' => ['POST', 'PUT'],
                    'Access-Control-Request-Method' => ['GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['X-Wsse'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],

            ],
        ];
    }

    public function visitorYearlyInMonth()
    {
        for ($i = 2018; $i <= date('Y'); $i++) {
            $visitorYearlyOfMonth[$i] = $this->visitorMonthly($i);
        }

        return $visitorYearlyOfMonth;
    }

    public function visitorQuarterly($year = null)
    {
        if(!isset($year)) {
            $year = date('Y');
          }
        $dataQuarterly = array_chunk($this->visitorMonthly($year), 3);
        for ($i = 0; $i < 4; $i++) {
            foreach ($dataQuarterly[$i] as $key => $valueQ) {
                
                $dataQ[$i][] =$valueQ['counter'];
                $monthQ[$i][] =date('F',strtotime($valueQ['month_range']));
            }  
            $vQuarterly[] = 
                [
                    'month_range' => implode('-', $monthQ[$i]),
                    'counter' => array_sum($dataQ[$i]),
                ];
        }
        
        return $vQuarterly;
        // echo '<pre>';
        // print_r($vQuarterly);
        // print_r($dataQ);
        // echo '<pre>';
    }

    public function visitorMonthly($year = null)
    {
        if(!isset($year)) {
            $year = date('Y');
          }
        for ($i=1; $i <= 12; $i++) { 
            $yearMonth = $year.'-'.$i;
            $monthVisitor[] = [
                'month_range' => date('Y-m',strtotime($yearMonth)),
                'counter' => Visitor::find()->where(['LIKE','created_at',date('Y-m',strtotime($yearMonth))])->sum('counter'),

            ];  
        }
        return $monthVisitor;
        
        // echo "<pre>";
        // print_r($monthVisitor);
        // echo "</pre>";

    }

    public function actionWeekly($type=null)
    {
        $weekNumber = 1;
        foreach ($this->weekOfMonth() as $value) {
            $counterVisitor = Visitor::find()
            ->where(['between','created_at',date('Y-m-d H:i:s',strtotime($value->monday)),date('Y-m-d H:i:s',strtotime($value->sunday))])
            ->sum('counter');
            if (!is_null($counterVisitor)) {
                $data[] = [
                    'week' => $weekNumber++,
                    'counter' => $counterVisitor,
                ];
            }
        }
        if ($type=='array') {
            $respond = $data;
        }elseif($type=='object'){
            $respond = (object) $data;
        }else{
            $respond = $this->asJson($data);
            // $respond = json_encode($data);
        }
        return $respond;
        // $this->response($data, REST_Controller::HTTP_OK); 
        // echo '<pre>';
        // print_r($respond);
        // echo '<br>';
        // print_r($data);
        // print_r(date('Y-m-d H:i:s',strtotime($value->monday)));
        // echo '<pre>';
    }

    public function weekOfMonth($value='')
    {
       return $weekOfMonth = (object) [
            'first' => (object) [
                'monday'=> $this->firstDayOfMonth()->format('d-m-Y'),
                'sunday'=> date("d-m-Y", strtotime('sunday this week', strtotime($this->firstDayOfMonth()->format('d-m-Y')))),
            ],
            'second' => (object) [
                'monday'=> $this->firstWeekOfMonth('first')->format('d-m-Y'),
                'sunday'=> date("d-m-Y", strtotime('sunday this week', strtotime($this->firstWeekOfMonth('first')->format('d-m-Y')))),
            ],
            'third' => (object) [
                'monday'=> $this->firstWeekOfMonth('second')->format('d-m-Y'),
                'sunday'=> date("d-m-Y", strtotime('sunday this week', strtotime($this->firstWeekOfMonth('second')->format('d-m-Y')))),
            ],
            'fourth' => (object) [
                'monday'=> $this->firstWeekOfMonth('third')->format('d-m-Y'),
                'sunday'=> date("d-m-Y", strtotime('sunday this week', strtotime($this->firstWeekOfMonth('third')->format('d-m-Y')))),
            ],
            'fifth' => (object) [
                'monday'=> $this->firstWeekOfMonth('fourth')->format('d-m-Y'),
                'sunday'=> date("d-m-Y", strtotime('last day of this month', strtotime($this->firstDayOfMonth()->format('d-m-Y')))),
            ],
        ];
    }

    public function firstWeekOfMonth($value='')
    {
        return $firstWeek = new \DateTime("$value Monday of this month");
    }

    public function firstDayOfMonth($value='')
    {
        return $firstDayOfMonth = new \DateTime("first day of this month");
    }

    public function lastDayOfMonth($value='')
    {
        return $lastDayOfMonth = new \DateTime("last day of this month");
    }

    public function actionNow()
    {
        // ->toArray();
        $visitorNow = Visitor::find()->where(['LIKE','created_at',date('Y-m-d')])->one();
        if (!empty($visitorNow)) {
            // Jika ada Data
            $model = $this->findModel($visitorNow->id);
            $model->counter = $visitorNow->counter+1;
            $model->save(false);
            // $this->db->update('visitor',['counter'=> $date_now->counter+1],['id' => $date_now->id]);
        }else{
            // Jika kosong
            $model = new Visitor();
            $model->counter = 1;
            $model->save(false);
            // $this->db->insert('visitor',['counter'=>1]);
        }

        // $this->response($this->db->like(['created_at'=> date('Y-m-d')])->get('visitor')->row('counter'), REST_Controller::HTTP_OK);
        $getVisitor = Visitor::find()->where(['LIKE','created_at',date('Y-m-d')])->one()->counter;
        return $this->asJson($getVisitor);
        // echo '<pre>';
        // print_r($findNowDate);
        // echo '<pre>';
    }

    /**
     * Lists all Visitor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VisitorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // echo '<pre>';
        // print_r(($this->actionWeekly()->data));
        // echo '<pre>';
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'visitorToday' => $dataProvider->query->where(['LIKE','created_at',date('Y-m-d')])->one(),
            'visitorWeekly' => $this->actionWeekly('array'),
            'visitorQuarterly' => $this->visitorQuarterly(),
            'visitorMonthly' => $this->visitorMonthly(),
            'visitorYearlyInMonth' => $this->visitorYearlyInMonth(),
        ]);
    }

    public function actionView()
    {
        $searchModel = new VisitorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Visitor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionView($id)
    // {
    //     return $this->render('view', [
    //         'model' => $this->findModel($id),
    //     ]);
    // }

    /**
     * Creates a new Visitor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Visitor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Visitor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Visitor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Visitor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Visitor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Visitor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }


}
