<?php

namespace backend\controllers;

use Yii;
use app\models\AccountsOds;
use app\models\AccountsOdsSearch;
use app\models\Regencies;
use yii\httpclient\Client;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * AccountsOdsController implements the CRUD actions for AccountsOds model.
 */
class AccountsOdsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionUsersView($id)
    {
        $model = $this->findModel($id);
        $dataset = $model->loadFile(Yii::getAlias("@web/web/uploads/batch_user_kabko/{$model->file_batch}"));
        return $this->render('view-user', [
            'model' => $this->findModel($id),
            'dataProvider' => $model->getDataProvider($dataset),
            'attribute' => $model->getFileAttributeGridView($dataset),
        ]);
    }

    public function actionCreateUsers($id)
    {
        $model = $this->findModel($id);
        $dataset = $model->loadFile(Yii::getAlias("@web/web/uploads/batch_user_kabko/{$model->file_batch}"));
        $data = $model->getFileData($dataset);
        $site = $model->site;
        $api_key = $model->api_key;
        // private function apiCkan($host='',$action='',$data='',$method='',$auth='')
        foreach ($data as $value) {
            $data_user = [
                'name' => $value[0],
                'password' => $value[1],
                'fullname' => $value[2],
                'email' => $value[3],  
            ];
            $this->apiCkan($site,'action/user_create',$data_user,'POST',$api_key);

            $data_organization = [
                'name' => $value[0],
                'description' => ucwords(strtolower($value[2])).' '.ucwords(strtolower($model->regencies->name)),
                'title' => $value[2],
            ];
            $this->apiCkan($site,'action/organization_create',$data_organization,'POST',$api_key);

            $data_user_organization = [
                'id' => $value[0],
                'username' => $value[0],
                'role' => 'admin',
            ];
            $this->apiCkan($site,'action/organization_member_create',$data_user_organization,'POST',$api_key);
        }

        // $data_user = [
        //     'name' => 'agusedyc',
        //     'password' => 'agusedyc',
        //     'fullname' => 'Agus Edy Cahyono',
        //     'email' => 'agusedyc@gmail.com',
            
        // ];
        
        // $result = $this->apiCkan($site,'action/user_create',$data_user,'POST',$api_key);

        // echo '<pre>';
        // print_r($result);
        // echo '</pre>';
    }

    /**
     * Lists all AccountsOds models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AccountsOdsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AccountsOds model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AccountsOds model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AccountsOds();
        $list_regencies = ArrayHelper::map(Regencies::find()->where(['province_id'=>'33'])->asArray()->all(), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'list_regencies' => $list_regencies,
        ]);
    }

    /**
     * Updates an existing AccountsOds model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $list_regencies = ArrayHelper::map(Regencies::find()->where(['province_id'=>'33'])->asArray()->all(), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'list_regencies' => $list_regencies,
        ]);
    }

    /**
     * Deletes an existing AccountsOds model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AccountsOds model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AccountsOds the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AccountsOds::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    private function apiCkan($host='',$action='',$data='',$method='',$auth='')
    {
        // $client = new Client(['baseUrl' => 'http://103.9.227.205/api/3']);
        // $host = 'http://data.jatengprov.go.id'
        $baseUrl = $host.'/api/3';
        $client = new Client(['baseUrl' => $baseUrl]);
        $response = $client->createRequest()
          // ->setHeaders(['Authorization' => '61e47dd8-352a-4fe3-99c6-058b1b7b73b0'])
          ->setHeaders(['Authorization' => $auth])
          ->setMethod($method)
          ->setFormat(Client::FORMAT_JSON)
          ->setUrl($action)
          ->setData($data)
          ->send();
        return $responseData = (object) $response->getData();
        // echo '<pre>';
        // print_r((object) $responseData->result);
        // echo '</pre>';
    }
}
