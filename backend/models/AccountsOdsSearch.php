<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AccountsOds;

/**
 * AccountsOdsSearch represents the model behind the search form of `app\models\AccountsOds`.
 */
class AccountsOdsSearch extends AccountsOds
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'port'], 'integer'],
            [['regencies_id', 'site', 'host', 'ssh_username', 'ssh_password', 'sysadmin_user', 'sysadmin_password', 'notes', 'file_batch', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AccountsOds::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('regencies');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'port' => $this->port,
            'regencies_id' => $this->regencies_id,
        ]);

        $query->andFilterWhere(['like', 'regencies.name', $this->regencies_id])
            ->andFilterWhere(['like', 'site', $this->site])
            ->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'ssh_username', $this->ssh_username])
            ->andFilterWhere(['like', 'ssh_password', $this->ssh_password])
            ->andFilterWhere(['like', 'sysadmin_user', $this->sysadmin_user])
            ->andFilterWhere(['like', 'sysadmin_password', $this->sysadmin_password])
            ->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'file_batch', $this->file_batch])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at]);

        return $dataProvider;
    }
}
