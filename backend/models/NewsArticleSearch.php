<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NewsArticle;

/**
 * NewsArticleSearch represents the model behind the search form of `app\models\NewsArticle`.
 */
class NewsArticleSearch extends NewsArticle
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'image_featured', 'create_user_id', 'update_user_id', 'timestamp_create', 'timestamp_update', 'timestamp_display_from', 'timestamp_display_until', 'is_deleted', 'is_display_limit','headline'], 'integer'],
            [['title', 'cat_id', 'content', 'image_list', 'file_list', 'teaser_text', 'geo_location'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsArticle::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder' => ['timestamp_create'=>SORT_DESC]],
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (empty($this->title)) {
            $query->joinWith('categories');    
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'cat_id' => $this->cat_id,
            'image_featured' => $this->image_featured,
            'create_user_id' => $this->create_user_id,
            'update_user_id' => $this->update_user_id,
            'timestamp_create' => $this->timestamp_create,
            'timestamp_update' => $this->timestamp_update,
            'timestamp_display_from' => $this->timestamp_display_from,
            'timestamp_display_until' => $this->timestamp_display_until,
            'is_deleted' => $this->is_deleted,
            'is_display_limit' => $this->is_display_limit,
            'headline' => $this->headline,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'news_cat.title', $this->cat_id])
            ->andFilterWhere(['like', 'image_list', $this->image_list])
            ->andFilterWhere(['like', 'file_list', $this->file_list])
            ->andFilterWhere(['like', 'teaser_text', $this->teaser_text])
            ->andFilterWhere(['like', 'headline', $this->headline])
            ->andFilterWhere(['like', 'geo_location', $this->geo_location]);

        return $dataProvider;
    }
}
