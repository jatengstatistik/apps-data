<?php

namespace app\models;

use Yii;
use app\models\Regencies;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;
use yii\data\ArrayDataProvider;

/**
 * This is the model class for table "accounts_ods".
 *
 * @property int $id
 * @property string $regencies_id
 * @property string $site
 * @property string $host
 * @property int $port
 * @property string $ssh_username
 * @property string $ssh_password
 * @property string $sysadmin_user
 * @property string $sysadmin_password
 * @property string $notes
 * @property string $file_batch
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Regency $regencies
 */
class AccountsOds extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ckan_account';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['regencies_id'], 'required'],
            [['port','id'], 'integer'],
            [['notes', 'created_at', 'updated_at'], 'string'],
            [['regencies_id'], 'string', 'max' => 4],
            [['file_batch'],'file','skipOnEmpty' => true],
            [['site','api_key', 'host', 'ssh_username', 'ssh_password', 'sysadmin_user', 'sysadmin_password'], 'string', 'max' => 50],
            [['regencies_id'], 'exist', 'skipOnError' => true, 'targetClass' => Regencies::className(), 'targetAttribute' => ['regencies_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'regencies_id' => Yii::t('app', 'Regencies'),
            'site' => Yii::t('app', 'Site'),
            'api_key' => Yii::t('app', 'API Key'),
            'host' => Yii::t('app', 'Host'),
            'port' => Yii::t('app', 'Port'),
            'ssh_username' => Yii::t('app', 'Ssh Username'),
            'ssh_password' => Yii::t('app', 'Ssh Password'),
            'sysadmin_user' => Yii::t('app', 'Sysadmin User'),
            'sysadmin_password' => Yii::t('app', 'Sysadmin Password'),
            'notes' => Yii::t('app', 'Notes'),
            'file_batch' => Yii::t('app', 'File Batch'),
            // 'created_at' => Yii::t('app', 'Created At'),
            // 'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegencies()
    {
        return $this->hasOne(Regencies::className(), ['id' => 'regencies_id']);
    }

    public function beforeValidate()
    {
        $this->file_batch = UploadedFile::getInstance($this, 'file_batch');
         
         return parent::beforeValidate();

    }

    public function beforeSave($insert) 
    {
            // Data Di Update
        if (!$this->isNewRecord) {
            if (!empty($this->file_batch)) {
                // File Diganti
                $this->file_batch = $this->file_batch;
                if (file_exists(Yii::getAlias("@root/uploads/batch_user_kabko/{$this->getOldAttributes()['file_batch']}"))) {
                    unlink(Yii::getAlias("@root/uploads/batch_user_kabko/{$this->getOldAttributes()['file_batch']}"));    
                }
                $this->file_batch->saveAs(Yii::getAlias("@root/uploads/batch_user_kabko/{$this->file_batch->name}"));
            }else{
                // File Tetap
                $this->file_batch = $this->getOldAttributes()['file_batch'];
            }
        }else{
            if (!empty($this->file_batch)) {
                $this->file_batch->saveAs(Yii::getAlias("@root/uploads/batch_user_kabko/{$this->file_batch->name}"));
            }
        }
        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        // parent::afterSave($insert, $changedAttributes);
        // $this->nama_data = $this->loadFile(Yii::getAlias("@web/web/uploads/batch_user_kabko/{$this->file_batch}"))['title'];
        // $this->updateAttributes(['nama_data']);
        // return true;
    }

    public function afterDelete()
    {
        parent::afterDelete();
        if (file_exists(Yii::getAlias("@root/uploads/batch_user_kabko/{$this->file_batch}"))) {
            unlink(Yii::getAlias("@root/uploads/batch_user_kabko/{$this->file_batch}"));    
        }
    }

    public function loadFile($pathFile='')
    {
        $inputFileName = Yii::$app->basePath.$pathFile;

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($inputFileName);
        $spreadsheetMetaData = $spreadsheet->getProperties();
        $sheetData   = [
            'title' => $spreadsheetMetaData->getTitle(),
            'data' => $spreadsheet->getActiveSheet()->toArray(),
        ];
        return $sheetData;
    }

    public function getFileAttributeGridView($sheetData='')
    {
        
        $changeKeyArray[] = ['class' => 'yii\grid\SerialColumn'];
        foreach ($sheetData['data'][0] as $key => $value) {
            $changeKeyArray[] = [
                'attribute' => $key,
                'label' => ucwords($value),
            ];
        }

        return $changeKeyArray;
    }

    public function getFileAttribute($sheetData='')
    {
        foreach ($sheetData['data'][0] as $key => $value) {
            $changeKeyArray[] = [
                'attribute' => $key,
                'label' => ucwords($value),
            ];
        }

        return $changeKeyArray;
    }

    public function getFileData($sheetData='')
    {
        // Memisahkan Nama Field pada baris 1 dengan Data pada baris selanjutnya;
        // $this->printData($sheetData['data']);
        $data = array_splice($sheetData['data'],1);

        return $data;
    }

    public function getDataProvider($dataset)
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => $this->getFileData($dataset),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => $this->getFileAttributeGridView($dataset),
            ],
        ]);

        return $dataProvider;
    }

    private function printData($dataPrint)
    {
        echo '<pre>';
        print_r($dataPrint);
        echo '</pre';
    }
}
