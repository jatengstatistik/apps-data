<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CkanDatasetCount]].
 *
 * @see CkanDatasetCount
 */
class CkanDatasetCountQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    public function skpd($date = '')
    {
    	$this->joinWith('organization');
        $this->andFilterWhere(['formated_date'=>$date]);
        return $this->andFilterWhere(['NOT LIKE', 'title', 'Pemerintah']);
    }

    public function kabko($date = '')
    {
    	$this->joinWith('organization');
        $this->andFilterWhere(['formated_date'=>$date]);
        return $this->andFilterWhere(['like', 'title', 'Pemerintah']);
    }

    /**
     * {@inheritdoc}
     * @return CkanDatasetCount[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CkanDatasetCount|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}