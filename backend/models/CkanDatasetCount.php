<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ckan_dataset_count".
 *
 * @property int $id
 * @property string $organization_id
 * @property int $dataset_harvested_count
 * @property int $dataset_count
 * @property string $date_harvested
 * @property int $formated_date
 * @property int $updated_at
 */
class CkanDatasetCount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ckan_dataset_count';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dataset_harvested_count', 'dataset_count', 'updated_at','dataset_total'], 'integer'],
            [['organization_id'], 'string', 'max' => 255],
            [['date_harvested', 'formated_date'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'organization_id' => Yii::t('app', 'Organization ID'),
            'dataset_harvested_count' => Yii::t('app', 'Dataset Harvested Count'),
            'dataset_count' => Yii::t('app', 'Dataset Count'),
            'dataset_total' => Yii::t('app', 'Dataset Total'),
            'date_harvested' => Yii::t('app', 'Date Harvested'),
            'formated_date' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return CkanDatasetCountQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CkanDatasetCountQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(CkanOrganization::className(), ['id_organization' => 'organization_id']);
    }

    public function getStatus()
    {
      if ($this->dataset_harvested_count>0) {
        // return ;
        $selisih_data = $this->dataset_count - $this->dataset_harvested_count;
        if ($selisih_data < 0) {
          // return 'Terhubung *';
          return 'Memperbaharui '.abs($selisih_data).' Data';
        }else{
          return 'Terhubung Data Terbaru';
        }
      }else{
        return 'Terputus';
      }
    }

    public static function getTotal($provider, $columnName)
    {
        $total = 0;
        foreach ($provider as $item) {
          $total += $item[$columnName];
      }
      return $total;  
    }

    public function sumColumn($data,$variable)
    {
        $sum=0;
        foreach ($data as $value) {
            $sum+=$value[$variable];
        }

        return $sum;

        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
    }

}
