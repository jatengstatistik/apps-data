<?php

namespace app\tasks;

use Yii;
use app\models\SiteVariable;
use yii\httpclient\Client;

/**
 * Class AlphabetTask
 * @package app\tasks
 */
class RecentDatasetTask extends \webtoolsnz\scheduler\Task
{
    public $description = 'Recent Datasets';
    // Every Hours
    public $schedule = '*/15 * * * *';

    public function run()
    {
        $var = SiteVariable::find(1)->one();
        $host = $var->site_url;
        $baseUrl = $host . '/api/3/action';
        $action = 'current_package_list_with_resources?limit=5';
        $data = '';
        $method = 'GET';
        $auth = $var->ckan_api_key;
        $client = new Client(['baseUrl' => $baseUrl]);
        $response = $client->createRequest()
            ->setHeaders(['Authorization' => $auth])
            ->setMethod($method)
            ->setFormat(Client::FORMAT_JSON)
            ->setUrl($action)
            ->setData($data)
            ->send();
        if ($response->isOk) {
            $responseData = (object) $response->getData();
            $var->site_recent_dataset = json_encode($responseData->result);
            $var->save(false);
            // echo '<pre>';
            // print_r(json_encode($responseData->result));
            // echo '</pre>';
        }
    }
}
