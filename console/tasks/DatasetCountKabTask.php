<?php

namespace app\tasks;

use Yii;
use app\models\CkanDatasetCount;
use app\models\CkanOrganization;
use yii\httpclient\Client;

/**
 * Class AlphabetTask
 * @package app\tasks
 */

class DatasetCountKabTask extends \webtoolsnz\scheduler\Task
{
    public $description = 'Dataset Count Kab Task';
    // Every Hours
    public $schedule = '*/12 * * * *';

    public function run()
    {
        // set_time_limit(90);
        $org = CkanOrganization::find()->where(['agency_type' => 'KABKOTA'])->all();
        $no = 1;
        foreach ($org as $value) {
            echo $no++ . '.';
            if (!empty($value->ckanuser)) {
                // Check Site Avalible / 200 ?
                $headers = @get_headers($value->ckanuser['site']);
                if ($headers && strpos($headers[0], '200')) {
                    $site_kab = $value->ckanuser['site'];
                    $action_kab = "organization_list?all_fields=true";
                    $method_kab = "GET";
                    $api_key_kab = "";
                    $data_kab = [];
                    $baseUrl = $site_kab . '/api/3/action';
                    $client = new Client(['baseUrl' => $baseUrl]);
                    $response = $client->createRequest()
                        ->setHeaders(['Authorization' => $api_key_kab])
                        ->setMethod($method_kab)
                        ->setUrl($action_kab)
                        ->setData($data_kab)
                        ->send();
                    if ($response->isOk) {
                        $response_data = (object) $response->getData();
                        $package_count = 0;
                        foreach ($response_data->result as $kab_ko) {
                            $package_count += $kab_ko['package_count'];
                        }
                        // Update Data Yang ada Jika data tersedia
                        $model = CkanDatasetCount::find()->where(['organization_id' => $value['id_organization']])->andWhere(['formated_date' => date('d-m-Y')])->one();
                        if (!empty($model)) {
                            echo '200-' . $package_count . '-';
                            $model->dataset_harvested_count = $package_count;
                            $model->updated_at = time();
                            $model->save(false);
                        }
                    }
                }
            } else {
                echo 'NF-';
            }
        }
    }
}
