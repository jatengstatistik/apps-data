<?php

namespace app\tasks;

use Yii;
use app\models\SiteVariable;
use yii\httpclient\Client;

/**
 * Class AlphabetTask
 * @package app\tasks
 */
class DigitalForecastJawaTengahTask extends \webtoolsnz\scheduler\Task
{
    public $description = 'Digital Forecast Jawa Tengah From BMKG';
    // Every Hours
    public $schedule = '*/30 * * * *';

    // http://dataweb.bmkg.go.id/MEWS/DigitalForecast/DigitalForecast-JawaTengah.xml
    // https://data.bmkg.go.id/DataMKG/MEWS/DigitalForecast/DigitalForecast-JawaTengah.xml
    public function run()
    {
        $client = new Client(['baseUrl' => 'https://data.bmkg.go.id/']);
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl('DataMKG/MEWS/DigitalForecast/DigitalForecast-JawaTengah.xml')
            ->send();
        if ($response->isOk) {
            $response_data = $response->getData();
            // Save to DB Prevent Error Access API BMKG
            $saveData = SiteVariable::find(1)->one();
            $saveData->site_whether = json_encode($response_data);
            $saveData->save(false);
        }
    }
}
