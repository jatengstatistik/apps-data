<?php

namespace app\tasks;

use Yii;
use app\models\CkanDatasetCount;
use app\models\CkanOrganization;
use app\models\NewsArticle;
use yii\httpclient\Client;

/**
 * Class AlphabetTask
 * @package app\tasks
 */

class DeactiveOutDateArticleTask extends \webtoolsnz\scheduler\Task
{
    public $description = 'Deactive Outdate Article';
    // Every Hours
    public $schedule = '*/30 * * * *';

    public function run()
    {
        $today = date('d-m-Y');
        $article = NewsArticle::find()->where(['not in', 'timestamp_display_from', ['0']])->all();
        foreach ($article as $value) {
            if (!$this->isDateRange($today, $value->timestamp_display_from, $value->timestamp_display_until)) {
                // if ($value->is_deleted != 1) {
                $deactive_article = NewsArticle::find()->where(['id' => $value->id])->one();
                $deactive_article->is_deleted = '1';
                $deactive_article->save(false);
                echo $value->id . '.U-';
                // }
            } else {
                echo $value->id . '.N-';
            }
        }
    }

    function isDateRange($date_from_user, $start_date, $end_date)
    {
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($date_from_user);

        // Check that user date is between start & end
        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }
}
