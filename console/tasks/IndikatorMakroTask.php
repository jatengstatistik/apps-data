<?php

namespace app\tasks;

use Yii;
use app\models\SiteVariable;
use yii\httpclient\Client;

/**
 * Class AlphabetTask
 * @package app\tasks
 */
class IndikatorMakroTask extends \webtoolsnz\scheduler\Task
{
    public $description = 'Indikator Makro';
    // Every Hours
    public $schedule = '0 */6 * * *';

    public function run()
    {
        // https://webapps.bps.go.id/jateng/api/v1/dashboard/in
        $client = new Client(['baseUrl' => 'https://webapps.bps.go.id/jateng/api/v1/']);
        $dashboard = $client->createRequest()
            ->setMethod('GET')
            ->setUrl('dashboard/in')
            ->send();
        if ($dashboard->isOk) {
            $valdashboard = $dashboard->getData();
            foreach ($valdashboard['data'] as $value) {
                $dibanding = (empty($value['sebelumnya'])) ? '' : 'dibanding periode lalu ' . $value['sebelumnya'];
                $indikator[$value['id']] = [
                    'name'  => $value['indikator'],
                    'now' => $value['bulan'] . ' ' . $value['tahun'],
                    'value' => $value['nilai'],
                    'mark'  => $value['tanda'],
                    'change'  => $value['poin'],
                    'before'  => $value['sebelumnya'],
                    // 'image'  => '<a href="/uploads/ots_img/'.$value['id'].'.png">File</a>',
                    'image'  => '/uploads/ots_img/' . $value['id'] . '.png',
                    'unit'  => $value['satuan'],
                    'desc'  => $value['indikator'] . ' ' . $value['bulan'] . ' ' . $value['tahun'] . ' ' . $value['tanda'] . ' ' . $value['poin'] . ' ' . $value['delta'] . ' ' . $dibanding,
                ];
            }
        }

        $var = SiteVariable::find(1)->one();
        $var->site_indikator_makro = json_encode($indikator);
        $var->save(false);
    }
}
