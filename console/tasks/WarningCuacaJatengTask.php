<?php

namespace app\tasks;

use Yii;
use app\models\SiteVariable;
use yii\httpclient\Client;

/**
 * Class AlphabetTask
 * @package app\tasks
 */
class WarningCuacaJatengTask extends \webtoolsnz\scheduler\Task
{
    public $description = 'WarningCuaca';
    // Every Hours
    // Error Api Not found
    public $schedule = '*/50 * * * *';

    public function run()
    {
        $client = new Client(['baseUrl' => 'http://datadisplay.bmkg.go.id/XML']);
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl('WarningCuaca.xml')
            ->send();
        if ($response->isOk) {
            $response_data = $response->getData();
            // Save to DB Prevent Error Access API BMKG
            $saveData = SiteVariable::find(1)->one();
            $saveData->site_warning_whether = json_encode($response_data);
            $saveData->save(false);
        }
    }
}
