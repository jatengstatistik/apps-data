<?php

namespace app\tasks;

use Yii;
use app\models\CkanGroup;
use app\models\SiteVariable;

/**
 * Class AlphabetTask
 * @package app\tasks
 */
class GroupTask extends \webtoolsnz\scheduler\Task
{
    public $description = 'Update Group Datasets';
    // Every Hours
    public $schedule = '*/30 * * * *';

    public function run()
    {
        // public function action($host='',$action='',$data='',$method='',$auth='')
        $var = SiteVariable::find(1)->one();
        $site = $var->site_url;
        $action = 'group_list_authz';
        $method = 'GET';
        $api_key = '94106015-00ef-41d1-b750-2dbae360d054';
        $group = Yii::$app->CkanApi->action($site, $action, [], $method, $api_key);
        foreach ($group->result as $value) {
            $model = CkanGroup::find()->where(['id_group' => $value['id']])->one();
            if (is_null($model)) {
                $model_insert = new CkanGroup;
                $model_insert->id_group = $value['id'];
                $model_insert->title = $value['title'];
                $model_insert->name = $value['name'];
                $model_insert->description = $value['description'];
                $model_insert->display_name = $value['display_name'];
                $model_insert->image_display_url = $value['image_display_url'];
                $model_insert->image_url = $value['image_url'];
                $model_insert->package_count = $value['package_count'];
                $model_insert->updated_at = time();
                $model_insert->save();
            } else {
                $model->id_group = $value['id'];
                $model->title = $value['title'];
                $model->name = $value['name'];
                $model->description = $value['description'];
                $model->display_name = $value['display_name'];
                $model->image_display_url = $value['image_display_url'];
                $model->image_url = $value['image_url'];
                $model->package_count = $value['package_count'];
                $model->updated_at = time();
                $model->save();
            }
        }
    }
}
