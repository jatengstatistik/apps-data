<?php

namespace app\tasks;

use Yii;
use app\models\CkanDatasetCount;
use app\models\CkanGroup;
use app\models\SiteVariable;
use yii\httpclient\Client;

/**
 * Class AlphabetTask
 * @package app\tasks
 */

class DatasetCountTask extends \webtoolsnz\scheduler\Task
{
    public $description = 'Count Datasets';
    // Every Hours
    public $schedule = '*/3 * * * *';

    public function run()
    {
        $var = SiteVariable::find(1)->one();
        $site = $var->site_url;
        $action = 'organization_list?all_fields=true';
        $method = 'GET';
        $api_key = $var->ckan_api_key;
        // init Client Request
        $baseUrl = $site . '/api/3/action';
        $client = new Client(['baseUrl' => $baseUrl]);
        $response = $client->createRequest()
            ->setHeaders(['Authorization' => $api_key])
            ->setMethod($method)
            ->setUrl($action)
            ->setData([])
            ->send();
        if ($response->isOk) {
            $response_data = (object) $response->getData();
            $no = 1;
            foreach ($response_data->result as $value) {
                echo $no++ . '.';
                $model = CkanDatasetCount::find()->where(['organization_id' => $value['id'], 'formated_date' => date('d-m-Y', time())])->one();
                if (is_null($model)) {
                    echo 'I-';
                    // Jika Null Insert Data Baru
                    $model_insert = new CkanDatasetCount;
                    $model_insert->organization_id = $value['id'];
                    $model_insert->dataset_count = $value['package_count'];
                    $model_insert->formated_date = date('d-m-Y', time());
                    $model_insert->updated_at = time();
                    $model_insert->save(false);
                } else {
                    // Cek Data Dobel
                    if (CkanDatasetCount::find()->where(['organization_id' => $value['id'], 'formated_date' => date('d-m-Y', time())])->count() == 1) {
                        echo 'U-';
                        // Update Data
                        $model->dataset_total = ($model->dataset_harvested_count > $model->dataset_count) ? $model->dataset_harvested_count : $model->dataset_count;
                        $model->dataset_count = $value['package_count'];
                        $model->updated_at = time();
                        $model->save(false);
                    } else {
                        echo 'Delete-';
                        // Ada Data Dobel Delete
                        $model->delete();
                    }
                }
            }
        } else {
            echo 'Failed get Response';
        }
    }
}
