<?php

namespace app\tasks;

use Yii;
use app\models\SiteVariable;
use app\models\CkanOrganization;

/**
 * Class AlphabetTask
 * @package app\tasks
 */
class OrganizationTask extends \webtoolsnz\scheduler\Task
{
    public $description = 'Organization List';
    public $schedule = '*/20 * * * *';
    public function run()
    {
        $var = SiteVariable::find(1)->one();
        $site = $var->site_url;
        $action = 'organization_list?all_fields=true';
        $method = 'GET';
        $api_key = '94106015-00ef-41d1-b750-2dbae360d054';
        $organization = Yii::$app->CkanApi->action($site, $action, [], $method, $api_key);
        foreach ($organization->result as $value) {
            $model = CkanOrganization::find()->where(['id_organization' => $value['id']])->one();
            if (is_null($model)) {
                $model_insert = new CkanOrganization;
                $model_insert->id_organization = $value['id'];
                $model_insert->name = $value['name'];
                $model_insert->title = $value['title'];
                $model_insert->description = $value['description'];
                $model_insert->image_display_url = $value['image_display_url'];
                // $model_insert->agency_type = $model->agency_type;
                $model_insert->created_at = time();
                $model_insert->updated_at = time();
                $model_insert->save(false);
            } else {
                $model->id_organization = $value['id'];
                $model->name = $value['name'];
                $model->title = $value['title'];
                $model->description = $value['description'];
                $model->image_display_url = $value['image_display_url'];
                // $model->agency_type = $model->agency_type;
                // $model->created_at = time();
                $model->updated_at = time();
                $model->save(false);
            }
        }
    }
}
