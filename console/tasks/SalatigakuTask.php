<?php

namespace app\tasks;

use Yii;
use app\models\NewsArticle;
use yii\httpclient\Client;

/**
 * Class AlphabetTask
 * @package app\tasks
 */
class SalatigakuTask extends \webtoolsnz\scheduler\Task
{
    public $description = 'Salatigaku';
    // Every Hours
    public $schedule = '0 */6 * * *';

    public function run()
    {
        $client = new Client(['baseUrl' => 'http://salatigaku.salatiga.go.id']);
        // Salatigaku Kuliner
        $kuliner = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('/webservice/service/kuliner')
            ->setData(['token' => md5('salatigaku')])
            ->send();
        if ($kuliner->isOk) {
            $get_kuliner = $kuliner->getData();
            foreach ($get_kuliner['datalist'] as $value) {
                $dta_kuliner[] = [
                    'regencies_id' => '3373',
                    'title' => $value['nama'],
                    'slug' => $this->stringToSlug($value['nama']),
                    'content' => $value['deskripsi'],
                    'cat_id' => '3',
                    'image_featured' => ($this->is_json($value['foto'])) ? $client->baseUrl . '/images/kuliner/' . json_decode($value['foto'])[0] : $client->baseUrl . '/images/kuliner/' . $value['foto'],
                    'timestamp_display_from' => '0',
                    'timestamp_display_until' => '0',
                    'geo_latitude' => $value['latitude'],
                    'geo_longitude' => $value['longitude'],
                ];
            }
        }

        $wisata = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('/webservice/service/wisata')
            ->setData(['token' => md5('salatigaku')])
            ->send();
        if ($wisata->isOk) {
            $get_wisata = $wisata->getData();
            foreach ($get_wisata['datalist'] as $value) {
                $dta_wisata[] = [
                    'regencies_id' => '3373',
                    'title' => $value['nama'],
                    'slug' => $this->stringToSlug($value['nama']),
                    'content' => $value['deskripsi'],
                    'cat_id' => '2',
                    'image_featured' => ($this->is_json($value['foto'])) ? $client->baseUrl . '/images/wisata/' . json_decode($value['foto'])[0] : $client->baseUrl . '/images/wisata/' . $value['foto'],
                    'timestamp_display_from' => '0',
                    'timestamp_display_until' => '0',
                    'geo_latitude' => $value['latitude'],
                    'geo_longitude' => $value['longitude'],
                ];
            }
        }

        $event = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('/webservice/service/event')
            ->setData(['token' => md5('salatigaku')])
            ->send();
        if ($event->isOk) {
            $get_event = $event->getData();
            foreach ($get_event['datalist'] as $value) {
                $dta_event[] = [
                    'regencies_id' => '3373',
                    'title' => $value['nama'],
                    'slug' => $this->stringToSlug($value['nama']),
                    'content' => $value['deskripsi'],
                    'cat_id' => '13',
                    'image_featured' => ($this->is_json($value['foto'])) ? $client->baseUrl . '/images/event/' . json_decode($value['foto'])[0] : $client->baseUrl . '/images/event/' . $value['foto'],
                    'timestamp_display_from' => strtotime($value['wkt_mulai']),
                    'timestamp_display_until' => strtotime($value['wkt_selesai']),
                    'geo_latitude' => $value['latitude'],
                    'geo_longitude' => $value['longitude'],
                ];
            }
        }

        $data_news = array_merge($dta_kuliner, $dta_wisata, $dta_event);

        foreach ($data_news as $value) {
            $news_slug_exist = NewsArticle::find()->where(['title' => $value['title']])->one();
            if (empty($news_slug_exist)) {
                $model = new NewsArticle();
                $model->regencies_id = $value['regencies_id'];
                $model->title = $value['title'];
                $model->slug = $value['slug'];
                $model->content = $value['content'];
                $model->cat_id = $value['cat_id'];
                $model->image_featured = $value['image_featured'];
                $model->timestamp_display_from = $value['timestamp_display_from'];
                $model->timestamp_display_until = $value['timestamp_display_until'];
                $model->geo_latitude = $value['geo_latitude'];
                $model->geo_longitude = $value['geo_longitude'];
                $model->save();
                // echo "done";
            } else {
                $news_slug_exist->regencies_id = $value['regencies_id'];
                $news_slug_exist->content = $value['content'];
                $news_slug_exist->cat_id = $value['cat_id'];
                $news_slug_exist->image_featured = $value['image_featured'];
                $news_slug_exist->timestamp_display_from = $value['timestamp_display_from'];
                $news_slug_exist->timestamp_display_until = $value['timestamp_display_until'];
                $news_slug_exist->geo_latitude = $value['geo_latitude'];
                $news_slug_exist->geo_longitude = $value['geo_longitude'];
                $news_slug_exist->save();
            }
        }
    }

    public function is_json($string, $return_data = false)
    {
        $data = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
    }

    public function stringToSlug($str)
    {
        $slug = preg_replace('@[\s!:;_\?=\\\+\*/%&#]+@', '-', $str);
        //this will replace all non alphanumeric char with '-'
        $slug = mb_strtolower($slug);
        //convert string to lowercase
        return $slug = trim($slug, '-');
        //trim whitespaces
    }
}
