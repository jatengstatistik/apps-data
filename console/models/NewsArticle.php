<?php

namespace app\models;

use Yii;
use app\models\NewsCat;
use app\models\User;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "news_article".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property int $cat_id
 * @property int $image_featured
 * @property string $image_list
 * @property string $file_list
 * @property int $create_user_id
 * @property int $update_user_id
 * @property int $timestamp_create
 * @property int $timestamp_update
 * @property int $timestamp_display_from
 * @property int $timestamp_display_until
 * @property int $is_deleted
 * @property int $is_display_limit
 * @property string $teaser_text
 * @property string $geo_location
 */
class NewsArticle extends \yii\db\ActiveRecord
{

    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    /**
     * @var UploadedFile|Null file attribute
     */
    public $file_list;
    public $address;
    public $longitude;
    public $latitude;
    // public $location;

    public $status_list = [
        self::STATUS_ACTIVE => 'Aktif',
        self::STATUS_DELETED => 'Nonaktif',
    ];

    public function behaviors()
    {
     return [
        [
            'class' => SluggableBehavior::className(),
            'attribute' => 'title',
            'slugAttribute' => 'slug',
            'ensureUnique'=> true,
        ],
        'timestamp' => [
            'class' => TimestampBehavior::className(),
            'attributes' => [
                 ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp_create', 'timestamp_update'],
                 ActiveRecord::EVENT_BEFORE_UPDATE => ['timestamp_update'],
            ],
        ],
        'blameable' => [
                  'class' => BlameableBehavior::className(),
                  'createdByAttribute' => 'create_user_id',
                  'updatedByAttribute' => 'update_user_id',
            ],
     ];
    }
   
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['file','image_featured'],'file'],
            [['title', 'content','image_featured', 'image_list', 'teaser_text', 'timestamp_display_from', 'timestamp_display_until','slug'], 'string'],
            [['create_user_id', 'update_user_id', 'timestamp_create', 'timestamp_update', 'is_deleted', 'is_display_limit'], 'integer'],
            [['geo_location','geo_longitude','geo_latitude'], 'string', 'max' => 255],
            [['cat_id','regencies_id'],'string'],
            ['is_deleted', 'default', 'value' => self::STATUS_ACTIVE],
            ['is_deleted', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'regencies_id' => Yii::t('app', 'Regencies'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug Title'),
            'content' => Yii::t('app', 'Content'),
            'cat_id' => Yii::t('app', 'Category'),
            'image_featured' => Yii::t('app', 'Featured Image'),
            'image_list' => Yii::t('app', 'Image List'),
            'file' => Yii::t('app', 'File List'),
            'create_user_id' => Yii::t('app', 'Create User ID'),
            'update_user_id' => Yii::t('app', 'Update User ID'),
            'timestamp_create' => Yii::t('app', 'Timestamp Create'),
            'timestamp_update' => Yii::t('app', 'Timestamp Update'),
            'timestamp_display_from' => Yii::t('app', 'Timestamp Display From'),
            'timestamp_display_until' => Yii::t('app', 'Timestamp Display Until'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
            'is_display_limit' => Yii::t('app', 'Is Display Limit'),
            'teaser_text' => Yii::t('app', 'Teaser Text'),
            'geo_location' => Yii::t('app', 'Geo Location'),
            'geo_longitude' => Yii::t('app', 'Geo Longitude'),
            'geo_latitude' => Yii::t('app', 'Geo Latitude'),
        ];
    }

    public function getStatus($status)
    {
        return $this->status_list[$status];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegencies()
    {
        return $this->hasOne(Regencies::className(), ['id' => 'regencies_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasOne(NewsCat::className(), ['id' => 'cat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreate()
    {
        return $this->hasOne(User::className(), ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserUpdate()
    {
        return $this->hasOne(User::className(), ['id' => 'update_user_id']);
    }

    public function beforeSave($insert){

        // if ($insert) {

        // }else{
            // $post_data = Yii::$app->request->post();
            // $this->geo_location = $post_data['NewsArticle']['geo_location'];
            // if (!empty($post_data['NewsArticle']['latitude'])) {
            //     $this->geo_latitude = $post_data['NewsArticle']['latitude'];
            //     $this->geo_longitude = $post_data['NewsArticle']['longitude'];
            // }
            // $this->content = str_replace('<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>', "", $this->content);
        // }

        return parent::beforeSave($insert);

    }

    public function beforeValidate() 
    {
        // if ($this->isNewRecord) {

        // }else{
            // $post_data = Yii::$app->request->post();
            // if (!empty($post_data)) {
            //     $this->geo_location = $post_data['NewsArticle']['geo_location'];
            //     if (!empty($post_data['NewsArticle']['latitude'])) {
            //         $this->geo_latitude = $post_data['NewsArticle']['latitude'];
            //         $this->geo_longitude = $post_data['NewsArticle']['longitude'];
            //     }
            //     $this->content = str_replace('<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>', "", $this->content);   
            // }
        // }
        return parent::beforeValidate();
    }
}
