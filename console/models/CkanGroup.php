<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ckan_group".
 *
 * @property int $id
 * @property string $id_group
 * @property string $title
 * @property string $name
 * @property string $description
 * @property string $display_name
 * @property string $image_display_url
 * @property string $image_url
 * @property int $package_count
 * @property int $created_at
 * @property int $updated_at
 */
class CkanGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ckan_group';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
             ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['package_count', 'created_at', 'updated_at'], 'integer'],
            [['id_group', 'title', 'name', 'display_name'], 'string', 'max' => 100],
            [['description', 'image_display_url', 'image_url'], 'string', 'max' => 255],
            [['id_group'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_group' => Yii::t('app', 'Id Group'),
            'title' => Yii::t('app', 'Title'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'display_name' => Yii::t('app', 'Display Name'),
            'image_display_url' => Yii::t('app', 'Image Display Url'),
            'image_url' => Yii::t('app', 'Image Url'),
            'package_count' => Yii::t('app', 'Package Count'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
