<?php

namespace app\models;

use Yii;
use app\models\AccountsOds;

/**
 * This is the model class for table "ckan_organization".
 *
 * @property string $id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property string $image_display_url
 */
class CkanOrganization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ckan_organization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id','created_at','updated_at'],'integer'],
            [['id_organization','agency_type'], 'string', 'max' => 50],
            // ['agency_type','default', 'value' => 'SKPD'],
            [['name', 'title'], 'string', 'max' => 200],
            [['description', 'image_display_url'], 'string', 'max' => 255],
            [['id_organization'], 'unique'],
            [['name'], 'unique'],
             [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => AccountsOds::className(), 'targetAttribute' => ['account_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_organization' => Yii::t('app', 'CKAN ID'),
            'agency_type' => Yii::t('app', 'Type'),
            'name' => Yii::t('app', 'Name'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'image_display_url' => Yii::t('app', 'Image Display Url'),
            'account_id' => Yii::t('app', 'Account Ods'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCkanuser()
    {
        return $this->hasOne(AccountsOds::className(), ['id' => 'account_id']);
    }
}
