<?php

/* @var $this \yii\web\View */
/* @var $content string */

use common\widgets\Alert;
use frontend\assets\SimpleAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

SimpleAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- <title><?= Html::encode($this->title) ?></title> -->
    <title>Open Data Jawa Tengah</title>
    <meta name="description" content="Portal Data Provinsi Jawa Tengah">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7COpen+Sans:300,300i,400,400i,600,600i,700,800" rel="stylesheet">
    
    <?php $this->registerCsrfMetaTags() ?>
    
    <!-- <link rel="icon" type="image/png" href="http://data.jatengprov.go.id/themes/opendata/images/icons/jateng.png"/> -->
    <link rel="icon" type="image/png" href="<?=Yii::$app->request->baseUrl?>/uploads/images/jateng.png">
    <?php $this->head() ?>
    
</head>
<body>
<?php $this->beginBody() ?>
<div id="page-loader" class="white">
    <div class="sk-wave">
        <div class="sk-rect sk-rect1"></div>
        <div class="sk-rect sk-rect2"></div>
        <div class="sk-rect sk-rect3"></div>
        <div class="sk-rect sk-rect4"></div>
        <div class="sk-rect sk-rect5"></div>
    </div>
</div>
<div class="wrapper">
    <header class="header header2 gray sticky-header">
        <div class="header-inner">
            <div class="container">
                <a href="<?= Url::to(['/']) ?>" class="site-logo" title="<?= $this->title ?>">
                    <img src="<?=Yii::$app->request->baseUrl?>/uploads/images/jateng.png" height="50px" width="60px">
                    
                </a>
                <div class="dropdown header-dropdown cart-dropdown">
                    <a href="<?= Url::to(['/']) ?>" class="site-logo" title="<?= $this->title ?>">
                        <img src="<?=Yii::$app->request->baseUrl?>/uploads/images/opendata-2.png" height="40px" width="70px">
                    </a>
                </div>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-container" aria-expanded="false">
                    <span class="toggle-text">Menu</span>
                    <span class="toggle-wrapper">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </span>
                </button>
                <div class="collapse navbar-collapse" id="main-nav-container">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="<?= Url::to(['/','slug'=>'jelajah-daerah']); ?>" role="button" aria-expanded="false"><?= strtoupper("Jelajah Daerah") ?></a>
                        </li>
                        <li class="dropdown">
                            <a href="<?= Url::to(['/','slug'=>'berita-statistik']); ?>" role="button" aria-expanded="false"><?= strtoupper("Berita Statistik") ?></a>
                        </li>
                        <li class="dropdown">
                            <a href="/dataset" role="button" aria-expanded="false"><?= strtoupper("Data & Informasi") ?></a>
                        </li>
                        <li class="dropdown">
                            <a href="<?= Url::to(['/','menu'=>'infografis']); ?>" role="button" aria-expanded="false"><?= strtoupper("Info Grafis") ?></a>
                        </li>
                        <li class="dropdown">
                            <a href="http://ppid.jatengprov.go.id/permohonan-informasi" role="button" aria-expanded="false"><?= strtoupper("Permintaan Data") ?></a>
                        </li>
                        <li>
                            <a href="/dataset?tags=ProdukHukum" role="button" aria-expanded="false"><?= strtoupper("Produk Hukum") ?></a>
                        </li>
                        <li class="dropdown">
                            <a href="/user/login" role="button" aria-expanded="false"><?= strtoupper("Login") ?></a>
                        </li>
                        <li class="dropdown search-dropdown pull-right">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="visible-inline-xs">Search</span><i class="fa fa-search"></i></a>
                            <div class="dropdown-menu " role="menu">
                                <form action="/dataset" method="GET">
                                    <input type="text" name="q" class="form-control" placeholder="Search..." required>
                                    <button type="submit" class="btn btn-custom"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    
        <?= $content ?>
    
    <?php $this->endBody() ?>    
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="widget about-widget">
                        <img src="<?=Yii::$app->request->baseUrl?>/uploads/images/opendata-1.png" width="70px" height="70px">
                        <a href="https://data.go.id">
                            <img src="<?=Yii::$app->request->baseUrl?>/uploads/images/satudata-id.png" width="80px" height="80px">
                        </a>
                        <hr>
                        <address>
                            <label>Get In Touch:</label> <br>
                            Jl. Menteri Supeno I/2 Semarang – Kota Semarang, Jawa Tengah 50243<br><br>
                            Telp: 024 - 8319140 | Fax: 024 - 8443916<br>
                            <a href="mailto:diskominfo@jatengprov.go.id">diskominfo@jatengprov.go.id</a>
                        </address>
                    </div>
                </div>

                <div class="col-md-5 col-sm-6">
                    <div class="widget">
                        <h4 class="widget-title" align="center">Temukan Kami</h4> 

                        <div class="google-maps">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d934.4643528981549!2d110.41789499753693!3d-6.9949381226165475!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3fe7ccb54333b13!2sDinas%20Komunikasi%20dan%20Informatika%20Provinsi%20Jawa%20Tengah!5e0!3m2!1sid!2sid!4v1614737347377!5m2!1sid!2sid" width="600" height="450" frameborder="0" style="border:0"></iframe>
                        </div>
                    </div>
                </div>

                <div class="clearfix visible-sm"></div>

                <div class="col-md-3 col-sm-6">
<!--                    
                    <div class="widget">
                        <h4 class="widget-title">Tagcloud</h4>
                        <div class="tagcloud">
                            <a href="#">Design</a>
                            <a href="#">Develop</a>
                            <a href="#">Seo</a>
                            <a href="#">jQuery</a>
                            <a href="#">Wordpress</a>
                            <a href="#">Angular</a>
                            <a href="#">Node</a>
                            <a href="#">Express</a>
                            <a href="#">Gulp</a>
                            <a href="#">Sass</a>
                            <a href="#">Bootstrap</a>
                            <a href="#">Html5</a>
                            <a href="#">Css3</a>
                            <a href="#">Node</a>
                            <a href="#">Retina</a>
                            <a href="#">Sprites</a>
                            <a href="#">Html5</a>
                            <a href="#">Css3</a>
                            <a href="#">Node</a>
                            <a href="#">Seo</a>
                            <a href="#">jQuery</a>
                            <a href="#">Sass</a>
                        </div>
                    </div>
-->
                </div>
    
                <div class="col-md-3 col-sm-6">
                    <div class="widget flickr-widget" id="visitorToday">
                        <!-- <h4 class="widget-title">Flickr</h4> -->

                    </div>
                </div>
                    
            </div>
        </div>

        <div class="footer-bottom">
            <div class="container">
                <div class="footer-right">
                    <p class="copyright"><?php echo "Copyright © 2018 - " . (int)date('Y')?> Pemerintah Provinsi Jawa Tengah - Theme by  <a href="http://eonythemes.com/" target="_blank" title="eonythemes">eonythemes</a>.</p>
                </div>
                
                <div class="social-icons">
                    <label>Find Us:</label>
                    <a href="https://twitter.com/jatengopendata" class="social-icon" title="Twitter"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.instagram.com/opendata_jateng/" class="social-icon" title="Instagram"><i class="fa fa-instagram"></i></a>

<!--                    
                    <a href="#" class="social-icon" title="Github"><i class="fa fa-github"></i></a>
                    <a href="#" class="social-icon" title="Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="social-icon" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                    <a href="#" class="social-icon" title="Tumblr"><i class="fa fa-tumblr"></i></a>
                    <a href="#" class="social-icon" title="Flickr"><i class="fa fa-flickr"></i></a>
                    <a href="#" class="social-icon" title="Github"><i class="fa fa-github"></i></a>
                    <a href="#" class="social-icon" title="Ge"><i class="fa fa-ge"></i></a>
                    <a href="#" class="social-icon" title="Pinterest"><i class="fa fa-pinterest"></i></a>
                    <a href="#" class="social-icon" title="Codepen"><i class="fa fa-codepen"></i></a>
-->
                </div>
            </div>
        </div>
    </footer>
</div>


<a id="scroll-top" href="#top" title="Scroll top"><i class="fa fa-angle-up"></i></a>

<style type="text/css">
    .google-maps {
        position: relative;
        padding-bottom: 50%;
        height: 0;
        overflow: hidden;
    }

    .google-maps iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100% !important;
        height: 100% !important;
    }
    /* End Responsive maps */

    .page-header.larger {
        padding: 20px 0;
    }
    a.site-logo {
        display: block;
        float: left;
        margin: 10px 0;
        margin-top: 10px;
        margin-right: 0px;
        margin-bottom: 10px;
        margin-left: 0px;
    }
    .dropdown.cart-dropdown > a {
        padding-top: 0px; 
        padding-bottom: 0px; 
    }
    .header2.sticky-header.fixed .collapse.navbar-collapse {
        margin-top: 20px;
    }
</style>
<script type="text/javascript">
    jQuery(document).ready(function() {
        "use strict";

        var revapi;
        if ( $("#rev_slider").revolution == undefined ) {
            revslider_showDoubleJqueryError("#rev_slider");
        } else {
            revapi = $("#rev_slider").show().revolution({
                sliderType: "standard",
                jsFileLocation: "assets/js/",
                sliderLayout: "auto",
                dottedOverlay:"none",
                delay: 15000,
                navigation: {
                    mouseScrollNavigation: "off",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on"
                    },
                    arrows: {
                        style: "custom",
                        enable: true,
                        hide_onmobile: false,
                        hide_under: 768,
                        hide_onleave: false,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 0,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 0,
                            v_offset: 0
                        }
                    },
                    bullets: {
                        enable: true,
                        style: "hades",
                        hide_onmobile: false,
                        hide_onleave: false,
                        hide_under: 768,
                        direction: "horizontal",
                        h_align: "center",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 15,
                        space: 5,
                        tmp: '<span class="tp-bullet-image"></span>'
                    }
                },
                responsiveLevels: [1200,992,768,480],
                gridwidth: [1140,970,750,480],
                gridheight: [520,440,360,300],
                lazyType: "smart",
                spinner: "spinner2",
                parallax: {
                    type: "off",
                    origo: "slidercenter",
                    speed: 2000,
                    levels: [2,3,4,5,6,7,12,16,10,50],
                    disable_onmobile: "on"
                },
                debugMode: false
            });
        }
    });
</script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109573835-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-109573835-1');
</script>
<script type="text/javascript">
    $(document).ready(function(){
    // $('#onload').modal('show');
    $('#modal-default-headline').modal('show');
    // http://data.jatengprov.go.id/apps/api/visitor
    $.ajax({
      url: 'http://data.jatengprov.go.id:8182/visitor/now',
      success: function(data) {
        $("#visitorToday").append('<h4 class="widget-title"> Kunjungan <br><br><br> Hari Ini '+data+'</h4>');
        // console.log(data);
        
      }
    });
    // http://data.jatengprov.go.id/apps/api/visitor/weekstats
    $.ajax({
      url: 'http://data.jatengprov.go.id:8182/visitor/weekly',
      success: function(weekData) {
        // $("#visitor").append(data);
        // console.log(weekData);
      }
    }).done(function(weekData) { 

        var items = [];

        $.each(weekData, function(i, item) { 
            $("#visitorToday").append('<h4 class="widget-title"> Minggu '+item.week+'  '+item.counter+'</h4>');
            // console.log(item.counter);
            /* Note your JSON has objects nested inside one another so for genres you'll
            need to do another loop for that object */
        })
        ;

    });


    });
    </script>
<?php $this->endPage() ?>
</body>
</html>
