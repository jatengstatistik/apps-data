<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NewsArticleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-article-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'content') ?>

    <?= $form->field($model, 'cat_id') ?>

    <?= $form->field($model, 'image_id') ?>

    <?php // echo $form->field($model, 'image_list') ?>

    <?php // echo $form->field($model, 'file') ?>

    <?php // echo $form->field($model, 'create_user_id') ?>

    <?php // echo $form->field($model, 'update_user_id') ?>

    <?php // echo $form->field($model, 'timestamp_create') ?>

    <?php // echo $form->field($model, 'timestamp_update') ?>

    <?php // echo $form->field($model, 'timestamp_display_from') ?>

    <?php // echo $form->field($model, 'timestamp_display_until') ?>

    <?php // echo $form->field($model, 'is_deleted') ?>

    <?php // echo $form->field($model, 'is_display_limit') ?>

    <?php // echo $form->field($model, 'teaser_text') ?>

    <?php // echo $form->field($model, 'geo_location') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
