<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NewsArticle */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cat_id')->textInput() ?>

    <?= $form->field($model, 'image_id')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'image_list')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'create_user_id')->textInput() ?>

    <?= $form->field($model, 'update_user_id')->textInput() ?>

    <?= $form->field($model, 'timestamp_create')->textInput() ?>

    <?= $form->field($model, 'timestamp_update')->textInput() ?>

    <?= $form->field($model, 'timestamp_display_from')->textInput() ?>

    <?= $form->field($model, 'timestamp_display_until')->textInput() ?>

    <?= $form->field($model, 'is_deleted')->textInput() ?>

    <?= $form->field($model, 'is_display_limit')->textInput() ?>

    <?= $form->field($model, 'teaser_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'geo_location')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
