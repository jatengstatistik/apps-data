<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News Articles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-article-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create News Article'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title:ntext',
            'content:ntext',
            'cat_id',
            'image_id:ntext',
            //'image_list:ntext',
            //'file:ntext',
            //'create_user_id',
            //'update_user_id',
            //'timestamp_create:datetime',
            //'timestamp_update:datetime',
            //'timestamp_display_from:datetime',
            //'timestamp_display_until:datetime',
            //'is_deleted',
            //'is_display_limit',
            //'teaser_text:ntext',
            //'geo_location:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
