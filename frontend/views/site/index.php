<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Open Data Jawa Tengah';
?>
<div class="runing-text-header-bottom">
    <div class="runing-text-top-text">
        <marquee scrollamount="5"><?php echo $runing_text; ?></marquee>
    </div>
</div>  
<div id="rev_slider_wrapper" class="slider-container-index rev_slider_wrapper rev_container_1 fullwidthbanner-container" data-alias="classicslider1">
    <div id="rev_slider" class="rev_slider fullwidthabanner" style="display:none;">
        <ul>
            <?php foreach ($carousel as $key => $value): ?>
                <li data-index="rs-<?= $key+1 ?>" data-transition="fade" data-slotamount="7" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1200" data-rotate="0" data-thumb="<?= Yii::$app->request->baseUrl.$value->media->url ?>" data-saveperformance="off" data-title="Success">
                
                <img src="<?= Yii::$app->request->baseUrl.$value->media->url ?>" alt="Slider bg 1" data-bgposition="center center" data-bgfit="cover" data-duration="11000" data-bgparallax="10" data-ease="Linear.easeNone" class="rev-slidebg" data-no-retina>
                                
                <div class="tp-caption tp-shape tp-shapewrapper rs-parallaxlevel-0" 
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                    data-y="['center','center','center','center']" data-voffset="['0','0','0','0']" 
                    data-width="full"
                    data-height="full"
                    data-whitespace="nowrap"
                    data-transform_idle="o:0.5;"
                    data-transform_in="opacity:0;s:1500;" 
                    data-transform_out="o:0;s:1000;" 
                    data-start="1000" 
                    data-basealign="slide" 
                    data-responsive_offset="on" 
                    data-responsive="off"
                    style="z-index: 3; background-color: rgba(0,0,0, 0.5);"> 
                </div>

                <div class="tp-caption tp-resizeme rs-parallaxlevel-0" 
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                    data-y="['center','center','center','center']" data-voffset="['-81','-71','-65','-60']" 
                    data-fontsize="['17','15','14','12']"
                    data-fontweight="400"
                    data-lineheight="['20','18','16','16']"
                    data-color="#fff"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-frames='[{"delay":1400,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":700,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                    data-responsive_offset="on" 
                    data-elementdelay="0" 
                    style="z-index: 5; white-space: nowrap; letter-spacing: 3px; text-transform: uppercase;">
                    Portal Data 
                </div>

                <div class="tp-caption tp-resizeme rs-parallaxlevel-0" 
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                    data-y="['center','center','center','center']" data-voffset="['-26','-22','-20','-21']" 
                    data-fontsize="['64','56','48','38']"
                    data-fontweight="700"
                    data-lineheight="['70','64','60','46']"
                    data-color="#fff"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-frames='[{"delay":1800,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":700,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                    data-responsive_offset="on" 
                    style="z-index: 6; text-shadow:0 1px 3px rgba(0,0,0, 0.2); letter-spacing: 1.5px; text-transform: uppercase;">Jawa Tengah
                </div>

                <div class="tp-caption tp-resizeme rs-parallaxlevel-0" 
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                    data-y="['center','center','center','center']" data-voffset="['32','31','29','20']" 
                    data-width="none"
                    data-height="none"
                    data-fontsize="['24','20','18','14']"
                    data-fontweight="400"
                    data-lineheight="['30','24','22','20']"
                    data-visibility="['on', 'on', 'on', 'off']"
                    data-color="#fff"
                    data-whitespace="nowrap"
                    data-frames='[{"delay":2200,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":700,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                    data-responsive_offset="on" 
                    style="z-index: 7; text-shadow:0 1px 1px rgba(0,0,0, 0.1); letter-spacing: 2px; text-transform: uppercase;">
                    Informasi Jawa Tengah Dalam Satu Portal
                </div>

                <div class="tp-caption tp-resizeme rs-parallaxlevel-0" 
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                    data-y="['center','center','center','center']" data-voffset="['100','94','84','74']" 
                    data-width="none"
                    data-height="none"
                    data-fontsize="['20','16','14','10']"
                    data-fontweight="300"
                    data-lineheight="['80','60','40','50']"
                    data-visibility="['on', 'on', 'on', 'off']"
                    data-color="#fff"
                    data-whitespace="nowrap"
                    data-frames='[{"delay":2200,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":700,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                    data-responsive_offset="on" 
                    style="z-index: 8; text-shadow:0 1px 1px rgba(0,0,0, 0.1); letter-spacing: 2px;">
                    (<?= $value->caption ?>)
                </div>

            </li>
            <?php endforeach ?>
        </ul>
        <div class="tp-bannertimer tp-bottom" style="height: 4px; background-color: rgba(255, 255, 255, 0.35);"></div>
    </div>
    </div>
    
    <div class="mb55 mb45-sm mb35-xs"></div>
    <div class="container">
        <h2 class="title custom text-center">Mau Cari Apa?</h2>
        <form action="/dataset" method="GET">
            <input type="text" name="q" class="form-control" placeholder="Cari Data" required="">
        </form>
    </div>
    
<div class="mb25"></div>
    <div class="bg-lightgray border pt55 pb40">
        <div class="container">
            <header class="title-block text-center">
                <h2 class="title mb10 custom text-center">Tema</h2>
            </header>
            <div class="row">
                <?php foreach ($group_list as $value): ?>
                <div class="col-sm-6 col-md-3">
                    <div class="text-block hover-bg light" style="background-image:url(<?php echo $value['image_display_url']; ?>)">
                        <h3 class="block-title"><a href="#"><?= $value['display_name'] ?></a></h3>
                        <p><?php echo $value['description']; ?></p>
                        <a href="<?php echo '/group/'.$value['name']; ?>" class="readmore dark"><?php echo $value['package_count']; ?> Dataset <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
<div class="mb25"></div>

<div class="mb25"></div>
    <div class="container">
        <header class="title-block text-center">
            <h2 class="title mb10 custom text-center">Indikator Strategis</h2>
            <p>Badan Pusat Statistik Provinsi Jawa Tengah</p>
        </header>
        <div class="clients-carousel owl-carousel" align="center">
            <?php foreach ($indikator_makro as $value): ?>
                    <div class="client">
                        <h4 style="margin-top: 5px"><?= $value->name ?></h4> 
                        <img class="img-thumbnail" style="width: 60%; height: 60%" src="<?= Yii::$app->request->baseUrl.$value->image ?>" alt="">  
                        <span class="count-cust"><?= $value->value ?></span>
                        <p style="margin-bottom: 30px"><?= $value->desc ?></p>
                    </div>
            <?php endforeach ?>
        </div>
    </div>
<div class="mb25"></div>

<div class="mb25"></div>
    <div class="bg-lightgray border pt55 pb40">
         <header class="title-block text-center">
            <h2 class="title mb10 custom text-center">Data Terbaru</h2>
        </header>
        <div class="row">
            <div class="col-md-12">
                <div class="mb5"></div>
                <?php foreach ($recent_dataset as $value): ?>
                    <div class="service text-center">
                        <h3 class="service-title"><a href="<?php echo '/dataset/'.$value->name; ?>"><?php echo $value->title; ?></a></h3>
                        <p><?php echo $value->notes; ?></p>
                    </div>    
                <?php endforeach ?>
            </div>
        </div>
    </div>
<div class="mb25"></div>

<div class="mb25"></div>
    <div class="bg border pt55 pb40">
        <div class="container">
            <header class="title-block text-center">
                <h2 class="title mb10 custom text-center">Info Grafis</h2>
            </header>
            <ul id="portfolio-filter" class="text-center mb40">
                <li class="active"><a href="#" data-filter="*">All</a></li>
                    <?php foreach ($cats_brs as $cat): ?>
                        <li><a href="#" data-filter=".<?= $cat->id ?>"><?= $cat->title ?></a></li>    
                    <?php endforeach ?>
            </ul>

            <div id="portfolio-item-container" class="popup-gallery max-col-4 mb60" data-layoutmode="fitRows">
                <?php foreach ($info_grafis as $value): ?>
                <div class="portfolio-item portfolio-anim <?= $value->cat_id ?>">
                    <figure>
                        <img src="<?php echo Yii::$app->request->baseUrl.'/'.$value->image_featured; ?>" alt="Portfolio Name">
                        <figcaption>
                            <!-- <?=Yii::$app->request->baseUrl?> -->
                            <a href="<?php echo Yii::$app->request->baseUrl.'/'.$value->image_featured; ?>" data-thumb="<?php echo Yii::$app->request->baseUrl.'/'.$value->image_featured; ?>" class="zoom-btn"><i class="fa fa-search"></i></a>
                            <a href="<?= Url::to(['/','slug' =>'jelajah-daerah','id' => $value->id]); ?>" class="link-btn"><i class="fa fa-link"></i></a>
                        </figcaption>
                    </figure>
                    <div class="portfolio-meta">
                        <h3 class="portfolio-title"><a href="#" title="Portfolio name"><?= $value->title; ?></a></h3>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
<div class="mb25"></div>

<div class="mb25"></div>
    <div class="bg-lightgray border pt55 pb40">
        <div class="container">
            <header class="title-block text-center">
                <h2 class="title mb10 custom text-center">Latest News</h2>
            </header>
            <div class="blog-row">
                <div id="blog-item-container" class="max-col-4">
                    <?php foreach ($article as $value): ?>
                    <article class="entry entry-grid">
                        <div class="entry-media">
                            <figure>
                                <a href="<?= Url::to(['/', 'slug' =>'jelajah-daerah', 'id' => $value->id]); ?>"><img width="750px" width="380px" src="<?php echo (strstr($value->image_featured, 'http'))? $value->image_featured : Yii::$app->request->baseUrl.'/'.$value->image_featured; ?>" alt="Post image"></a>
                            </figure>
                            <div class="entry-meta">
                                <span><i class="fa fa-calendar"></i><?php echo Yii::$app->formatter->asDate($value->timestamp_create); ?></span>
                                <a href="#"><i class="fa fa-user"></i> Admin</a>
                            </div>
                        </div>
                        <h2 class="entry-title"><a target="__blank" href="https://maps.google.com/?q=<?= $value->geo_latitude ?>,<?= $value->geo_longitude ?>"><i class="fa fa-location-arrow"></i></a><a href="<?= Url::to(['/', 'slug' =>'jelajah-daerah', 'id' => $value->id]); ?>"><?= $value->title; ?></a></h2>
                        <div class="entry-content">
                            <p><?php
                                $text  = implode(' ', array_slice(explode(' ', $value->content), 0, 10));
                                echo $imgRm = preg_replace('/<img[^>]+\>/i', "", $text);
                                // echo $brRm = preg_replace("/<br[^>]+>/", "", $imgRm);
                                 ?></p>
                            <a href="<?= Url::to(['/','slug' =>'jelajah-daerah','id' => $value->id]); ?>" class="readmore">Read more<i class="fa fa-angle-right"></i></a>
                        </div>
                    </article>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
<div class="mb25"></div>

<div class="mb25"></div>
    <div class="container">
        <header class="title-block text-center">
            <h2 class="title mb10 custom text-center">Link Terkait</h2>
        </header>
        <div class="clients-carousel owl-carousel">
            <?php foreach ($banner as $value): ?>
                <a href="<?= $value->link ?>" class="client" target="_blank" title="<?= $value->title ?>">
                    <img src="<?= Yii::$app->request->baseUrl.$value->media->url ?>" alt="<?= $value->title ?>">
                </a>
            <?php endforeach ?>
        </div>
    </div>
<div class="mb25"></div>

<?php if (count($headline)>0): ?>
<div class="modal fade" id="modal-default-headline">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Headline Open Data Jateng</h4>
      </div>
      <div class="modal-body">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <?php foreach ($headline as $key => $value): ?>
                    <div class="item <?php echo ($key==0) ? "active" : null?>">
                        <img src="<?php echo (strstr($value->image_featured, 'http'))? $value->image_featured : Yii::$app->request->baseUrl.'/'.$value->image_featured; ?>">

                        <div class="btn-block">
                          <a type="button" href="<?= Url::to(['/','slug' =>'jelajah-daerah','id' => $value->id]); ?>" class="btn btn-block btn-danger btn-xs"><?= $value->title ?></a>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
              <span class="fa fa-angle-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
              <span class="fa fa-angle-right"></span>
            </a>
          </div>
      </div>
    </div>
  </div>
</div>
<?php endif ?>

<style type="text/css" media="screen">
    body {
        background-color: white;
    }
    .runing-text-header-bottom {
        background-color: #e53935;
    }
    .runing-text-top-text {
        color: white;
    }
    .count-cust {
        /* display: block; */
        color: #e53935;
        font-size: 26px;
        font-weight: 600;
    }
</style>