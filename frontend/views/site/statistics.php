<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use acerix\yii2\readmore\Readmore;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
// use yii\bootstrap\ActiveForm;
// use yii\captcha\Captcha;

$this->title = "Dataset Statistik";
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="CkanOrganizationSearch">
 
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
 
    <?= $form->field($model, 'globalSearch') ?>
 
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>
 
    <?php ActiveForm::end(); ?>
 
</div>

<div class="main">
    <div class="page-header larger parallax custom" <!-- style="background-image:url(theme/simple/assets/images/page-header-bg.jpg) -->">
        <div class="container">
            <h1><?= $retVal = (empty($this->title)) ? $slug : $this->title; ?></h1>
            <ol class="breadcrumb">
                <li><a href="<?= Url::to(['/']) ?>">Home</a></li>
                <!-- <li><a href="">Infografis</a></li> -->
                <li class="active"><?= $this->title; ?></li>
            </ol>
        </div>
    </div>

    <div class="container">
      <h2 class="title-border custom mb25">Statistik Dataset</h2>

      <div class="row">
          <?php foreach ($model as $value): ?>
          <div class="col-sm-6 col-md-3">
              <div class="text-block" align="center" style="; height: 230px;">
                  <figure>
                      <a href="#"><img width="100px" height="100px" src="<?= $value->organization->image_display_url ?>" ></a>
                  </figure>
                  <h3 class="block-title"><a href="#"><?= $value->organization->title ?></a></h3>
                  <!-- <p>Lorem ipsum dolor sit amet, consec tetur adipisicing elit. Iusto debitis nemo ipsa iure aliquid systems.</p> -->
                  <a href="<?= (empty($value->organization->ckanuser['site'])) ? 'http://data.jatengprov.go.id/organization/'.$value->organization->name : $value->organization->ckanuser['site'].'/dataset' ?>" class="readmore custom">Total Dataset <?= $value->dataset_count  ?></a>
              </div>
          </div>
          <?php endforeach ?>
      </div>
  </div>
    
  <nav class="pagination-container">
        <?php 
            echo LinkPager::widget([
                'pagination' => $pagination,
            ]);
         ?>
    </nav>
</div>
<style type="text/css" media="screen">
    body {
        background-color: white;
    }
</style>