<?php 
// use acerix\yii2\readmore\Readmore;
use yii\helpers\Html;
use yii\helpers\Url;
// use yii\bootstrap\ActiveForm;
// use yii\captcha\Captcha;

// echo '<pre>';
// print_r(Yii::$app->request->get());
// echo '</pre>';

$this->title = $cat;
 ?>
<div class="page-header larger parallax custom">
    <div class="container">
        <h1><?= $this->title ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= Url::to(['/']); ?>">Home</a></li>
            <li><a href="<?= Url::to(['/','slug'=>$slug]) ?>"><?= $cat ?></a></li>
            <li class="active"><?= $model->title ?></li>
        </ol>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-push-3">
            <article class="entry">
                <div class="entry-media">
                    <figure>
                        <img width="750px" height="380px" src="<?php echo (strstr($model->image_featured, 'http'))? $model->image_featured : Yii::$app->request->baseUrl.'/'.$model->image_featured; ?>" alt="entry image">
                    </figure>
                </div>

                <h2 class="entry-title"><?php echo $model->title; ?></h2>
                <div class="entry-meta">
                    <span><a target="__blank" href="https://maps.google.com/?q=<?= $model->geo_latitude ?>,<?= $model->geo_longitude ?>"><i class="fa fa-map-marker"></i> <?= ucwords($model->regencies->name)?></a></span>
                    <span><i class="fa fa-calendar"></i> <?php echo Yii::$app->formatter->asDate($model->timestamp_create); ?></span>
                    <!-- <a href="#"><i class="fa fa-comments"></i>8 Comment(s)</a> -->
                    <a href="#"><i class="fa fa-user"></i> Admin</a>
                </div>

                <div class="entry-content">
                    <!-- <img alt="Telur asin" src="/uploads/content/1556770354554.png" class="fr-fic fr-dii"> -->
                <p><?php 
                        // echo Yii::$app->request->baseUrl;
                        // echo 'src="'.Yii::$app->request->baseUrl.'/';
                        // Change url image uploadded in production - Updated 20200303
                        echo str_replace('src="', 'src="'.Yii::$app->request->baseUrl, $model->content); 
                    ?>
                </p>
                </div>

                <!-- <div class="entry-author">
                    <figure>
                        <img src="assets/images/blog/users/author.jpg" alt="Author" class="img-responsive">
                    </figure>
                    <div class="author-content">
                        <h4><a href="#">Justin Gadget</a></h4>
                        <p>Perspiciatis accusantium laudantium deserunt ad vel sit ipsam consequuntur tenetur similique, recusandae esse a enim, quibusdam ullam, error doloribus. Accusamus alias, ipsum tempora sapiente reiciendis quasi nostrum ratione aspernatur repellat.</p>
                        <div class="social-icons">
                            <label>Find Us:</label>
                            <a href="#" class="social-icon" title="Facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="social-icon" title="Twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="social-icon" title="Github"><i class="fa fa-github"></i></a>
                            <a href="#" class="social-icon" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                            <a href="#" class="social-icon" title="Tumblr"><i class="fa fa-tumblr"></i></a>
                            <a href="#" class="social-icon" title="Flickr"><i class="fa fa-flickr"></i></a>
                        </div>
                    </div>
                </div> -->
            </article>

            <!-- <div class="single-related-posts">
                <h3 class="title mb25">Related Posts</h3>
                <div class="blog-related-carousel owl-carousel">
                    <article class="entry entry-grid">
                        <div class="entry-media">
                            <figure>
                                <a href="single.html"><img src="assets/images/blog/post1.jpg" alt="Post image"></a>
                            </figure>
                            <div class="entry-meta">
                                <span><i class="fa fa-calendar"></i>17 Jan, 2016</span>
                                <a href="#"><i class="fa fa-user"></i> Admin</a>
                            </div>
                        </div>
                        <h2 class="entry-title"><i class="fa fa-file-image-o"></i><a href="single.html">Lorem ipsum dolor sit ametli elits.</a></h2>
                        <div class="entry-content">
                            <p>Molestiae neque doloremque, voluptatum nostrum praesentium esse fugiat sus siel. Deserunt praesentium archite.</p>
                            <a href="#" class="readmore">Read more<i class="fa fa-angle-right"></i></a>
                        </div>
                    </article>

                    <article class="entry entry-grid">
                        <div class="entry-media">
                            <figure id="blog-post-gallery" class="carousel slide" data-ride="carousel" data-interval="10000">
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <a href="single.html"><img src="assets/images/blog/post2.jpg" alt="Post image"></a>
                                    </div>

                                    <div class="item">
                                        <a href="single.html"><img src="assets/images/blog/post3.jpg" alt="Post image"></a>
                                    </div>
                                </div>

                                
                                <a class="left carousel-control" href="#blog-post-gallery" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                                <a class="right carousel-control" href="#blog-post-gallery" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
                            </figure>

                            <div class="entry-meta">
                                <span><i class="fa fa-calendar"></i>14 Jan, 2016</span>
                                <a href="#"><i class="fa fa-user"></i> Admin</a>
                            </div>
                        </div>
                        <h2 class="entry-title"><i class="fa fa-file-image-o"></i><a href="single.html">Lorem ipsum dolor sit ametli elits.</a></h2>
                        <div class="entry-content">
                            <p>Molestiae neque doloremque, voluptatum nostrum praesentium esse fugiat sus siel. Deserunt praesentium archite.</p>
                            <a href="#" class="readmore">Read more<i class="fa fa-angle-right"></i></a>
                        </div>
                    </article>

                    <article class="entry entry-grid">
                        <div class="entry-media">
                            <figure>
                                <a href="single.html"><img src="assets/images/blog/post4.jpg" alt="Post image"></a>
                            </figure>
                            <div class="entry-meta">
                                <span><i class="fa fa-calendar"></i>17 Jan, 2016</span>
                                <a href="#"><i class="fa fa-user"></i> Admin</a>
                            </div>
                        </div>
                        <h2 class="entry-title"><i class="fa fa-file-image-o"></i><a href="single.html">Lorem ipsum dolor sit ametli elits.</a></h2>
                        <div class="entry-content">
                            <p>Molestiae neque doloremque, voluptatum nostrum praesentium esse fugiat sus siel. Deserunt praesentium archite.</p>
                            <a href="#" class="readmore">Read more<i class="fa fa-angle-right"></i></a>
                        </div>
                    </article>

                    <article class="entry entry-grid">
                        <div class="entry-media">
                            <figure>
                                <a href="single.html"><img src="assets/images/blog/post5.jpg" alt="Post image"></a>
                            </figure>
                            <div class="entry-meta">
                                <span><i class="fa fa-calendar"></i>17 Jan, 2016</span>
                                <a href="#"><i class="fa fa-user"></i> Admin</a>
                            </div>
                        </div>
                        <h2 class="entry-title"><i class="fa fa-file-image-o"></i><a href="single.html">Lorem ipsum dolor sit ametli elits.</a></h2>
                        <div class="entry-content">
                            <p>Molestiae neque doloremque, voluptatum nostrum praesentium esse fugiat sus siel. Deserunt praesentium archite.</p>
                            <a href="#" class="readmore">Read more<i class="fa fa-angle-right"></i></a>
                        </div>
                    </article>
                </div>
            </div> -->

            <!-- <div class="comments">
                <h3 class="title mb25"><span>Commnets (20)</span></h3>
                <ul class="comments-list media-list">
                    <li class="media">
                        <div class="comment">
                            <div class="media-left">
                                <img class="media-object" src="assets/images/blog/users/user1.jpg" alt="User name">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Steve Nash<span class="comment-date">49 min ago</span></h4>

                                <p>Enim ad sapiente itaque illo laudantium tempore rem iure accusantium atque. Qui facilis quibusdam laborum. Nobis animi provident ipsum quidem eveniet voluptate expedita veniam porro quod a. Temporibus, voluptates provident.Ex deserunt error, culpa voluptatibus.</p>
                            </div>
                        </div>
                        <ul>
                            <li class="media">
                                <div class="comment">
                                    <div class="media-left">
                                        <img class="media-object" src="assets/images/blog/users/user2.jpg" alt="User name">
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Desmond Mason<span class="comment-date">32 min ago</span></h4>

                                        <p>Ex deserunt error, culpa voluptatibus, explicabo commodi id fugiat ipsa. Doloremque ipsum sapiente laudantium similique repellat, tempora voluptatem. Sunt tempore eligendi inventore molestiae delectus harum quasi consequuntur nisi repellendus saepe?</p>
                                    </div>
                                </div>
                                <ul>
                                    <li class="media">
                                        <div class="comment">
                                            <div class="media-left">
                                                <img class="media-object" src="assets/images/blog/users/user3.jpg" alt="User name">
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Ben Wallece<span class="comment-date">21 min ago</span></h4>

                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur, iure. Minima alias nam magni distinctio voluptatum dolorum nostrum, iusto magnam ullam iure pariatur, qui dolore maiores iste, unde vitae. Dolores.</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="media">
                        <div class="comment">
                            <div class="media-left">
                                <img class="media-object" src="assets/images/blog/users/user4.jpg" alt="User name">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Steve Kerr<span class="comment-date">2 hours ago</span></h4>

                                <p>Minima quae unde quam totam enim. Corporis debitis, tenetur illo deleniti totam perferendis doloribus. Laboriosam aperiam provident debitis earum fuga quibusdam odit unde vel hic commodi dolor, minus, nobis aliquid.</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div> -->

            <!-- <div id="respond" class="comment-respond">
                <h3 class="title mb25"><span>Send a Reply</span></h3>
                <form action="#" method="post">
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Name" required>
                        </div>
                        <div class="col-sm-6">
                            <input type="email" class="form-control" placeholder="Email" required>
                        </div>
                    </div>
                    <input type="url" class="form-control" placeholder="Website" required>
                    <textarea class="form-control" rows="6" placeholder="Add Your Comment" required></textarea>
                    <div class="mb5"></div>
                    <input type="submit" class="btn btn-black min-width" value="Send Reply">
                </form>
            </div> -->
        </div>

        <aside class="col-md-3 col-md-pull-9 sidebar">
            <div class="widget search-widget">
                <form action="#">
                    <input type="search" class="form-control" placeholder="Search in here" required>
                    <button type="submit" class="btn btn-link"><i class="fa fa-search"></i></button>
                </form>
            </div>

            <div class="widget">
                <h3 class="widget-title">Categories</h3>
                <ul class="fa-ul">
                    <?php foreach ($category as $value): ?>
                        <li><a href="<?= Url::to(['/','slug'=>Yii::$app->request->get('slug'),'cat'=>$value->slug]); ?>"><i class="fa-li fa fa-chain"></i><?= $value->title; ?></a></li>    
                    <?php endforeach ?>
                </ul>
            </div>

            <div class="widget">
                <h3 class="widget-title">Recent Posts</h3>
                <ul class="posts-list">
                    <?php foreach ($recent_post as $value): ?>
                        <li>
                            <figure>
                                <a href="<?= Url::to(['/','slug'=>Yii::$app->request->get('slug'),'id' => $value->id]); ?>" title="Dolores labore quod"><img width="60px" height="60px" src="<?php echo (strstr($value->image_featured, 'http'))? $value->image_featured : Yii::$app->request->baseUrl.'/'.$value->image_featured; ?>" alt="Post"></a>
                            </figure>
                            <h5><a href="<?= Url::to(['/','slug'=>Yii::$app->request->get('slug'),'id' => $value->id]); ?>"><?= $value->title ?></a></h5>
                            <span><?= Yii::$app->formatter->asDate($value->timestamp_create); ?></span>
                        </li>    
                    <?php endforeach ?>
                </ul>
            </div>

            <!-- <div class="widget">
                <h3 class="widget-title">Tagcloud</h3>
                <div class="tagcloud">
                    <a href="#">Design</a>
                    <a href="#">Develop</a>
                    <a href="#">Seo</a>
                    <a href="#">jQuery</a>
                    <a href="#">Wordpress</a>
                    <a href="#">Angular</a>
                    <a href="#">Node</a>
                    <a href="#">Express</a>
                    <a href="#">Gulp</a>
                    <a href="#">Sass</a>
                    <a href="#">Bootstrap</a>
                    <a href="#">Html5</a>
                    <a href="#">Css3</a>
                    <a href="#">Node</a>
                </div>
            </div> -->

            <!-- <div class="widget flickr-widget">
                <h3 class="widget-title">Flickr</h3>
                <div class="row">
                    <ul class="flickr-widget-list clearfix"></ul>
                </div>
            </div> -->

            <div class="widget">
                <h3 class="widget-title">Twitter Feed</h3>
                <a class="twitter-timeline" data-lang="id" data-height="500" href="https://twitter.com/jatengopendata?ref_src=twsrc%5Etfw">Tweets by jatengopendata</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>

            <!-- <div class="widget">
                <h3 class="widget-title">Video</h3>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" width="560" height="315" src="http://www.youtube.com/embed/jt8YicG-F6c" allowfullscreen></iframe>
                </div>
            </div> -->
        </aside>
    </div>
</div>
<style type="text/css" media="screen">
    body {
        background-color: white;
    }
</style>