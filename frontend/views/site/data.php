<?php 
// use acerix\yii2\readmore\Readmore;
use yii\helpers\Html;
use yii\helpers\Url;
// use yii\bootstrap\ActiveForm;
// use yii\captcha\Captcha;

// $this->title = $title;
?>
<div class="page-header larger parallax custom" style="background-image:url(assets/images/page-header-bg.jpg)">
    <div class="container">
        <h1>Datasets</h1>
        <ol class="breadcrumb">
            <li><a href="<?= Url::to(['']) ?>">Home</a></li>
            <!-- <li><a href="#">Pages</a></li> -->
            <li class="active">Datasets</li>
        </ol>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="category-filter-row">
                <div class="right">
                    <span class="cat-product-count">Total Datasets: 2083</span>
                    <div class="filter-product-view">
                        <a href="category.html" class="btn btn-sm btn-gray" title="Category Grid"><i class="fa fa-th"></i></a>
                        <a href="category-list.html" class="btn btn-sm btn-custom" title="Category List"><i class="fa fa-th-list"></i></a>
                    </div>
                </div>
                <div class="left">
                    <div class="filter-container filter-sort">
                        <label>Sort by:</label>
                        <select class="form-control input-sm">
                            <option value="Date">Date</option>
                            <option value="Color">Color</option>
                            <option value="Size">Size</option>
                            <option value="Price">Price</option>
                        </select>
                    </div>
                    <div class="filter-container filter-show">
                        <label>Show:</label>
                        <select class="form-control input-sm">
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                        </select>
                    </div>
                </div>
            </div>
            <?php foreach ($dataset as $value): ?>
                <div class="product product-list">
                    <div class="row">
                        <div class="col-md-8 col-sm-7">
                            <h3 class="product-title"><a href="#"><?= $value['title'] ?></a></h3>
                            <div class="product-content">
                                <p><?= $value['notes'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>

            <nav class="pagination-container">
                <label>Showing: 1-4 of 16</label>
                <ul class="pagination">
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li>
                        <a href="#" aria-label="Next">
                            <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

        <aside class="col-md-3 sidebar shop-sidebar">

            <div class="widget">
                <div class="filter-group-widget">
                    <div class="panel-group" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-custom">
                            <div class="panel-heading" role="tab" id="brandFilter-header">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#brandFilter" aria-expanded="true" aria-controls="brandFilter">
                                        Groups
                                        <span class="panel-icon"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="brandFilter" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="brandFilter-header">
                                <div class="panel-body">
                                    <ul class="filter-brand-list">
                                        <li><a href="#"><i class="fa fa-angle-right"></i>Yikes &amp; Sports <span>(11)</span></a></li>
                                        <li><a href="#"><i class="fa fa-angle-right"></i>Smittzy &amp; Jane <span>(7)</span></a></li>
                                        <li><a href="#"><i class="fa fa-angle-right"></i>Susan's Secrets <span>(10)</span></a></li>
                                        <li><a href="#"><i class="fa fa-angle-right"></i>Puma &amp; Cougar <span>(14)</span></a></li>
                                        <li><a href="#"><i class="fa fa-angle-right"></i>Jane Shirts <span>(9)</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </aside>
    </div>
</div>
<style type="text/css" media="screen">
    body {
        background-color: white;
    }
</style>