<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use acerix\yii2\readmore\Readmore;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
// use yii\bootstrap\ActiveForm;
// use yii\captcha\Captcha;

$this->title = 'Jelajah Daerah';
// $this->params['breadcrumbs'][] = $this->title;
?>
<?php //echo Yii::getAlias(); ?>
<?php 
// echo Yii::getAlias('@backend');
 ?>
<div class="main">
    <div class="page-header larger parallax custom" style="background-image:url(assets/images/page-header-bg.jpg)">
        <div class="container">
            <h1><?= $this->title; ?></h1>
            <ol class="breadcrumb">
                <li><a href="<?= Url::to(['/']) ?>">Home</a></li>
                <!-- <li><a href="#">Pages</a></li> -->
                <li class="active"><?= $this->title; ?></li>
            </ol>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-push-3">
                <?php foreach ($model as $key => $value): ?>
                <article class="entry entry-list">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="entry-media">
                                <figure>
                                    <a href="<?= Url::to(['site/news', 'id' => $value->id]); ?>"><img src="<?php echo '/'.$value->image_featured; ?>" alt="Post image" width="750px" height="380px"></a>
                                </figure>
                                <div class="entry-meta">
                                    <!-- fa-map-marker -->
                                    <span><i class="fa fa-map-marker"></i><?= ucwords($value->regencies->name)?></span><span><i class="fa fa-calendar"></i><?php echo Yii::$app->formatter->asDate($value->timestamp_create); ?></span>
                                    <a href="#"><i class="fa fa-user"></i> Admin</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h2 class="entry-title"><a target="__blank" href="https://maps.google.com/?q=<?= $value->geo_latitude ?>,<?= $value->geo_longitude ?>"><i class="fa fa-location-arrow"></i></a><a href="<?= Url::to(['site/news', 'id' => $value->id]); ?>"><?= $value->title; ?></a></h2>
                            <div class="entry-content">
                                <?php 
                                $text  = implode(' ', array_slice(explode(' ', $value->content), 0, 20));
                                echo $imgRm = preg_replace('/<img[^>]+\>/i', "", $text);
                                // echo $brRm = preg_replace("/<br[^>]+>/", "", $imgRm);
                                 ?>
                                <a href="<?= Url::to(['site/news', 'id' => $value->id]); ?>" class="readmore">Read more<i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </article>
                <?php endforeach ?>

                <nav class="pagination-container">
                    <?php 
                        echo LinkPager::widget([
                            'pagination' => $pagination,
                        ]);
                     ?>
                    <!-- <label>Showing: 1-4 of 16</label>
                    <ul class="pagination">
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                            </a>
                        </li>
                    </ul> -->
                </nav>

                
            </div>

            <aside class="col-md-3 col-md-pull-9 sidebar">
                <div class="widget search-widget">
                    <form action="#">
                        <input type="search" class="form-control" placeholder="Search in here" required>
                        <button type="submit" class="btn btn-link"><i class="fa fa-search"></i></button>
                    </form>
                </div>

                <div class="widget">
                    <h3 class="widget-title">Categories</h3>
                    <ul class="fa-ul">
                        <?php foreach ($category as $value): ?>
                            <li><a href="#"><i class="fa-li fa fa-chain"></i><?= $value->title; ?></a></li>    
                        <?php endforeach ?>
                    </ul>
                </div>

                <div class="widget">
                    <h3 class="widget-title">Recent Posts</h3>
                    <ul class="posts-list">
                        <?php foreach ($recent_post as $value): ?>
                            <li>
                                <figure>
                                    <a href="#" title="Dolores labore quod"><img width="60px" height="60px" src="<?php echo '/'.$value->image_featured; ?>" alt="Post"></a>
                                </figure>
                                <h5><a href="#"><?= $value->title ?></a></h5>
                                <span><?= Yii::$app->formatter->asDate($value->timestamp_create); ?></span>
                            </li>    
                        <?php endforeach ?>
                    </ul>
                </div>

                <!-- <div class="widget">
                    <h3 class="widget-title">Tagcloud</h3>
                    <div class="tagcloud">
                        <a href="#">Design</a>
                        <a href="#">Develop</a>
                        <a href="#">Seo</a>
                        <a href="#">jQuery</a>
                        <a href="#">Wordpress</a>
                        <a href="#">Angular</a>
                        <a href="#">Node</a>
                        <a href="#">Express</a>
                        <a href="#">Gulp</a>
                        <a href="#">Sass</a>
                        <a href="#">Bootstrap</a>
                        <a href="#">Html5</a>
                        <a href="#">Css3</a>
                        <a href="#">Node</a>
                    </div>
                </div> -->

            </aside>
        </div>
    </div>
</div>
<style type="text/css" media="screen">
    body {
        background-color: white;
    }
</style>