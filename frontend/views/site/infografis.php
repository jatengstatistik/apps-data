<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use acerix\yii2\readmore\Readmore;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
// use yii\bootstrap\ActiveForm;
// use yii\captcha\Captcha;

$this->title = "Info Grafis";
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="main">
    <div class="page-header larger parallax custom" <!-- style="background-image:url(theme/simple/assets/images/page-header-bg.jpg) -->">
        <div class="container">
            <h1><?= $retVal = (empty($this->title)) ? $slug : $this->title; ?></h1>
            <ol class="breadcrumb">
                <li><a href="<?= Url::to(['/']) ?>">Home</a></li>
                <!-- <li><a href="">Infografis</a></li> -->
                <li class="active"><?= $this->title; ?></li>
            </ol>
        </div>
    </div>

    <div class="container">
        <ul id="portfolio-filter" class="text-center">
            <li class="active"><a href="#" data-filter="*">All</a></li>
	        <?php foreach ($cats_brs as $cat): ?>
            <li><a href="#" data-filter=".<?= $cat->id ?>"><?= $cat->title ?></a></li>    
            <?php endforeach ?>
        </ul>
    </div>

    <div class="container">
        <div id="portfolio-item-container" class="popup-gallery max-col-4" data-layoutmode="fitRows" style="position: relative; height: 855px;">
        	<?php foreach ($model as $value): ?>
	        <div class="portfolio-item portfolio-anim <?= $value->cat_id ?>">
	            <figure>
	                <img src="<?php echo Yii::$app->request->baseUrl.'/'.$value->image_featured; ?>" alt="Portfolio Name">
	                <figcaption>
	                    <a href="<?php echo Yii::$app->request->baseUrl.'/'.$value->image_featured; ?>" data-thumb="<?php echo Yii::$app->request->baseUrl.'/'.$value->image_featured; ?>" class="zoom-btn"><i class="fa fa-search"></i></a>
	                    <a href="<?= Url::to(['/','slug' =>'jelajah-daerah','id' => $value->id]); ?>" class="link-btn"><i class="fa fa-link"></i></a>
	                </figcaption>
	            </figure>
	            <div class="portfolio-meta">
	                <h3 class="portfolio-title"><a href="#" title="Portfolio name"><?= $value->title; ?></a></h3>
	            </div>
	        </div>
	        <?php endforeach ?>
        </div>
    	<nav class="pagination-container">
            <?php 
                echo LinkPager::widget([
                    'pagination' => $pagination,
                ]);
             ?>
        </nav>
    </div>
</div>
<style type="text/css" media="screen">
    body {
        background-color: white;
    }
</style>