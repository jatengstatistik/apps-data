<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CkanDatasetCount;

/**
 * CkanDatasetCountSearch represents the model behind the search form of `app\models\CkanDatasetCount`.
 */
class CkanDatasetCountSearch extends CkanDatasetCount
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'dataset_harvested_count', 'dataset_count', 'updated_at','dataset_total'], 'integer'],
            [['organization_id', 'date_harvested', 'formated_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CkanDatasetCount::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // $query->joinWith('organization');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dataset_harvested_count' => $this->dataset_harvested_count,
            'dataset_count' => $this->dataset_count,
            'dataset_total' => $this->dataset_count,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'organization_id', $this->organization_id])
            ->andFilterWhere(['like', 'date_harvested', $this->date_harvested])
            ->andFilterWhere(['like', 'formated_date', $this->formated_date]);

        return $dataProvider;
    }
}
