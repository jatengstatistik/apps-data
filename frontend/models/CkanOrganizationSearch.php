<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CkanOrganization;

/**
 * CkanOrganizationSearch represents the model behind the search form of `app\models\CkanOrganization`.
 */
class CkanOrganizationSearch extends CkanOrganization
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','id_organization','agency_type', 'name', 'title', 'description', 'image_display_url'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CkanOrganization::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'id_organization', $this->id_organization])
            ->andFilterWhere(['like', 'agency_type', $this->agency_type])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image_display_url', $this->image_display_url]);

        return $dataProvider;
    }
}
