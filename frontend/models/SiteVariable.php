<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_variable".
 *
 * @property int $id
 * @property string $site_url
 * @property string $site_ip
 * @property string $site_local_address
 * @property string $ckan_api_key
 * @property string $site_recent_dataset
 * @property int $created_at
 * @property int $updated_at
 */
class SiteVariable extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site_variable';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['site_url', 'site_ip', 'site_local_address', 'ckan_api_key', 'site_recent_dataset', 'created_at', 'updated_at'], 'required'],
            [['site_recent_dataset','site_indikator_makro'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['site_url', 'site_ip', 'site_local_address', 'ckan_api_key'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'site_url' => Yii::t('app', 'Site Url'),
            'site_ip' => Yii::t('app', 'Site Ip'),
            'site_local_address' => Yii::t('app', 'Site Local Address'),
            'ckan_api_key' => Yii::t('app', 'Ckan Api Key'),
            'site_recent_dataset' => Yii::t('app', 'Site Recent Dataset'),
            'site_indikator_makro' => Yii::t('app', 'Site Indikator Makro'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
