<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "package".
 *
 * @property int $id
 * @property string $title
 */
class Package extends \yii\db\ActiveRecord
{
    public static function getDb() {
        return Yii::$app->get('dbckan');
    }

    public function behaviors()
    {
        return [
            [
                // 'class' => SluggableBehavior::className(),
                // 'attribute' => 'title',
                // define if field tabel didnt exist
                // 'slugAttribute' => 'slug',
                // 'immutable' => true,
                // 'ensureUnique'=>true,
            ],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'package';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['title'], 'required'],
            // [['title'], 'string', 'max' => 150],
            // [['parent_id'],'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            // 'id' => Yii::t('app', 'ID'),
            // 'title' => Yii::t('app', 'Title'),
            // 'slug_cat' => Yii::t('app', 'Slug Category'),
            // 'parent_id' => Yii::t('app', 'Parent ID'),
        ];
    }
    
}
