<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "popup_banner".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $captions
 * @property string $url_link
 * @property string $path_file
 * @property int $start_duration
 * @property int $end_duration
 * @property int $created_at
 * @property int $updated_at
 */
class PopupBanner extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                // define if field tabel didnt exist
                // 'slugAttribute' => 'slug',
                'immutable' => true,
                'ensureUnique'=>true,
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'popup_banner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['path_file'],'file'],
            [['start_duration', 'end_duration',],'safe'],
            [['captions'], 'string'],
            [['created_at', 'updated_at','status'], 'integer'],
            [['title', 'slug', 'url_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'captions' => Yii::t('app', 'Captions'),
            'url_link' => Yii::t('app', 'Url Link'),
            'path_file' => Yii::t('app', 'Path File'),
            'start_duration' => Yii::t('app', 'Start Duration'),
            'end_duration' => Yii::t('app', 'End Duration'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
