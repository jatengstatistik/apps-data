<?php

namespace frontend\controllers;

use Yii;
use app\models\Package;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DataController extends Controller
{
	/**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    function actionTest($value='')
    {
        $model = $this->apiCkan('api/3/action/current_package_list_with_resources',['limit'=>'5','offset'=>'0']);
        echo '<pre>';
        print_r($model);
        echo '</pre>';
    }

    public function actionIndex()
    {

        return $this->render('index');
    }

    public function actionPackage()
    {
    	$model = Package::find()->all();
    	// $data = $this->apiCkan('action/package_list');
    	echo '<pre>';
    	print_r($model);
    	echo '</pre>';
    }

    public function actionDatasetShow()
    {
    	$data = $this->apiCkan('action/package_show',['id'=>'0data-klimatologi-stasiun-kradenan-kabupaten-purworejo']);
    	echo '<pre>';
    	print_r($data->result);
    	echo '</pre>';		
    }

    public function datasetShow($id='')
    {
    	$data = $this->apiCkan('action/package_show',['id'=> $id]);
    	return $data;
    	// echo '<pre>';
    	// print_r();
    	// echo '</pre>';		
    }

    public function actionRecentDataset()
    {
        $package_list = $this->apiCkan('action/recently_changed_packages_activity_list')->result;
    	foreach ($package_list as $value) {
    		$dataset[] = [
    			'title' => $value['data']['package']['title'], 
                'name' => $value['data']['package']['name'], 
                'notes' => $value['data']['package']['notes'], 
                'link_dataset' => '/dataset/'.$value['data']['package']['name'],
    		];
    	}
    	echo '<pre>';
    	print_r($dataset);
    	echo '</pre>';
    }

    public function actionGroup()
    {
    	
    }

    public function actionTag(		)
    {
    	
    }

    private function apiCkan($url='',$data='',$method='')
    {
    	// $client = new Client(['baseUrl' => 'http://103.9.227.205/api/3']);
    	$client = new Client(['baseUrl' => 'http://data.jatengprov.go.id/api/3']);
		$response = $client->createRequest()
		  // ->setHeaders(['Authorization' => '61e47dd8-352a-4fe3-99c6-058b1b7b73b0'])
		  ->setMethod($method='GET')
		  // ->setFormat(Client::FORMAT_JSON)
		  ->setUrl($url)
		  ->setData($data)
		  ->send();
		return $responseData = (object) $response->getData();
		// echo '<pre>';
		// print_r((object) $responseData->result);
		// echo '</pre>';
    }

}
