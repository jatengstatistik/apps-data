<?php
namespace frontend\controllers;

use Yii;
use app\models\Banner;
use app\models\Carousel;
use app\models\CkanDatasetCount;
use app\models\CkanGroup;
use app\models\NewsArticle;
use app\models\NewsCat;
use app\models\PopupBanner;
use app\models\SiteVariable;
use common\models\LoginForm;
use frontend\models\ContactForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\VerifyEmailForm;
use linslin\yii2\curl;
use yii\base\InvalidArgumentException;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\httpclient\Client;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    function getKeys($collection='',$field='')
    {
        foreach ($collection as $value) {
            $keys[] = $value->$field;
        }

        return $keys;
    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        shell_exec('yii scheduler/run-all > /dev/null &');
        return $result;
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $getId = Yii::$app->request->get('id');
        $slug = Yii::$app->request->get('slug');
        $catUrl = Yii::$app->request->get('cat');
        $menu= [
          'infografis','statistics',
        ];
        if (!empty($slug)&&empty($getId)) {
            // Ambil Judul
            $slug = NewsCat::findOne(['slug'=> $slug]);
            if (empty($catUrl)) {    
                // $cat = $slug->cats
                $catId = $this->getKeys($slug->cats,'id');
                $cat = '';
            } else {
                // Ambil Category
                $catId = NewsCat::findOne(['slug'=> $catUrl]);
                $cat = $catId->title;
            }
            
            $article = NewsArticle::find()->where(['in','cat_id', $catId])->andWhere(['is_deleted'=>0]);
            $pagination = new Pagination(['totalCount' => $article->count(), 'defaultPageSize' => 5]);
            $model = $article->offset($pagination->offset)
              ->limit($pagination->limit)
              ->orderBy(['timestamp_create' => SORT_DESC])
              ->all();
            return $this->render('post',[
                'model' => $model,
                'pagination' => $pagination,
                'category' => NewsCat::findOne(['slug'=> $slug])->cats,
                'recent_post' => $article->orderBy(['timestamp_create' => SORT_DESC])->limit(5)->all(),
                'slug' => $slug->title,
                'cat' => $cat,
            ]);
            // echo '<pre>';
            // print_r($this->getKeys($cat->cats,'id'));
            // print_r($article);
            // echo '</pre>';
        }elseif(!empty($slug)&&!empty($getId)){

            return $this->render('postView', [
                'model' => NewsArticle::findOne($getId),
                'category' => NewsCat::findOne(['slug'=> $slug])->cats,
                'recent_post' => NewsArticle::find()->orderBy(['timestamp_create' => SORT_DESC])->limit(5)->all(),
                'cat' => NewsCat::findOne(['slug'=> $slug])->title,
                'slug' => NewsCat::findOne(['slug'=> $slug])->slug,
            ]);
        }elseif(in_array(Yii::$app->request->get('menu'),$menu)){
          // Jika Param Menu Terisi
          switch (Yii::$app->request->get('menu')) {
            case 'infografis':
                $cats_brs = NewsCat::find()->where(['slug'=>'berita-statistik'])->one()->cats;
                $article = NewsArticle::find()->where(['in','cat_id',$this->getKeys($cats_brs,'id')]);
                $pagination = new Pagination(['totalCount' => $article->count(), 'defaultPageSize' => 20]);
                $model = $article->offset($pagination->offset)
                  ->limit($pagination->limit)
                  ->orderBy(['timestamp_create' => SORT_DESC])
                  ->all();
                return $this->render('infografis',[
                  'model'=> $model,
                  'pagination'=> $pagination,
                  'cats_brs' => $cats_brs,
                ]);
              break;
            case 'statistics':
                $data_instansi = CkanDatasetCount::find()->where(['formated_date' => date('d-m-Y',time())]);
                // $article = NewsArticle::find()->where(['in','cat_id',[6,9,10,11,12]]);
                $pagination = new Pagination(['totalCount' => $data_instansi->count(), 'defaultPageSize' => 20]);
                $model = $data_instansi->offset($pagination->offset)
                  ->limit($pagination->limit)
                  // ->orderBy(['timestamp_create' => SORT_DESC])
                  ->all();
                return $this->render('statistics',[
                  'model'=> $model,
                  'pagination'=> $pagination,
                ]);
              break;
            
            default:
              // code...
              break;
          } 
          // End Switch
            
        }else{
            // Index / Halaman Home / Awal
            $cats_brs = NewsCat::find()->where(['slug'=>'berita-statistik'])->one()->cats;
            return $this->render('index',[
              'cats_brs' => $cats_brs,
              'article' => NewsArticle::find()->where(['is_deleted'=>0])->andWhere(['not in','cat_id',$this->getKeys($cats_brs,'id')])->orderBy(['timestamp_create' => SORT_DESC])->limit(8)->all(),
              'info_grafis' => NewsArticle::find()->where(['in','cat_id',$this->getKeys($cats_brs,'id')])->orderBy(['timestamp_create' => SORT_DESC])->limit(10)->all(),
              'runing_text' =>  $this->setRuningText(),
              'recent_dataset' =>  json_decode(SiteVariable::find(1)->one()->site_recent_dataset),
              'indikator_makro' =>  json_decode(SiteVariable::find(1)->one()->site_indikator_makro),
              'group_list' => CkanGroup::find()->all(),
              'banner' => Banner::find()->all(),
              'carousel' => Carousel::find()->all(),
              'headline' => NewsArticle::find()->where(['headline'=>1,'is_deleted'=>0])->all(),
            ]);
        }
        
    }

    function setRuningText()
    {
        $runText = [
          // $this->getWarningWhether(),
          $this->getWhether(),
        ];
        return implode(' ', $runText);
    }

    function whetherCodeBmkg($code='')
    {
        switch ($code) {
            // case ($code == '0'||$code == '100'):
            case (0):
                return 'Cerah';
                break;
            // case ($code == '1'||$code == '101'):
            case (1):
                return 'Cerah Berawan';
                break;
            case (2):
                return 'Cerah Berawan';
                break;
            case (3):
                return 'Berawan';
                break;
            case (4):
                return 'Berawan Tebal';
                break;
            case (5):
                return 'Udara Kabur';
                break;
            case (10):
                return 'Asap';
                break;
            case (45):
                return 'Kabut';
                break;
            case (60):
                return 'Hujan Ringan';
                break;
            case (61):
                return 'Hujan Sedang';
                break;
            case (63):
                return 'Hujan Lebat';
                break;
            case (80):
                return 'Hujan Lokal';
                break;
            case (95):
                return 'Hujan Petir';
                break;
            case (97):
                return 'Hujan Petir';
                break;
            default:
                return null;
                break;
        }
        // http://data.bmkg.go.id/prakiraan-cuaca/#kodecuaca
        // 0 / 100 : Cerah / Clear Skies
        // 1 / 101 : Cerah Berawan / Partly Cloudy
        // 2 / 102 : Cerah Berawan / Partly Cloudy
        // 3 / 103 : Berawan / Mostly Cloudy
        // 4 / 104 : Berawan Tebal / Overcast
        // 5 : Udara Kabur / Haze
        // 10 : Asap / Smoke
        // 45 : Kabut / Fog
        // 60 : Hujan Ringan / Light Rain
        // 61 : Hujan Sedang / Rain
        // 63 : Hujan Lebat / Heavy Rain
        // 80 : Hujan Lokal / Isolated Shower
        // 95 : Hujan Petir / Severe Thunderstorm
        // 97 : Hujan Petir / Severe Thunderstorm
    }

    function currentWhether($whethers='')
    {
        date_default_timezone_set("Asia/Jakarta");
        $time = date('d-m-Y H:i:s',strtotime(date('d-m-Y H:i:s')));
        switch ($time) {
            case ((strtotime($time) >= strtotime(date('d-m-Y 07:01:00'))) && (strtotime($time) <= strtotime(date('d-m-Y 13:00:00')))):
                    $whether = $whethers['0'];
                    return $this->whetherCodeBmkg($whether['value']);
                break;
            case ((strtotime($time) >= strtotime(date('d-m-Y 13:01:00'))) && (strtotime($time) <= strtotime(date('d-m-Y 19:00:00')))):
                    $whether = $whethers['1'];
                    return $this->whetherCodeBmkg($whether['value']);
                break;
            case ((strtotime($time) >= strtotime(date('d-m-Y 19:01:00'))) && (strtotime($time) <= strtotime(date('d-m-Y 01:00:00').'+1 day'))):
                    $whether = $whethers['2'];
                    return $this->whetherCodeBmkg($whether['value']);
                break;
            case ((strtotime($time) >= strtotime(date('d-m-Y 01:01:00'))) && (strtotime($time) <= strtotime(date('d-m-Y 07:00:00')))):
                    $whether = $whethers['3'];
                    return $this->whetherCodeBmkg($whether['value']);
                break;
            default:
                    return null;
                break;
        }
    }

    public function getWhether()
    {
        $nowTime = date('d-M-Y H:i:s',strtotime(date('d-M-Y H:i:s')."+7hours"));
        // get data From DB last active API
        $savedData = SiteVariable::find(1)->one()->site_whether;
        $response_data = json_decode($savedData,'true');
        $data_cuaca = $response_data['forecast']['area'];

        foreach ($data_cuaca as $value) {
            if (!empty($value['parameter']['6']['timerange'])) {
                $whethers[] = $value['name']['1'].' - '.$this->currentWhether($value['parameter']['6']['timerange']);
            }
        }

        return "Prakiraan Cuaca Jawa Tengah ".$nowTime.'  '.implode(', ', $whethers)." Sumber: BMKG (Badan Meteorologi, Klimatologi, dan Geofisika)";
    }

    // http://datadisplay.bmkg.go.id/XML/WarningCuaca.xml
    public function getWarningWhether()
    {
        // get data From DB last active API
        $savedData = SiteVariable::find(1)->one()->site_warning_whether;
        $response_data = json_decode($savedData,'true');
        $data_cuaca = $response_data['info']['data'];
        
        foreach ($data_cuaca as $value) {
            if (strpos($value['headline'], "Jawa Tengah") !== false) {
                return $value['headline'].' - '.$value['description'];   
            }else{
              return null;
            }
        }
    }

    function actionData()
    {
      if ($curl->errorCode === null) {
        $result = json_decode($response);
        foreach ($result as $value) {
          $dataset[] = [
            'title' => $value->data->package->title, 
            'name' => $value->data->package->name, 
            'notes' => $value->data->package->notes, 
          ];
        }
        $result  = $this->unique_multidim_array($dataset,'name');
        $filter = ['pemerintah-'];
        foreach ($result as $val) {
          if (!$this->match($filter,$val['name'])) {
            $data_filter[] = $val;
          }
        }

      } else {
           // List of curl error codes here https://curl.haxx.se/libcurl/c/libcurl-errors.html
          switch ($curl->errorCode) {
          
              case 6:
                  //host unknown example
                  break;
          }
      } 
      return $this->render('data',[
        'dataset' => array_slice($data_filter, 5),
      ]);
    }

    function match($needles, $haystack)
    {
        foreach($needles as $needle){
            if (strpos($haystack, $needle) !== false) {
                return true;
            }
        }
        return false;
    }

    function unique_multidim_array($array, $key) { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 
        
        foreach($array as $val) { 
            if (!in_array($val[$key], $key_array)) { 
                $key_array[$i] = $val[$key]; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    }

    /**
     * Displays exploreRegion.
     *
     * @return mixed
     */
    public function actionExploreRegion()
    {
        $article = NewsArticle::find();
        $pagination = new Pagination(['totalCount' => $article->count(), 'defaultPageSize' => 5]);
        $model = $article->offset($pagination->offset)
          ->limit($pagination->limit)
          ->all();
        return $this->render('exploreRegion',[
            'model' => $model,
            'pagination' => $pagination,
            'category' => NewsCat::find()->all(),
            'recent_post' => NewsArticle::find()->orderBy('timestamp_create','desc')->limit(5)->all(),
        ]);
    }

    /**
     * Displays statistic News.
     *
     * @return mixed
     */
    public function actionNewsStatistic()
    {
        return $this->render('newsStatistic');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }
}
