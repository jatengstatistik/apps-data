<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class SimpleAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $sourcePath = '@frontend';
    public $baseUrl = '@web';
    public $css = [
        // 'css/site.css',
        // 'theme/simple/assets/css/fotorama.css',
        // 'theme/simple/assets/css/jquery.fullpage.css',
        // 'theme/simple/assets/css/jquery.multiscroll.css',
        'theme/simple/assets/css/layers.css',
        'theme/simple/assets/css/navigation.css',
        'theme/simple/assets/css/plugins.min.css',
        'theme/simple/assets/css/settings.css',
        'theme/simple/assets/css/style.css',
        // 'theme/simple/assets/css/homepages/index-agency2.css',
        // 'theme/simple/assets/css/homepages/index-agency3.css',
        // 'theme/simple/assets/css/homepages/index-agency4.css',
        // 'theme/simple/assets/css/homepages/index-agency5.css',
        // 'theme/simple/assets/css/homepages/index-agency6.css',
        // 'theme/simple/assets/css/homepages/index-architect.css',
        // 'theme/simple/assets/css/homepages/index-blog2.css',
        // 'theme/simple/assets/css/homepages/index-cafe.css',
        // 'theme/simple/assets/css/homepages/index-cafe2.css',
        // 'theme/simple/assets/css/homepages/index-charity.css',
        // 'theme/simple/assets/css/homepages/index-charity2.css',
        // 'theme/simple/assets/css/homepages/index-construction.css',
        // 'theme/simple/assets/css/homepages/index-construction2.css',
        // 'theme/simple/assets/css/homepages/index-construction3.css',
        // 'theme/simple/assets/css/homepages/index-creative.css',
        // 'theme/simple/assets/css/homepages/index-creative2.css',
        // 'theme/simple/assets/css/homepages/index-creative3.css',
        // 'theme/simple/assets/css/homepages/index-creative4.css',
        // 'theme/simple/assets/css/homepages/index-dentist.css',
        // 'theme/simple/assets/css/homepages/index-event.css',
        // 'theme/simple/assets/css/homepages/index-event2.css',
        // 'theme/simple/assets/css/homepages/index-gym.css',
        // 'theme/simple/assets/css/homepages/index-gym2.css',
        // 'theme/simple/assets/css/homepages/index-hotel.css',
        // 'theme/simple/assets/css/homepages/index-landing.css',
        // 'theme/simple/assets/css/homepages/index-landing2.css',
        // 'theme/simple/assets/css/homepages/index-landing3.css',
        // 'theme/simple/assets/css/homepages/index-lawyer.css',
        // 'theme/simple/assets/css/homepages/index-medical.css',
        // 'theme/simple/assets/css/homepages/index-medical2.css',
        // 'theme/simple/assets/css/homepages/index-onepage.css',
        // 'theme/simple/assets/css/homepages/index-onepage2.css',
        // 'theme/simple/assets/css/homepages/index-onepage3.css',
        // 'theme/simple/assets/css/homepages/index-onepage4.css',
        // 'theme/simple/assets/css/homepages/index-restaurant.css',
        // 'theme/simple/assets/css/homepages/index-restaurant2.css',
        // 'theme/simple/assets/css/homepages/index-shop4.css',
        // 'theme/simple/assets/css/homepages/index-veterinary.css',
        // 'theme/simple/assets/css/homepages/index-agency.css',
        // 'theme/simple/assets/css/theme-skins/bluegrey.css',
        // 'theme/simple/assets/css/theme-skins/brown.css',
        // 'theme/simple/assets/css/theme-skins/deeppurple.css',
        // 'theme/simple/assets/css/theme-skins/green.css',
        // 'theme/simple/assets/css/theme-skins/grey.css',
        // 'theme/simple/assets/css/theme-skins/lightblue.css',
        // 'theme/simple/assets/css/theme-skins/pink.css',
        // 'theme/simple/assets/css/theme-skins/purple.css',
        'theme/simple/assets/css/theme-skins/red.css',
        // 'theme/simple/assets/css/theme-skins/yellow.css',
    ];
    public $js = [
        'theme/simple/assets/js/plugins.min.js',
        // 
        'theme/simple/assets/js/twitter/jquery.tweet.min.js',
        // 
        'theme/simple/assets/js/main.js',
        // 'theme/simple/assets/js/coming-soon.js',
        // 'theme/simple/assets/js/contact.js',
        // 'theme/simple/assets/js/fotorama.js',
        // 'theme/simple/assets/js/jquery.backstretch.min.js',
        // 'theme/simple/assets/js/jquery.countdown.min.js',
        // 'theme/simple/assets/js/jquery.elevateZoom.min.js',
        // 'theme/simple/assets/js/jquery.fullpage.min.js',
        // 'theme/simple/assets/js/jquery.kwicks.min.js',
        // 'theme/simple/assets/js/jquery.multiscroll.min.js',
        // 'theme/simple/assets/js/jquery.pagepiling.min.js',
        'theme/simple/assets/js/jquery.plugin.min.js',        
        'theme/simple/assets/js/jquery.themepunch.tools.min.js',
        'theme/simple/assets/js/jquery.themepunch.revolution.min.js',        
        // 'theme/simple/assets/js/jquery.validate.min.js',        
        'theme/simple/assets/js/modernizr.js',
        // 'theme/simple/assets/js/morphext.min.js',
        // 'theme/simple/assets/js/particles.min.js',        
        // 'theme/simple/assets/js/spectragram.min.js',
        // 'theme/simple/assets/js/typed.min.js',
        
        'theme/simple/assets/js/extensions/revolution.extension.actions.min.js',
        'theme/simple/assets/js/extensions/revolution.extension.carousel.min.js',
        'theme/simple/assets/js/extensions/revolution.extension.kenburn.min.js',
        'theme/simple/assets/js/extensions/revolution.extension.layeranimation.min.js',
        'theme/simple/assets/js/extensions/revolution.extension.migration.min.js',
        'theme/simple/assets/js/extensions/revolution.extension.navigation.min.js',
        'theme/simple/assets/js/extensions/revolution.extension.parallax.min.js',
        'theme/simple/assets/js/extensions/revolution.extension.slideanims.min.js',
        'theme/simple/assets/js/extensions/revolution.extension.video.min.js',     
    ];
    public $depends = [
        // 'yii\web\YiiAsset',
    //     'yii/bootstrap/BootstrapAsset',
    ];
}
